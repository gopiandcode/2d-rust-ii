
module Interpreter (simulate_game) where
import Control.Monad.Trans
import Control.Monad.State
import Control.Monad.Trans.Maybe
import Data.Char
import System.IO
import Data.List



import CodeGenerator

getInt :: IO Int
getInt = do
  skip_whitespace;
  digits <- get_digits;
  case digits of
    [] -> do
      putStrLn "Please enter a valid integer."
      getLine
      getInt
    xs -> return $ foldl' (\a x -> 10 * a + x) 0 . Prelude.map digitToInt $ xs
  
skip_whitespace :: IO ()
skip_whitespace = do
  nc <- hLookAhead stdin
  if isSpace nc
    then do getChar; skip_whitespace
    else return()
get_digits :: IO [Char]
get_digits = do
  nc <- hLookAhead stdin
  if isDigit nc
    then do getChar; fmap (nc:) get_digits
    else return []

print_option :: (Int,String) -> IO ()
print_option (ind,val) = do
  putStrLn $ "\t" ++ show ind ++ ") " ++ val

choose_from_list :: [String] -> IO Int
choose_from_list options = do
  putStrLn "Please choose one of the following:"
  mapM_ print_option $ zip [0..] options
  choice <- getInt;
  putStrLn $ "Your choice was " ++ (show choice)
  if choice < length options
    then return choice
    else (do
       putStrLn $ "Please enter a value between [0.." ++ (show . length $ options) ++ "]"
       choose_from_list options)

check_exit :: IO Bool
check_exit = do
  putStrLn "Exit the game?"
  option <- choose_from_list ["Yes", "No"]
  return $ option == 0

data InterpreterState = IS {
  is_stack :: [(Int, DialogID)],
  is_items :: [ItemID],
  is_code :: [(DialogID, [Instruction])],
  is_map :: Maybe MapID,
  is_num :: [(NumVarID, Int)],
  is_txt :: [(TextVarID, String)]
}

simulate_game :: [(DialogID, [Instruction])] -> IO ()
simulate_game = fmap fst . runStateT run_program . build_interpreter

build_interpreter :: [(DialogID, [Instruction])] -> InterpreterState
build_interpreter ins =
  IS [] [] ins Nothing [] []

run_program :: StateT InterpreterState IO ()
run_program = do
  state <- get;
  case is_stack state of
    [] -> do
      should_exit <- liftIO check_exit
      if should_exit
        then return ()
        else do
           value <- liftIO . choose_from_list . Prelude.map (show . fst) . is_code $ state;
           put (state { is_stack = [(0, DialogID value)] })
           run_program
    (pos,dlg):rst -> do
      let ocode = Data.List.lookup dlg . is_code $ state;
      case ocode of
        Nothing -> error "Invalid dialog id found"
        Just code -> if length code <= pos
                        then do
                           put (state { is_stack = rst })
                           run_program
                        else do
                           let ins = code !! pos;
                           execute_instruction ins;
                           run_program

replace_text :: [String] -> String -> String
replace_text (h:t) ('{':'}':rest) = h ++ replace_text t rest
replace_text [] str = str
replace_text items [] = []
replace_text items (h:t) = h : replace_text items t

eval_str_arg :: Either TextExpr NumExpr -> StateT InterpreterState IO String
eval_str_arg (Left texp) = eval_text_expr texp
eval_str_arg (Right nexp) = fmap show . eval_num_expr $ nexp

eval_num_expr :: NumExpr -> StateT InterpreterState IO Int
eval_num_expr (IntConst i) = return (fromInteger i)
eval_num_expr (NumVar id) = do
  state <- get;
  case Data.List.lookup id (is_num state) of
    Nothing -> return 0
    Just v  -> return v
eval_num_expr (NumNeg a) = do
  v <- eval_num_expr a;
  return $ (-v)
eval_num_expr (NumAdd a b) = do
  na <- eval_num_expr a;
  nb <- eval_num_expr b;
  return $ na + nb
eval_num_expr (NumSub a b) = do
  na <- eval_num_expr a;
  nb <- eval_num_expr b;
  return $ na - nb
eval_num_expr (NumMul a b) = do
  na <- eval_num_expr a;
  nb <- eval_num_expr b;
  return $ na * nb
eval_num_expr (NumDiv a b) = do
  na <- eval_num_expr a;
  nb <- eval_num_expr b;
  return $ na `div` nb

eval_text_expr :: TextExpr -> StateT InterpreterState IO String
eval_text_expr (StringConst s) = return s
eval_text_expr (TextVar id) = do
  state <- get;
  case Data.List.lookup id (is_txt state) of
    Nothing -> return ""
    Just s  -> return s
eval_text_expr (TextFormat fmt_str exprs) = do
  texprs <- mapM eval_str_arg exprs;
  return $ replace_text texprs fmt_str
eval_text_expr (TextAdd a b) = do
  ta <- eval_text_expr a;
  tb <- eval_text_expr b;
  return $ ta ++ tb

eval_bool_expr :: BoolExpr -> StateT InterpreterState IO Bool
eval_bool_expr (BoolConst b) = return b
eval_bool_expr (BoolCheck id) = do
  state <- get;
  return $ elem id (is_items state)
eval_bool_expr (BoolNot b) = do
  bn <- eval_bool_expr b;
  return $ not bn
eval_bool_expr (BoolAnd a b) = do
  ba <- eval_bool_expr a;
  bb <- eval_bool_expr b;
  return $ and [ba, bb]
eval_bool_expr (BoolOr a b) = do
  ba <- eval_bool_expr a;
  bb <- eval_bool_expr b;
  return $ or [ba, bb]
eval_bool_expr (BoolGt a b) = do
  na <- eval_num_expr a;
  nb <- eval_num_expr b;
  return $ na > nb
eval_bool_expr (BoolLt a b) = do
  na <- eval_num_expr a;
  nb <- eval_num_expr b;
  return $ na < nb
eval_bool_expr (BoolGte a b) = do
  na <- eval_num_expr a;
  nb <- eval_num_expr b;
  return $ na >= nb
eval_bool_expr (BoolLte a b) = do
  na <- eval_num_expr a;
  nb <- eval_num_expr b;
  return $ na <= nb
eval_bool_expr (BoolEq a b) = do
  na <- eval_num_expr a;
  nb <- eval_num_expr b;
  return $ na == nb


increment_pos :: StateT InterpreterState IO ()
increment_pos = do
  state <- get;
  case is_stack state of
    ((pos,dlg):rest) -> do
      put (state { is_stack = (pos+1,dlg):rest })
    [] -> return ()
jump_to_pos :: Int -> StateT InterpreterState IO ()
jump_to_pos pos = do
  state <- get;
  case is_stack state of
    ((pos',dlg):rest) -> do
      put (state { is_stack = (pos,dlg):rest })
    [] -> return ()
    
push_dlg :: DialogID -> StateT InterpreterState IO ()
push_dlg id = do
  state <- get;
  put (state { is_stack = (0,id):(is_stack state) })
drop_item :: ItemID -> StateT InterpreterState IO ()
drop_item id = do
  state <- get;
  put (state { is_items = filter (/=id) . is_items $ state})

set_numvar :: NumVarID -> Int -> StateT InterpreterState IO ()
set_numvar id value = do
  state <- get;
  let new_num = (id,value): (filter ((/=id) . fst) . is_num $ state);
  put (state { is_num = new_num });
  return ()

set_strvar :: TextVarID -> String -> StateT InterpreterState IO ()
set_strvar id value = do
  state <- get;
  let new_text = (id,value): (filter ((/=id) . fst) . is_txt  $ state);
  put (state { is_txt = new_text });
  return ()

execute_instruction :: Instruction -> StateT InterpreterState IO ()
execute_instruction (Show bg) = do
  liftIO $ putStrLn ("INFO: Showing background " ++ show bg);
  increment_pos
execute_instruction (Goto map) = do
  liftIO $ putStrLn ("INFO: Going to map " ++ show map);
  state <- get;
  put (state { is_map = Just map });
  increment_pos  
execute_instruction (Run dlg) = do
  liftIO $ putStrLn ("INFO: Running Dialog " ++ show dlg);
  increment_pos;
  push_dlg dlg
execute_instruction (Drop item) = do
  liftIO $ putStrLn ("INFO: Dropping Item " ++ show item);
  drop_item item;
  increment_pos
execute_instruction (State chr state) = do
  liftIO $ putStrLn ("INFO: Setting Character " ++ show chr ++ " to state " ++ show state);
  increment_pos
execute_instruction (Position chr opos) = do
  liftIO $ putStrLn ("INFO: Setting Character " ++ show chr ++ " to position " ++ show opos);
  increment_pos
execute_instruction (Say ochr texpr) = do
  text <- eval_text_expr texpr;
  liftIO $ putStrLn (show ochr ++ ": " ++ text)
  increment_pos
execute_instruction (SetNum id nexpr) = do
  nval <- eval_num_expr nexpr;
  liftIO $ putStrLn ("INFO: Setting numeric variable " ++ show id ++ " to value " ++ show nval);
  set_numvar id nval;
  increment_pos
execute_instruction (SetStr id texpr) = do
  tval <- eval_text_expr texpr;
  liftIO $ putStrLn ("INFO: Setting text variable " ++ show id ++ " to value " ++ show tval);
  set_strvar id tval;
  increment_pos
execute_instruction (JumpCond cases) = do
   options <- mapM eval_text_expr . map fst $ cases;
   choice <- liftIO $ choose_from_list options;
   jump_to_pos . snd $ cases !! choice
execute_instruction (JumpBool bexpr pos) = do
  bval <- eval_bool_expr bexpr;
  if bval
    then jump_to_pos pos
    else increment_pos
execute_instruction (Jump pos) = jump_to_pos pos
execute_instruction (Remove) = do
  liftIO $ putStrLn ("INFO: Removing current item")
