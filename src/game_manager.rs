use sdl2::event::Event;
use sdl2::video::SwapInterval;
use sdl2::VideoSubsystem;

use gl::types::{GLfloat, GLint, GLsizei, GLuint, GLsizeiptr};

use nalgebra::base::{Matrix4, Vector2, Vector3, Vector4};
use nalgebra::{Translation3, Rotation3, Transform3};

use log::{error, warn, info, debug, trace};

use std::time::{Duration, Instant, SystemTime};
use std::os::raw::c_void;
use std::fs::File;
use std::io::{Read, Write};

use glyph_brush::{BrushAction, BrushError, GlyphBrushBuilder, GlyphBrush, Section};
use glyph_brush::rusttype::{Scale, Rect, point};

use crate::resource_loader::{ResourceManager, TextureID};
use crate::gpu_buffer::buffer_data;
use crate::graphics::{TempTexturedQuad, TempQuad, TexturedQuad, Quad, construct_transform_matrix};
use crate::statics::{
    APPLICATION_NAME, APPLICATION_INITIAL_SIZE, SAVE_FILE, START_BUTTON_SIZE, START_BUTTON_SPACING, SAVE_BUTTON_SIZE, SAVE_BUTTON_SPACING, SAVE_SAVES_BUTTON_SIZE, SAVE_SAVES_BUTTON_SPACING, SAVE_SAVES_COUNT, START_BUTTON_START,
    SAVE_SEPERATOR_SIZE
};
use crate::shader::Shader;
use crate::text::TextRenderer;
use crate::globals::RenderingGlobals;
use crate::config::RenderingConfig;
use crate::dialog::{DialogRenderer};
use crate::input::{InputManager, LogicalKey};
use crate::character::{CharacterRenderer, CharacterPosition};
use crate::option::OptionRenderer;
use crate::map::{MapManager};
use crate::script::{ScriptEngine,ScriptSave};
use crate::input::FrameInputs;


#[derive(Debug)]
pub enum GameState {
    Start(usize),
    // fields are (in order):
    // in side menu, position in side menu, (page of saves, position on visible)
    Save(bool, usize, (usize, usize)),
    Normal
}

#[derive(Debug)]
pub struct GameManagerState {

    start_screen_bg: TextureID,
    start_screen_fg: TextureID,
    start_screen_button: TextureID,

    start_screen_active_color: Vector4<GLfloat>,
    start_screen_inactive_color: Vector4<GLfloat>,

    save_screen_bg: TextureID,
    save_screen_button: TextureID,
    save_screen_saves_button: TextureID,

    save_screen_active_color: Vector4<GLfloat>,
    save_screen_inactive_color: Vector4<GLfloat>,


    saves: Vec<ScriptSave>,
    exit_requested: bool,
    state: GameState
}

pub struct GameRenderer<'a, 'b, 'c, 'd> {
    state: &'a mut GameManagerState,
    script_engine: &'b mut ScriptEngine<'c,'d>
}

impl GameManagerState {
    pub fn new(
        start_screen_bg: TextureID,
        start_screen_fg: TextureID,
        start_screen_button: TextureID,

        start_screen_active_color: Vector4<GLfloat>,
        start_screen_inactive_color: Vector4<GLfloat>,

        save_screen_bg: TextureID,
        save_screen_button: TextureID,
        save_screen_saves_button: TextureID,

        save_screen_active_color: Vector4<GLfloat>,
        save_screen_inactive_color: Vector4<GLfloat>,
    ) -> Self {


        let saves = match File::open(SAVE_FILE) {
                Ok(mut f) => {
                    let mut buf = Vec::new();
                    f.read_to_end(&mut buf);
                    let decoded: Result<Vec<ScriptSave>, _> = bincode::deserialize(&buf[..]);
                    decoded.ok()
                }
                _ => None
        };

        let saves = if let Some(saves) = saves { saves } else { Vec::new() };


        Self {
            start_screen_bg,
            start_screen_fg,
            start_screen_button,

            start_screen_active_color,
            start_screen_inactive_color,

            save_screen_bg,
            save_screen_button,
            save_screen_saves_button,

            save_screen_active_color,
            save_screen_inactive_color,

            saves: saves,
            exit_requested: false,
            state: GameState::Start(0)
        }
    }

    pub fn renderer<'a,'b,'c,'d>(&'a mut self, engine: &'b mut ScriptEngine<'c,'d>) -> GameRenderer<'a,'b,'c,'d> {
        GameRenderer{
            state: self,
            script_engine: engine
        }
    }

}


impl<'a,'b,'c,'d> GameRenderer<'a,'b,'c,'d> {

    pub fn exit_requested(&self) -> bool {
        self.state.exit_requested
    }

    pub fn update(&mut self, input: FrameInputs, delta_time: f32) {
        let new_state = match &mut self.state.state {
            GameState::Normal => {
                // if esc pressed, move to save state
                if input.input_keys.iter().find(|key| **key == LogicalKey::ESC).is_some() {
                    Some(GameState::Save(true, 0, (0, 0)))
                } else {
                    self.script_engine.update(input, delta_time); None
                }
            },
            &mut GameState::Start(ref mut pos) => {
                // move up and down, and change state to either normal or load game or exit

                // result holds the new state if any
                let mut result = None;

                for key in input.input_keys.iter() {
                    result = match key {
                        LogicalKey::UP => if *pos > 0 { *pos -= 1; None } else { None },
                        LogicalKey::DOWN => if *pos < 2 { *pos += 1; None } else { None },
                        LogicalKey::ENTER => {
                            match *pos {
                                0 => {
                                    self.script_engine.reset_state();
                                    Some(GameState::Normal)
                                },
                                1 => Some(GameState::Save(true, 0, (0, 0))),
                                2 => {
                                    self.state.exit_requested = true;
                                    Some(GameState::Start(*pos))
                                }
                                _ => unreachable!(),
                            }
                        }
                        _ => None
                    };

                    if result.is_some() {break};
                }

                result
            },
            GameState::Save(ref mut in_sidebar, ref mut sidebar_pos, (ref mut save_page, ref mut save_pos)) => {
                // move from sidebar to main bar, load and save state

                let mut result = None;

                for key in input.input_keys.iter() {
                    result = match key {
                        LogicalKey::UP => {
                            if *in_sidebar {
                                if *sidebar_pos > 0 {
                                    *sidebar_pos -= 1;
                                }
                            } else {
                                if *save_pos > 0 {
                                    *save_pos -=1;
                                } else if *save_page > 0 {
                                    *save_page -= 1;
                                }
                            }
                            None
                        }
                        LogicalKey::DOWN => {
                                if *in_sidebar {
                                    if *sidebar_pos < 3 {
                                        *sidebar_pos += 1;
                                    }
                                } else {
                                    if *save_pos < SAVE_SAVES_COUNT - 1 && (*save_page + *save_pos) < self.state.saves.len() {
                                        *save_pos +=1;
                                    } else if (*save_page + *save_pos) < self.state.saves.len() {
                                        *save_page += 1;
                                    }
                                }
                                None
                        }
                        LogicalKey::LEFT => {
                            *in_sidebar = true;
                            None
                        }
                        LogicalKey::RIGHT => {
                            *in_sidebar = false;
                            None
                        }
                        LogicalKey::ESC => {
                            Some(GameState::Normal)
                        }
                        LogicalKey::ENTER => {
                            if *in_sidebar {
                                match *sidebar_pos {
                                    // save
                                    0 => {
                                        if let Some(state) = self.script_engine.save_state() {
                                            let index = *save_page + *save_pos;
                                            // if the current index is an existing one, overwrite it
                                            // otherwise append
                                            if index >= self.state.saves.len() {
                                                self.state.saves.push(state);
                                            } else {
                                                self.state.saves[index] = state;
                                            }

                                            // then save to save file
                                            match File::create(SAVE_FILE) {
                                                Err(e) => println!("Err: could not save {:?}", e),
                                                Ok(mut f) => {
                                                    if let Ok(enc) = bincode::serialize(&self.state.saves) {
                                                        f.write_all(&enc);
                                                    }
                                                }
                                            }


                                            Some(GameState::Normal)
                                        } else { unreachable!() }
                                    },
                                    // load
                                    1 => {
                                            let index = *save_page + *save_pos;
                                            // if the current index is an existing one, load the state
                                            // otherwise do nothing
                                            if index >= self.state.saves.len() {
                                                None
                                            } else {
                                                self.script_engine.load_state(self.state.saves[index].clone());
                                                Some(GameState::Normal)
                                            }
                                    },
                                    // back - return to normal game state
                                    2 => {
                                        Some(GameState::Normal)
                                    }
                                    // exit - return to start screen
                                    3 => {
                                        Some(GameState::Start(0))
                                    }
                                    _ => None
                                }
                            } else {
                                None
                            }
                        },
                        _ => None
                    };

                    if result.is_some() {break};
                }

                result
            }
        };

        if let Some(state) = new_state {
            self.state.state = state;
        }



    }



    pub fn draw(&mut self) {
        match &self.state.state {
            &GameState::Normal => self.script_engine.draw(),
            &GameState::Start(st_pos) => {

                // draw background
                // ------------------
                let mut pos = Vector2::new(0.0, 0.0);
                let mut tex_pos = Vector2::new(0.0, 0.0);

                let mut tex_size = {
                    let (w,h) = self.state.start_screen_bg.get_dimensions(self.script_engine.resource_manager);
                    Vector2::new(w as f32, h as f32)
                };

                let mut size = Vector2::new(APPLICATION_INITIAL_SIZE.0 as GLfloat, APPLICATION_INITIAL_SIZE.1 as GLfloat);
                let color = Vector4::new(1.0, 1.0, 1.0, 1.0);

                TempTexturedQuad::new(
                    &pos,
                    &size,
                    0.0,
                    &tex_pos,
                    &tex_size,
                    0.0,
                    &color,
                    self.state.start_screen_bg
                ).draw(&mut self.script_engine.rendering_globals, self.script_engine.resource_manager);

                // draw menu options
                // ------------------

                size[0] = START_BUTTON_SIZE.0;
                size[1] = START_BUTTON_SIZE.1;

                let mut tex_size = {
                    let (w,h) = self.state.start_screen_button.get_dimensions(self.script_engine.resource_manager);
                    Vector2::new(w as f32, h as f32)
                };


                for (ind, txt) in ["New Game", "Load Game", "Exit"].iter().enumerate() {
                    pos[0] = 0.0;
                    pos[1] = START_BUTTON_START - (ind as f32) * (START_BUTTON_SIZE.1 + START_BUTTON_SPACING);

                    let color = if ind == st_pos {
                        &self.state.start_screen_active_color
                    } else {
                        &self.state.start_screen_inactive_color
                    };

                    TempTexturedQuad::new(
                        &pos,
                        &size,
                        0.0,
                        &tex_pos,
                        &tex_size,
                        0.0,
                        color,
                        self.state.start_screen_button
                    ).draw(&mut self.script_engine.rendering_globals, self.script_engine.resource_manager);

                    pos[0] += (APPLICATION_INITIAL_SIZE.0 as GLfloat)/2.0;
                    pos[1] = (APPLICATION_INITIAL_SIZE.1 as GLfloat)/2.0 - pos[1];
                    self.script_engine.text_renderer.add_centered_text(
                        txt, 0.3,
                        &pos,
                        &size,
                        &Vector4::new(1.0, 1.0, 1.0, 1.0)
                    );

                }


                self.script_engine.text_renderer.draw(
                    &mut self.script_engine.rendering_globals,
                    self.script_engine.resource_manager
                );


                pos[0] = 0.0;
                pos[1] = 0.0;

                tex_pos[0] = 0.0;
                tex_pos[1] = 0.0;

                let mut tex_size = {
                    let (w,h) = self.state.start_screen_bg.get_dimensions(self.script_engine.resource_manager);
                    Vector2::new(w as f32, h as f32)
                };

                size[0] = APPLICATION_INITIAL_SIZE.0 as GLfloat;
                size[1] = APPLICATION_INITIAL_SIZE.1 as GLfloat;

                let color = Vector4::new(1.0, 1.0, 1.0, 1.0);
                // draw foreground
                // ------------------
                let mut tex_size = {
                    let (w,h) = self.state.start_screen_bg.get_dimensions(self.script_engine.resource_manager);
                    Vector2::new(w as f32, h as f32)
                };
                TempTexturedQuad::new(
                    &pos,
                    &size,
                    0.0,
                    &tex_pos,
                    &tex_size,
                    0.0,
                    &color,
                    self.state.start_screen_fg
                ).draw(&mut self.script_engine.rendering_globals, self.script_engine.resource_manager);



            },
            &GameState::Save(in_sidebar, sidebar_pos, (save_page, save_pos)) => {
                // move from sidebar to main bar, load and save state
                // draw background
                // ------------------
                let mut pos = Vector2::new(0.0, 0.0);
                let mut tex_pos = Vector2::new(0.0, 0.0);

                let mut tex_size = {
                    let (w,h) = self.state.save_screen_bg.get_dimensions(self.script_engine.resource_manager);
                    Vector2::new(w as f32, h as f32)
                };

                let mut size = Vector2::new(APPLICATION_INITIAL_SIZE.0 as GLfloat, APPLICATION_INITIAL_SIZE.1 as GLfloat);
                let color = Vector4::new(1.0, 1.0, 1.0, 1.0);

                TempTexturedQuad::new(
                    &pos,
                    &size,
                    0.0,
                    &tex_pos,
                    &tex_size,
                    0.0,
                    &color,
                    self.state.save_screen_bg
                ).draw(&mut self.script_engine.rendering_globals, self.script_engine.resource_manager);

                // draw menu options
                // ------------------

                size[0] = SAVE_BUTTON_SIZE.0;
                size[1] = SAVE_BUTTON_SIZE.1;

                let mut tex_size = {
                    let (w,h) = self.state.save_screen_button.get_dimensions(self.script_engine.resource_manager);
                    Vector2::new(w as f32, h as f32)
                };


                for (ind, txt) in ["Save Game", "Load Game", "Back to Game", "Exit"].iter().enumerate() {

                    pos[0] = - (SAVE_SAVES_BUTTON_SIZE.0 + SAVE_SEPERATOR_SIZE + SAVE_BUTTON_SIZE.0)/2.0 + SAVE_BUTTON_SIZE.0/2.0;
                    pos[1] =
                        (START_BUTTON_SIZE.1 * 4.0 + START_BUTTON_SPACING * 3.0) / 2.0 - START_BUTTON_SIZE.1/2.0
                        - (ind as f32) * (START_BUTTON_SIZE.1 + START_BUTTON_SPACING);

                    let color = if in_sidebar && ind == sidebar_pos {
                        &self.state.start_screen_active_color
                    } else {
                        &self.state.start_screen_inactive_color
                    };

                    TempTexturedQuad::new(
                        &pos,
                        &size,
                        0.0,
                        &tex_pos,
                        &tex_size,
                        0.0,
                        color,
                        self.state.save_screen_button
                    ).draw(&mut self.script_engine.rendering_globals, self.script_engine.resource_manager);

                    pos[0] += (APPLICATION_INITIAL_SIZE.0 as GLfloat)/2.0;
                    pos[1] = (APPLICATION_INITIAL_SIZE.1 as GLfloat)/2.0 - pos[1];
                    self.script_engine.text_renderer.add_centered_text(
                        txt, 0.3,
                        &pos,
                        &size,
                        &Vector4::new(1.0, 1.0, 1.0, 1.0)
                    );

                }


                size[0] = SAVE_SAVES_BUTTON_SIZE.0;
                size[1] = SAVE_SAVES_BUTTON_SIZE.1;

                let mut tex_size = {
                    let (w,h) = self.state.save_screen_saves_button.get_dimensions(self.script_engine.resource_manager);
                    Vector2::new(w as f32, h as f32)
                };


                for (ind, save) in (0..SAVE_SAVES_COUNT).zip(
                    self.state.saves.iter().map(|x| Some(x)).chain(std::iter::repeat(None))
                        .skip(save_page)
                ) {
                    pos[0] =
                        (SAVE_SAVES_BUTTON_SIZE.0 + SAVE_SEPERATOR_SIZE + SAVE_BUTTON_SIZE.0)/2.0 -
                        (SAVE_SAVES_BUTTON_SIZE.0 / 2.0);
                    pos[1] =
                        (SAVE_SAVES_BUTTON_SIZE.1 * (SAVE_SAVES_COUNT as GLfloat) +
                         SAVE_SAVES_BUTTON_SPACING * ((SAVE_SAVES_COUNT - 1) as GLfloat)) / 2.0
                        - SAVE_SAVES_BUTTON_SIZE.1/2.0
                        - (ind as f32) * (SAVE_SAVES_BUTTON_SIZE.1 + SAVE_SAVES_BUTTON_SPACING);

                    let color = if ind == save_pos {
                        &self.state.start_screen_active_color
                    } else {
                        &self.state.start_screen_inactive_color
                    };

                    TempTexturedQuad::new(
                        &pos,
                        &size,
                        0.0,
                        &tex_pos,
                        &tex_size,
                        0.0,
                        color,
                        self.state.save_screen_button
                    ).draw(&mut self.script_engine.rendering_globals, self.script_engine.resource_manager);

                    if let Some(save_state) = save {
                        pos[0] += (APPLICATION_INITIAL_SIZE.0 as GLfloat)/2.0 - size[0]/2.0 + 10.0;
                        pos[1] = (APPLICATION_INITIAL_SIZE.1 as GLfloat)/2.0 - pos[1] - size[1]/2.0 + 10.0;

                        let text = format!("Save File {:?}\n Date: {:?}",
                                           save_page + ind + 1,
                                           save_state.time);

                        self.script_engine.text_renderer.add_text(
                            &text, 0.3,
                            &pos,
                            &size,
                            &Vector4::new(1.0, 1.0, 1.0, 1.0)
                        );

                    }


                }


                self.script_engine.text_renderer.draw(
                    &mut self.script_engine.rendering_globals,
                    self.script_engine.resource_manager
                );



            }
        }
    }

}
