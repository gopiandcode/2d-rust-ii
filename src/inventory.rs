use crate::text::TextRenderer;
use crate::graphics::{TempTexturedQuad, TempQuad, TexturedQuad, Quad};
use crate::globals::RenderingGlobals;
use crate::statics::{APPLICATION_INITIAL_SIZE, INVENTORY_BOX_SIZE, INVENTORY_BOX_SPACING, INVENTORY_ITEM_SIZE};
use crate::input::{FrameInputs, LogicalKey};
use crate::resource_loader::{ResourceManager, ResourceError};
use crate::character::{CharacterRenderer, CharacterPosition, CharacterID};
use crate::config::RenderingConfig;
use crate::dialog::DialogRenderer;
use crate::map::{MapManager, SelectID};
use crate::bg::BackgroundManager;
use crate::state::StateManager;
use crate::option::OptionRenderer;

use std::path::Path;
use std::vec::IntoIter;
use std::fs::File;
use std::io::Read;
use std::collections::HashMap;
use std::time::SystemTime;

use gl::types::{GLfloat, GLint, GLsizei, GLuint, GLsizeiptr};
use nalgebra::base::{Vector4, Vector3, Vector2};
use regex::Regex;
use chrono::{Utc, DateTime};
use serde::{Serialize, Deserialize};

use crate::bg::BackgroundID;
use crate::character::{StateID};
use crate::resource_loader::TextureID;



pub struct InventoryRendererState {
    item_box_tex: TextureID,
    arrow_tex: TextureID,

    base_color: Vector4<GLfloat>,

    arrow_color: Vector4<GLfloat>,
    inactive_arrow_color: Vector4<GLfloat>,

    item_box_color: Vector4<GLfloat>,
    active_item_box_color: Vector4<GLfloat>,


    page: usize,
    cursor_pos: usize,
}

impl InventoryRendererState {

    pub fn new(
        item_box_tex: TextureID,
        arrow_tex: TextureID,

        base_color: Vector4<GLfloat>,

        arrow_color: Vector4<GLfloat>,
        inactive_arrow_color: Vector4<GLfloat>,

        item_box_color: Vector4<GLfloat>,
        active_item_box_color: Vector4<GLfloat>,
    ) -> Self {
        Self {
            item_box_tex, arrow_tex, base_color, arrow_color,
            inactive_arrow_color, item_box_color, active_item_box_color,
            page: 0,
            cursor_pos: 0
        }
    }


    pub fn renderer<'a,'b>(&'b mut self, map: &'a mut MapManager) -> InventoryRenderer<'a,'b> {
        InventoryRenderer {
            state: self,
            map_manager: map
        }
    }


}


pub struct InventoryRenderer<'a, 'b> {
    state: &'b mut InventoryRendererState,
    map_manager: &'a mut MapManager
}

impl<'a,'b> InventoryRenderer<'a,'b> {
    pub fn update(&mut self, input: FrameInputs, delta_time: f32) {
        for input in input.input_keys.iter() {
            //   the board is arranged as follows
            //
            //     1  2  3
            //  0  4  5  6  10
            //     7  8  9
            //
            match input {
                // if not on first row, or on right arrow, move up
                &LogicalKey::UP => if self.state.cursor_pos > 3 && self.state.cursor_pos != 10 { self.state.cursor_pos -= 3; }
                // if not on last row, or on left arrow, move down
                &LogicalKey::DOWN => if self.state.cursor_pos < 7 && self.state.cursor_pos != 0 { self.state.cursor_pos += 3; }
                // if on first column, move to left arrow
                &LogicalKey::LEFT => if self.state.cursor_pos == 4  || self.state.cursor_pos == 7 {
                    self.state.cursor_pos = 0;
                // if on right arrow
                } else if self.state.cursor_pos == 10 {
                    self.state.cursor_pos = 6;
                // otherwise, if not on left arrow
                } else if self.state.cursor_pos != 0 {
                    self.state.cursor_pos -= 1;
                }
                // if on last column, move to right arrow
                &LogicalKey::RIGHT => if self.state.cursor_pos == 3  || self.state.cursor_pos == 6  {
                    self.state.cursor_pos = 10;
                    // if on left arrow, move to middle row
                } else if self.state.cursor_pos == 0 {
                    self.state.cursor_pos = 4;
                    // otherwise, if not on left arrow
                } else if self.state.cursor_pos != 10 {
                    self.state.cursor_pos += 1;
                }
                &LogicalKey::ENTER => {
                    // if on left arrow
                    if self.state.cursor_pos == 0 {
                        // decrement position if possible
                        if self.state.page > 0 {
                            self.state.page -= 1;
                        }
                    } else if self.state.cursor_pos == 10 {
                        if (self.state.page+1) * 9 < self.map_manager.inventory_size() {
                            self.state.page += 1;
                        }
                    }
                }
                _ => ()
            }
        }
    }


    pub fn draw(&mut self, globals: &mut RenderingGlobals, resource_manager: &mut ResourceManager) {

        let mut pos = Vector2::new(0.0, 0.0);
        let tex_pos = Vector2::new(0.0, 0.0);

        let mut tex_size = {
            let (w,h) = self.state.item_box_tex.get_dimensions(resource_manager);
            Vector2::new(w as f32, h as f32)
        };

        let size = Vector2::new(APPLICATION_INITIAL_SIZE.0 as GLfloat, APPLICATION_INITIAL_SIZE.1 as GLfloat);

        // draw the base overlay
        TempQuad::new(
            &pos, &size, 0.0, &self.state.base_color
        ).draw(globals, resource_manager);


        // set size to option box size
        let size = Vector2::new(INVENTORY_BOX_SIZE, INVENTORY_BOX_SIZE);

        // move through each row and colums
        for ((ind, (row,col)), item) in [
            (0,0),
            (0,1),
            (0,2),
            (1,0),
            (1,1),
            (1,2),
            (2,0),
            (2,1),
            (2,2),
        ].iter().enumerate().zip(
            // iterate through the inventory
            self.map_manager.inventory().iter()
                // iterate as a sequence of options for each entry, followed by infinite nones
                .map(|it| Some(it)).chain(std::iter::repeat(None))
                // skip the number of pages
                .skip(self.state.page * 9)
        ) {

            pos[0] =  (- INVENTORY_BOX_SIZE - INVENTORY_BOX_SPACING) + (INVENTORY_BOX_SIZE + INVENTORY_BOX_SPACING) * (*col as f32);
            pos[1] = (INVENTORY_BOX_SIZE + INVENTORY_BOX_SPACING) - (INVENTORY_BOX_SIZE + INVENTORY_BOX_SPACING) * (*row as f32);

            let color = if self.state.cursor_pos == ind + 1 {
                &self.state.active_item_box_color
            } else {
                &self.state.item_box_color
            };

            TempTexturedQuad::new(
                &pos,
                &size,
                0.0,
                &tex_pos,
                &tex_size,
                0.0,
                color,
                self.state.item_box_tex
            ).draw(globals, resource_manager);

            if let Some(inv_it) = item {
                let mut tex_size = {
                    let (w,h) = inv_it.inv_tex.get_dimensions(resource_manager);
                    Vector2::new(w as f32, h as f32)
                };
                let item_size = Vector2::new(INVENTORY_ITEM_SIZE, INVENTORY_ITEM_SIZE);

                TempTexturedQuad::new(
                    &pos,
                    &item_size,
                    0.0,
                    &tex_pos,
                    &tex_size,
                    0.0,
                    &Vector4::new(1.0, 1.0, 1.0, 1.0),
                    inv_it.inv_tex
                ).draw(globals, resource_manager);
            }

        }

        pos[0] =  (- INVENTORY_BOX_SIZE - INVENTORY_BOX_SPACING) + (INVENTORY_BOX_SIZE + INVENTORY_BOX_SPACING) * -1.0;
        pos[1] = (INVENTORY_BOX_SIZE + INVENTORY_BOX_SPACING) - (INVENTORY_BOX_SIZE + INVENTORY_BOX_SPACING) * 1.0;

        let tex_size = {
            let (w,h) = self.state.arrow_tex.get_dimensions(resource_manager);
            Vector2::new(w as f32, h as f32)
        };

        let color = if self.state.cursor_pos == 0 {
            &self.state.arrow_color
        } else {
            &self.state.inactive_arrow_color
        };

        TempTexturedQuad::new(
            &pos,
            &size,
            0.0,
            &tex_pos,
            &tex_size,
            0.0,
            color,
            self.state.arrow_tex
        ).draw(globals, resource_manager);


        pos[0] =  (- INVENTORY_BOX_SIZE - INVENTORY_BOX_SPACING) + (INVENTORY_BOX_SIZE + INVENTORY_BOX_SPACING) * 3.0;
        pos[1] = (INVENTORY_BOX_SIZE + INVENTORY_BOX_SPACING) - (INVENTORY_BOX_SIZE + INVENTORY_BOX_SPACING) * 1.0;



        let color = if self.state.cursor_pos == 10 {
            &self.state.arrow_color
        } else {
            &self.state.inactive_arrow_color
        };

        TempTexturedQuad::new(
            &pos,
            &size,
            180.0_f32.to_radians(),
            &tex_pos,
            &tex_size,
            0.0,
            color,
            self.state.arrow_tex
        ).draw(globals, resource_manager);



    }


}


