pub const APPLICATION_NAME: &str = "Game";
pub const APPLICATION_INITIAL_SIZE: (i32, i32) = (1280
, 720);
pub const CHARACTER_SIZE: (usize,usize) = (650, 950);
pub const CHARACTER_POSITIONS: (i32, i32, i32) = (-APPLICATION_INITIAL_SIZE.0/2 + (CHARACTER_SIZE.0/2) as i32, 0, APPLICATION_INITIAL_SIZE.0/2 - (CHARACTER_SIZE.0/2) as i32);
pub const CHARACTER_SPACING: i32 = 200;
pub const DIALOG_CORNER_SIZE: (f32, f32) = (30.0, 30.0);
pub const OPTION_CORNER_SIZE: (f32, f32) = (30.0, 30.0);
pub const OPTION_BOX_MARGIN: f32 = 200.0;
pub const OPTION_BOX_SIZE: f32 = 100.0;
pub const OPTION_BOX_SPACING: f32 = 80.0;
pub const INVENTORY_BOX_SIZE: f32 = 300.0;
pub const INVENTORY_ITEM_SIZE: f32 = 280.0;
pub const INVENTORY_BOX_SPACING: f32 = 50.0;
pub const SAVE_FILE: &str = "./res/saves.bin";

pub const START_BUTTON_SIZE: (f32, f32) = (800.0, 100.0);
pub const START_BUTTON_SPACING: f32 = 50.0;
pub const START_BUTTON_START: f32  = -100.0;
pub const SAVE_BUTTON_SIZE: (f32, f32) = (400.0, 100.0);
pub const SAVE_BUTTON_SPACING: f32 = 50.0;
pub const SAVE_SAVES_BUTTON_SIZE: (f32, f32) = (800.0, 150.0);
pub const SAVE_SAVES_BUTTON_SPACING: f32 = 50.0;
pub const SAVE_SAVES_COUNT: usize = 5;
pub const SAVE_SEPERATOR_SIZE: f32 = 100.0;

