#version 330 core
layout (location = 0) in vec2 vp;

out vec2 tex_coords;

uniform mat4 transform;
uniform mat3 tex_transform;

uniform vec2 tex_size;
uniform vec2 size;

void main() {
     tex_coords = ((tex_transform * vec3(vp, 1.0)).xy)/tex_size;
     gl_Position = (transform * vec4(vp, 0.0, 1.0))/vec4(size, 1.0, 1.0);
}
