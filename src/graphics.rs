use std::cell::RefCell;
use std::rc::Rc;
use std::ffi::{CString, CStr};
use std::ptr;
use std::path::Path;

use sdl2::event::Event;

use gl::types::{GLfloat, GLint, GLsizei, GLuint, GLsizeiptr};

use nalgebra::base::{Matrix4, Matrix3, Vector2, Vector3, Vector4};
use nalgebra::{Translation3, Rotation3, Transform3, Translation2, Rotation2, Transform2};

use log::{error, warn, info, debug, trace};

use std::time::{Duration, Instant, SystemTime};
use std::os::raw::c_void;

use crate::resource_loader;
use crate::statics::{APPLICATION_INITIAL_SIZE};
use crate::texture::Texture;
use crate::resource_loader::ResourceManager;
use crate::resource_loader::{ShaderID, TextureID};

use crate::shader::Shader;
use crate::globals::{RenderingGlobals};



pub fn construct_transform_matrix(
    size_x: GLfloat, size_y: GLfloat,
    position_x: GLfloat, position_y: GLfloat,
    rotation: GLfloat
) -> Matrix4<GLfloat> {

    let position_vector = Translation3::from_vector(Vector3::new(
        position_x,
        position_y,
        0.0
    ));
    let center_prime = Translation3::from_vector(Vector3::new(0.0, 0.0, 0.0));
    let rotation = Rotation3::from_axis_angle(&Vector3::z_axis(), -rotation);
    let center = Translation3::from_vector(Vector3::new(0.0, 0.0, 0.0));
    let scaling_matrix = Transform3::from_matrix_unchecked(
        Matrix4::new_nonuniform_scaling(
            &Vector3::new(size_x, size_y, 1.0)
        )
    );

    let model =
        position_vector *
        center_prime *
        rotation *
        center *
        scaling_matrix;

    *model.matrix()
}


fn construct_transform2d_matrix(
    size_x: GLfloat, size_y: GLfloat,
    position_x: GLfloat, position_y: GLfloat,
    rotation: GLfloat
) -> Matrix3<GLfloat> {
    let position_vector = Translation2::from_vector(Vector2::new(
        position_x,
        position_y,
    ));
    let center_prime = Translation2::from_vector(Vector2::new(0.5 * size_x, 0.5 * size_y));
    let rotation = Rotation2::new(-rotation);
    let center = Translation2::from_vector(Vector2::new(-0.5 * size_x, -0.5 * size_y));
    let scaling_matrix = Transform2::from_matrix_unchecked(
        Matrix3::new_nonuniform_scaling(
            &Vector2::new(size_x, size_y)
        )
    );

    let model =
        position_vector *
        center_prime *
        rotation *
        center *
        scaling_matrix;

    *model.matrix()
}

#[derive(Debug, Clone)]
pub struct Quad {
    pub position: Vector2<GLfloat>,
    pub size: Vector2<GLfloat>,
    pub rotation: GLfloat,
    pub color: Vector4<GLfloat>,
}


impl Quad {
    pub fn new(
        position: Vector2<GLfloat>,
        size: Vector2<GLfloat>,
        rotation: GLfloat,
        color: Vector4<GLfloat>
    ) -> Self {
        Self {
            position: Vector2::new(
                position.x,
                position.y
            ),
            size,
            rotation,
            color
        }
    }




    pub fn draw(
        &self, globals: &mut RenderingGlobals, resource_manager: &mut ResourceManager
    ) {
        let size_x = self.size.x/(APPLICATION_INITIAL_SIZE.0 as f32);
        let size_y = self.size.y/(APPLICATION_INITIAL_SIZE.1 as f32);
        let position_x = 2.0 * self.position.x/(APPLICATION_INITIAL_SIZE.0 as f32);
        let position_y = 2.0 * self.position.y/(APPLICATION_INITIAL_SIZE.1 as f32);

        let model = construct_transform_matrix(
            size_x, size_y,
            position_x, position_y,
            self.rotation
        );

        unsafe {
            let (vao, vbo) = globals.GPU_QUAD;

            let FILL_SHADER = &mut resource_manager[globals.FILL_SHADER];

            FILL_SHADER.enable();
                FILL_SHADER.set_matrix4f(
                    &globals.TRANSFORM_TAG, &model
                );
                FILL_SHADER.set_vector4f(
                    &globals.COLOR_TAG, &self.color
                );
                gl::BindVertexArray(vao);
                    gl::DrawArrays(gl::TRIANGLES, 0, 6);
                gl::BindVertexArray(0);
            gl::UseProgram(0);
        }

    }
}




impl Default for Quad {

    fn default() -> Self {
        Self::new(
            Vector2::new(0.0, 0.0),
            Vector2::new(200.0, 200.0),
            0.0,
            Vector4::new(
                0.4, 0.6, 0.3, 0.8
            )
        )
    }
}



#[derive(Debug, Clone)]
pub struct TempQuad<'a> {
    pub position: &'a Vector2<GLfloat>,
    pub size: &'a Vector2<GLfloat>,
    pub rotation: GLfloat,
    pub color: &'a Vector4<GLfloat>,
}


impl<'a> TempQuad<'a> {
    pub fn new(
        position: &'a Vector2<GLfloat>,
        size: &'a Vector2<GLfloat>,
        rotation: GLfloat,
        color: &'a Vector4<GLfloat>
    ) -> Self {
        Self {
            position,
            size,
            rotation,
            color
        }
    }




    pub fn draw(
        &self, globals: &mut RenderingGlobals, resource_manager: &mut ResourceManager
    ) {
        let size_x = self.size.x/(APPLICATION_INITIAL_SIZE.0 as f32);
        let size_y = self.size.y/(APPLICATION_INITIAL_SIZE.1 as f32);
        let position_x = 2.0 * self.position.x/(APPLICATION_INITIAL_SIZE.0 as f32);
        let position_y = 2.0 * self.position.y/(APPLICATION_INITIAL_SIZE.1 as f32);

        let model = construct_transform_matrix(
            size_x, size_y,
            position_x, position_y,
            self.rotation
        );

        unsafe {
            let (vao, vbo) = globals.GPU_QUAD;

            let FILL_SHADER = &mut resource_manager[globals.FILL_SHADER];

            FILL_SHADER.enable();
                FILL_SHADER.set_matrix4f(
                    &globals.TRANSFORM_TAG, &model
                );
                FILL_SHADER.set_vector4f(
                    &globals.COLOR_TAG, &self.color
                );
                gl::BindVertexArray(vao);
                    gl::DrawArrays(gl::TRIANGLES, 0, 6);
                gl::BindVertexArray(0);
            gl::UseProgram(0);
        }

    }
}




#[derive(Debug)]
pub struct TexturedQuad {
    pub position: Vector2<GLfloat>,
    pub size: Vector2<GLfloat>,
    pub rotation: GLfloat,
    pub tex_position: Vector2<GLfloat>,
    pub tex_size: Vector2<GLfloat>,
    pub tex_rotation: GLfloat,
    pub tex_color: Vector4<GLfloat>,
    pub texture: TextureID,
}



impl TexturedQuad {

    pub fn new(
        position: Vector2<GLfloat>,
        size: Vector2<GLfloat>,
        rotation: GLfloat,
        tex_position: Vector2<GLfloat>,
        tex_size: Vector2<GLfloat>,
        tex_rotation: GLfloat,
        tex_color: Vector4<GLfloat>,
        texture: TextureID
    ) -> Self {
        Self {
            position,
            size,
            rotation,
            tex_position,
            tex_size,
            tex_rotation,
            tex_color,
            texture
        }
    }



    pub fn test_quad(resource_manager: &mut ResourceManager) -> Self {
        let default_tex = resource_manager.load_texture("./res/images/empty.png")
            .expect("Could not load the default texture");
        Self::new(
            // position
            Vector2::new(0.0, 0.0),
            // size
            Vector2::new(640.0, 640.0),
            // rotation
            0.0,
            // tex_position
            Vector2::new(0.0, 0.0),
            // tex_size
            Vector2::new(640.0, 640.0),
            // tex_rotation
            0.0,
            // tex_color
            Vector4::new(1.0, 1.0, 1.0, 1.0),
            // texture
            default_tex
        )

    }


    pub fn draw(&self, globals: &mut RenderingGlobals, resource_manager: &mut ResourceManager) {
        let size_x = self.size.x;
        let size_y = self.size.y;
        let position_x = 2.0 * self.position.x;
        let position_y = 2.0 * self.position.y;
        let tex_position_x = (self.tex_position.x + (self.tex_size.x as f32)/2.0);
        let tex_position_y = (self.tex_position.y + (self.tex_size.y as f32)/2.0);
        let tex_size_x = self.tex_size.x as f32/2.0;
        let tex_size_y = - self.tex_size.y as f32/2.0;




        let transform = construct_transform_matrix(
            size_x, size_y,
            position_x, position_y,
            self.rotation
        );
        let tex_transform = construct_transform2d_matrix(
            tex_size_x, tex_size_y,
            tex_position_x, tex_position_y,
            self.tex_rotation
        );


            let (vao, vbo) = globals.GPU_QUAD;

        let (texture_width,texture_height) = self.texture.get_dimensions(resource_manager);
        self.texture.bind(resource_manager, 0);


            let TEX_SHADER = &mut resource_manager[globals.TEX_SHADER];

            TEX_SHADER.enable();
                TEX_SHADER.set_vector2f(
                    &globals.SIZE_TAG, &Vector2::new(
                        APPLICATION_INITIAL_SIZE.0 as f32,
                        APPLICATION_INITIAL_SIZE.1 as f32
                    )
                );
                TEX_SHADER.set_vector2f(
                    &globals.TEX_SIZE_TAG, &Vector2::new(
                        texture_width as f32,
                        texture_height as f32
                    )
                );

                TEX_SHADER.set_matrix4f(
                    &globals.TRANSFORM_TAG, &transform
                );
                TEX_SHADER.set_matrix3f(
                    &globals.TEX_TRANSFORM_TAG, &tex_transform
                );
                TEX_SHADER.set_vector4f(
                    &globals.SPRITE_COLOR_TAG, &self.tex_color
                );

                TEX_SHADER.set_int(
                    &globals.IMAGE_TAG, 0
                );

        unsafe {
                gl::BindVertexArray(vao);
                    gl::EnableVertexAttribArray(0);
                    gl::DrawArrays(gl::TRIANGLES, 0, 6);
                gl::BindVertexArray(0);
            gl::UseProgram(0);
        }


    }

}




#[derive(Debug)]
pub struct TempTexturedQuad<'a> {
    pub position: &'a Vector2<GLfloat>,
    pub size: &'a Vector2<GLfloat>,
    pub rotation: GLfloat,
    pub tex_position: &'a Vector2<GLfloat>,
    pub tex_size: &'a Vector2<GLfloat>,
    pub tex_rotation: GLfloat,
    pub tex_color: &'a Vector4<GLfloat>,
    pub texture: TextureID,
}



impl<'a> TempTexturedQuad<'a> {

    pub fn new(
        position: &'a Vector2<GLfloat>,
        size: &'a Vector2<GLfloat>,
        rotation: GLfloat,
        tex_position: &'a Vector2<GLfloat>,
        tex_size: &'a Vector2<GLfloat>,
        tex_rotation: GLfloat,
        tex_color: &'a Vector4<GLfloat>,
        texture: TextureID
    ) -> Self {
        Self {
            position,
            size,
            rotation,
            tex_position,
            tex_size,
            tex_rotation,
            tex_color,
            texture
        }
    }



    pub fn draw(&self, globals: &mut RenderingGlobals, resource_manager: &mut ResourceManager) {
        let size_x = self.size.x;
        let size_y = self.size.y;
        let position_x = 2.0 * self.position.x;
        let position_y = 2.0 * self.position.y;
        let tex_position_x = (self.tex_position.x + (self.tex_size.x as f32)/2.0);
        let tex_position_y = (self.tex_position.y + (self.tex_size.y as f32)/2.0);
        let tex_size_x = self.tex_size.x as f32/2.0;
        let tex_size_y = - self.tex_size.y as f32/2.0;




        let transform = construct_transform_matrix(
            size_x, size_y,
            position_x, position_y,
            self.rotation
        );
        let tex_transform = construct_transform2d_matrix(
            tex_size_x, tex_size_y,
            tex_position_x, tex_position_y,
            self.tex_rotation
        );


            let (vao, vbo) = globals.GPU_QUAD;

        let (texture_width,texture_height) = self.texture.get_dimensions(resource_manager);
        self.texture.bind(resource_manager, 0);


            let TEX_SHADER = &mut resource_manager[globals.TEX_SHADER];

            TEX_SHADER.enable();
                TEX_SHADER.set_vector2f(
                    &globals.SIZE_TAG, &Vector2::new(
                        APPLICATION_INITIAL_SIZE.0 as f32,
                        APPLICATION_INITIAL_SIZE.1 as f32
                    )
                );
                TEX_SHADER.set_vector2f(
                    &globals.TEX_SIZE_TAG, &Vector2::new(
                        texture_width as f32,
                        texture_height as f32
                    )
                );

                TEX_SHADER.set_matrix4f(
                    &globals.TRANSFORM_TAG, &transform
                );
                TEX_SHADER.set_matrix3f(
                    &globals.TEX_TRANSFORM_TAG, &tex_transform
                );
                TEX_SHADER.set_vector4f(
                    &globals.SPRITE_COLOR_TAG, &self.tex_color
                );

                TEX_SHADER.set_int(
                    &globals.IMAGE_TAG, 0
                );

        unsafe {
                gl::BindVertexArray(vao);
                    gl::EnableVertexAttribArray(0);
                    gl::DrawArrays(gl::TRIANGLES, 0, 6);
                gl::BindVertexArray(0);
            gl::UseProgram(0);
        }


    }

}

