#!/bin/bash

rm *.AppImage
rm -rf VN.AppDir

cargo build --release

mkdir -p VN.AppDir
cp -r res VN.AppDir
cp target/release/gl-game VN.AppDir/AppRun

cd VN.AppDir
mv res/logo.png VN.png

echo '[Desktop Entry]' > VN.desktop
echo 'Name=VN' >> VN.desktop
echo 'Exec=VN' >> VN.desktop
echo 'Icon=VN' >> VN.desktop
echo 'Type=Application' >> VN.desktop
echo 'Categories=Game;' >> VN.desktop

cd ..
appimagetool-x86_64.AppImage VN.AppDir
