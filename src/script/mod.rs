use crate::text::TextRenderer;
use crate::graphics::{TexturedQuad, Quad};
use crate::globals::RenderingGlobals;
use crate::statics::{APPLICATION_INITIAL_SIZE};
use crate::input::{FrameInputs, LogicalKey};
use crate::resource_loader::{ResourceManager, ResourceError};
use crate::character::{CharacterRenderer, CharacterPosition, CharacterID};
use crate::config::RenderingConfig;
use crate::dialog::{DialogRenderer, DialogRendererState};
use crate::map::{MapManager, SelectID};
use crate::bg::BackgroundManager;
use crate::state::StateManager;
use crate::option::{OptionRenderer, OptionRendererState};

use std::path::Path;
use std::vec::IntoIter;
use std::fs::File;
use std::io::Read;
use std::collections::HashMap;
use std::time::SystemTime;

use gl::types::{GLfloat, GLint, GLsizei, GLuint, GLsizeiptr};
use nalgebra::base::{Vector4, Vector3, Vector2};
use regex::Regex;
use chrono::{Utc, DateTime};
use serde::{Serialize, Deserialize};

pub mod lexer;
pub mod parser;
pub mod compiler;

use crate::script::parser::{ScriptParser, ParseError, parse_program};
use crate::script::lexer::{ScriptLexer, LexError, ScriptToken};
use crate::script::compiler::{DialogOperation, TempState, compile_program, CompileError, MatchText};
use crate::bg::BackgroundID;
use crate::map::{MapID, ItemID};
use crate::character::{StateID};
use crate::inventory::{InventoryRendererState, InventoryRenderer};



use std::fmt::Debug;
use std::str;
use std::collections::HashSet;

/*
SCRIPTING SYNTAX - DEFINED AD-HOC
-----------------------------------


PROGRAM            := STATEMENT*

STATEMENT          := DECL;
| DIALOG

DECL               := CHARACTER_DECL
|  VARIABLE_DECL

CHARACTER_DECL     := character <NAME> : "<FILE>" { <STATE_NAME>* }

VARIABLE_DECL      := variable int <NAME> := <VALUE>
| variable text <NAME> := "<VALUE>"
;

DIALOG             := dialog <NAME> {
DIALOG_DECL*
DIALOG_BODY*
}

DIALOG_BODY        := MODIFY_STATE_OP
                    | MODIFY_POSITION_OP
                    | SAY_OP
                    | INPUT_OP
                    | ASSIGNMENT_OP
                    | WHILE_OP
                    | IF_OP
;


DIALOG_DECL := VARIABLE_DECL

MODIFY_STATE_OP    :=  state <NAME> <STATE_NAME>;

MODIFY_POSITION_OP := position <NAME> <LEFT|RIGHT|CENTER|OFFSCREEN>;

SAY_OP             := speak <NAME>  SPEAKABLE_VALUE
                    | info SPEAKABLE_VALUE
;

WHILE_OP := while BOOL_EXPR {  DIALOG BODY   }

IF_OP := if BOOL_EXPR { DIALOG_BODY* } (else { DIALOG_BODY* })?


SPEAKABLE_VALUE := "<TEXT>" | <VARIABLE> | format("<TEXT>" (, <VARIABLE>|"<TEXT>")*) 

INPUT_OP := input {
            INPUT_MATCH => DIALOG_BODY *
}

INPUT_MATCH := "  " | variable

ASSIGNMENT_OP := variable <NAME> := EXPR | "<TEXT>"

EXPR := FACTOR_EXPR + FACTOR_EXPR | FACTOR_EXPR - FACTOR_EXPR | FACTOR_EXPR
FACTOR_EXPR := BASE_EXPR * BASE_EXPR | BASE_EXPR / BASE_EXPR | BASE_EXPR
BASE_EXPR := VALUE | (EXPR)

BOOL_EXPR := EXPR < EXPR | EXPR > EXPR | EXPR <= EXPR | EXPR >= EXPR | EXPR == EXPR

*/



#[derive(Serialize,Deserialize,Debug,Copy, Clone)]
pub struct DialogID(usize);
#[derive(Serialize,Deserialize,Debug,Copy, Clone)]
pub struct InstructionID(usize);

impl InstructionID {
    fn incr(&mut self) {
        self.0 += 1;
    }
}



pub struct ScriptEngine<'a, 'b> {
    rendering_config: &'b RenderingConfig,
    pub resource_manager: &'a mut ResourceManager,
    pub text_renderer: TextRenderer<'b>,
    pub rendering_globals: RenderingGlobals,
    dialog_renderer: DialogRendererState,
    option_renderer: OptionRendererState,
    inventory_renderer: InventoryRendererState,
    script_state: Option<ScriptState>,
    hdpi: f32,

    initial_state: Option<ScriptSave>,
}

pub struct ScriptState {

    code_hash: u64,
    decl_hash: u64,

    code: Vec<Vec<DialogOperation>>,

    character_renderer: CharacterRenderer,
    map_manager: MapManager,
    bg_manager: BackgroundManager,
    state_manager: StateManager,
    map_bindings: HashMap<SelectID, DialogID>,

    stack: Vec<(DialogID, InstructionID)>,
    executing_async: bool,
    timeout: Option<f32>,

    in_inv: bool

}


#[derive(Serialize,Deserialize,Debug,Clone)]
pub struct ScriptSave {

    // If codes don't match print warning.
    code_hash: u64,
    // If declarations don't match fail.
    decl_hash: u64,

    pub time: DateTime<Utc>,

    characters_on_screen: Vec<(CharacterID, CharacterPosition)>,
    character_states: Vec<(CharacterID, StateID)>,

    current_map: Option<MapID>,
    map_states: Vec<(MapID, Vec<ItemID>)>,
    map_inventory: Vec<ItemID>,

    bg_stack: Vec<Option<BackgroundID>>,

    text_variables: Vec<String>,
    numeric_variables: Vec<i32>,

    exec_stack: Vec<(DialogID, InstructionID)>,
}

#[derive(Debug)]
pub enum ScriptError {
    CompileError(CompileError),
    ParseError(ParseError),
    LexError(LexError),
    ResourceError(ResourceError),
    ScriptError(String),
    LoadError(String),
}


impl<'a,'b> ScriptEngine<'a,'b> {

    pub fn reset_state(&mut self) {
        if let Some(state) = self.initial_state.as_ref() {
            self.load_state(state.clone());
        }
    }

    pub fn save_state(&self) -> Option<ScriptSave> {
        let state = self.script_state.as_ref()?;
        let (characters_on_screen, character_states) = state.character_renderer.save_state();
        let (map_states, map_inventory, current_map) = state.map_manager.save_state();
        let bg_stack = state.bg_manager.save_state();
        let (text_variables, numeric_variables) = state.state_manager.save_state();
        let exec_stack = state.stack.clone();
        let time = Utc::now();
        let code_hash = state.code_hash;
        let decl_hash = state.decl_hash;

        Some(
            ScriptSave {
                code_hash,
                decl_hash,
                time,
                characters_on_screen,
                character_states,
                map_states,
                map_inventory,
                bg_stack,
                text_variables,
                numeric_variables,
                exec_stack,
                current_map
            }
        )
    }

    pub fn load_state(&mut self, sstate: ScriptSave) -> Result<(), ScriptError> {
        let state = self.script_state.as_mut().ok_or(ScriptError::LoadError(
            String::from("Can not load state before loading script")
        ))?;

        (
            if sstate.decl_hash != state.decl_hash {
                Err(ScriptError::LoadError(
                    format!("Saved State Declarations do not match current code {:?} != {:?}", sstate.decl_hash, state.decl_hash )
                ))
            } else {
                if sstate.code_hash != state.code_hash {
                    eprintln!("WARN: Saved State Code does not match current code {:?} != {:?}", sstate.code_hash, state.code_hash )
                }
                Ok(())
            }
        )?;

        state.character_renderer.restore_state(sstate.characters_on_screen, sstate.character_states);
        state.map_manager.restore_state(sstate.map_states, sstate.map_inventory, sstate.current_map);
        state.bg_manager.restore_state(sstate.bg_stack);
        state.state_manager.restore_state(sstate.text_variables, sstate.numeric_variables);
        state.stack = sstate.exec_stack;

        Ok(())
    }



    pub fn new(mut resource_manager: &'a mut ResourceManager, rendering_config: &'b RenderingConfig, hdpi: f32) -> Result<Self, ResourceError> {

        let mut rendering_globals = RenderingGlobals::new(&mut resource_manager);

        let text_renderer = TextRenderer::new(hdpi, &mut rendering_globals, &rendering_config, &mut resource_manager);

        let dialog_renderer = {
            let dialog_box = resource_manager.load_dialog_box("./res/dialog/basic.dlg")?;
            DialogRendererState::new(dialog_box)
        };



        let option_renderer = {
            let option_box = resource_manager.load_option_box("./res/dialog/basic.opt")?;
            OptionRendererState::new(option_box)
        };


        let inventory_renderer = resource_manager.load_inventory_renderer("./res/dialog/basic.inv")?;



        Ok(Self {
            resource_manager,
            rendering_config,
            rendering_globals,
            text_renderer,
            dialog_renderer,
            option_renderer,
            inventory_renderer,
            hdpi,
            script_state: None,
            initial_state: None
        })
    }


    pub fn load_script<T: AsRef<Path>>(&mut self, script_file: T) -> Result<(), ScriptError> {

        let script_string = {
            let mut buf = String::new();
            let mut file = File::open(script_file).expect("could not open file");

            file.read_to_string(&mut buf);

            buf
        };


        let mut error: Option<LexError> = None;
        let mut lexer = ScriptLexer::from(&script_string, &mut error);
        let mut parser = ScriptParser::from(lexer);
        let (decl_hash, code_hash, program) = parse_program(&mut parser).map_err(|err| ScriptError::ParseError(err))?;

        if let Some(value) = error {
            return Err(ScriptError::LexError(value));
        }



        let mut character_renderer = CharacterRenderer::new(&self.rendering_config);


        let mut map_manager = MapManager::new();
        let mut bg_manager = BackgroundManager::new();
        let mut state_manager = StateManager::new();
        let mut map_bindings = HashMap::new();
        let mut main = None;
        // add stack for main_method
        bg_manager.push_stack();


        let ops = compile_program(
            program,
            &mut TempState::new(
                self.resource_manager,
                &mut bg_manager,
                &mut map_manager,
                &mut character_renderer,
                &mut state_manager,
                &mut map_bindings,
                &mut main,
            )
        ).map_err(|err| ScriptError::CompileError(err))?;

        let main = main.ok_or(ScriptError::ScriptError(String::from("Script is missing main")))?;

        self.script_state = Some(ScriptState {
            decl_hash,
            code_hash,

            code: ops,

            character_renderer,
            map_manager,
            bg_manager,
            state_manager,
            map_bindings,

            stack: vec![(main,InstructionID(0))],
            executing_async: false,
            timeout: None,
            in_inv: false,
        });

        self.initial_state = self.save_state();
        Ok(())
    }


    pub fn update(&mut self, input: FrameInputs, delta_time: f32) {

        let mut sync_exec = true;

        while sync_exec {
            match self.script_state.as_mut() {
                Some(state) => {

                    // if inventory key pressed, then toggle inventory status
                    if input.input_keys.iter().find(|x| *x == &LogicalKey::INV).is_some() {
                        state.in_inv = !state.in_inv;
                    }


                    // if in inventory
                    if state.in_inv  {
                        // else update inv
                        self.inventory_renderer.renderer(
                            &mut state.map_manager
                        ).update(input, delta_time);
                        break;
                    } else {
                        match state.stack.iter_mut().last() {
                            Some(val) => {
                                let (dialog, instruction) = val;

                                let instructions = &state.code[dialog.0];
                                if instruction.0 >= instructions.len() {
                                    // if completed pop off top of stack
                                    state.stack.pop();
                                    state.bg_manager.pop_stack();
                                    state.timeout = Some(0.0);
                                    state.map_manager.completed.take();
                                } else {
                                    match &instructions[instruction.0] {
                                        DialogOperation::Show(bg_id) => {
                                            state.bg_manager.set_selected(*bg_id);
                                            instruction.incr();
                                        },
                                        DialogOperation::Goto(map_id) => {
                                            state.map_manager.set_map(*map_id);
                                            instruction.incr();
                                        },
                                        DialogOperation::Run(dlg_id) => {
                                            // push on new stack
                                            instruction.incr();
                                            state.stack.push((*dlg_id, InstructionID(0)));
                                            state.bg_manager.push_stack();
                                        }
                                        DialogOperation::Drop(item_id) => {
                                            state.map_manager.drop_item(*item_id);
                                            instruction.incr();
                                        }
                                        DialogOperation::State(chr_id, st_id) => {
                                            state.character_renderer.set_character_state(*chr_id, st_id.0);
                                            instruction.incr();
                                        }
                                        DialogOperation::Position(chr_id, pos) => {
                                            match pos {
                                                Some(vl) => state.character_renderer.set_character_position(*chr_id, *vl),
                                                None => state.character_renderer.set_character_offscreen(*chr_id)
                                            };
                                            instruction.incr();
                                        }
                                        DialogOperation::SetNumVar(num_vr, num_expr) => {
                                            let val = state.state_manager.eval_expr(num_expr);
                                            state.state_manager.set_numeric_variable(*num_vr, val);
                                            instruction.incr();
                                        },
                                        DialogOperation::SetStrVar(txt_id, str_val) => {
                                            state.state_manager.set_text_variable(*txt_id, str_val.clone());
                                            instruction.incr();
                                        },
                                        DialogOperation::SetFmt(txt_id, fmt_str, vals) => {
                                            let mut new_str = fmt_str.clone();
                                            let reg = Regex::new("{}").unwrap();

                                            for val in vals {
                                                let val = state.state_manager.get_numeric_variable(*val);
                                                let val = val.to_string();
                                                new_str = reg.replace(&new_str, val.as_str()).to_string();
                                            }
                                            state.state_manager.set_text_variable(*txt_id, new_str);
                                            instruction.incr();
                                        }
                                        DialogOperation::JumpBool(expr, trgt) => if state.state_manager.eval_bexpr(&state.map_manager, &expr) {*instruction = *trgt;} else {instruction.incr();},
                                        DialogOperation::Jump(pos) => *instruction = *pos,
                                        DialogOperation::Remove => {state.map_manager.remove_item(); instruction.incr();}


                                        DialogOperation::JumpCond(conds) => {
                                            let mut option_renderer =  self.option_renderer.renderer(&mut self.text_renderer);
                                            option_renderer.update(input.clone(), delta_time);
                                            if state.executing_async {
                                                let complete_status = option_renderer.is_complete();
                                                if let Some(ind) = complete_status {
                                                    // if complete, exit executing async, and
                                                    // jump to the corresponding pos
                                                    state.executing_async = false;
                                                    *instruction = conds[ind].1;
                                                } else {
                                                    // if option selection not complete, stop the execution loop
                                                    sync_exec = false;
                                                }
                                            } else {

                                                let opts = conds.iter().map(|(mtch, _)| {
                                                    match mtch {
                                                        MatchText::Text(txt) => txt.as_str(),
                                                        MatchText::Variable(var) => state.state_manager.get_text_variable(*var)
                                                    }
                                                }).collect();

                                                // configure the option renderer to display the options
                                                self.option_renderer.renderer(&mut self.text_renderer).set_options(opts);

                                                // set async execution to true
                                                // stop the execution loop
                                                state.executing_async = true;
                                                sync_exec = false;
                                            }
                                        },
                                        DialogOperation::SayText(chr, text) => {
                                            let mut dialog_renderer = self.dialog_renderer.renderer(&mut self.text_renderer);
                                            if state.executing_async {
                                                dialog_renderer.update(input.clone(), delta_time);
                                                if dialog_renderer.is_complete() {
                                                    // if complete, exit executing async, and
                                                    state.executing_async = false;
                                                    instruction.incr();
                                                } else {
                                                    // if option selection not complete, stop the execution loop
                                                    sync_exec = false;
                                                }
                                            } else {

                                                // set the character to be active
                                                if let Some(ch) = chr {
                                                    state.character_renderer.set_character_active(*ch);
                                                }
                                                // configure the dialog renderer to print the corresponding text
                                                dialog_renderer.set_text(text.as_str());

                                                // set async execution to true
                                                // stop the execution loop
                                                state.executing_async = true;
                                                sync_exec = false;
                                            }

                                        },
                                        DialogOperation::SayVar(chr, txt_id) => {
                                            let mut dialog_renderer = self.dialog_renderer.renderer(&mut self.text_renderer);
                                            if state.executing_async {
                                                dialog_renderer.update(input.clone(), delta_time);
                                                if dialog_renderer.is_complete() {
                                                    // if complete, exit executing async, and
                                                    state.executing_async = false;
                                                    instruction.incr();
                                                } else {
                                                    // if option selection not complete, stop the execution loop
                                                    sync_exec = false;
                                                }
                                            } else {

                                                // set the character to be active
                                                if let Some(ch) = chr {
                                                    state.character_renderer.set_character_active(*ch);
                                                }
                                                let text = state.state_manager.get_text_variable(*txt_id);
                                                // configure the dialog renderer to print the corresponding text
                                                dialog_renderer.set_text(text);

                                                // set async execution to true
                                                // stop the execution loop
                                                state.executing_async = true;
                                                sync_exec = false;
                                            }
                                        },
                                    }
                                }
                            },
                            None => {

                                match state.timeout.take() {
                                    Some(val) => {
                                        if val < 300.0 {
                                            state.timeout = Some(val + delta_time);
                                            sync_exec = false;
                                        }
                                    }
                                    None => {
                                        // nothing on the stack - thus just update the map
                                        state.map_manager.update(input.clone(), delta_time);

                                        // if action on map item
                                        match state.map_manager.completed.as_ref() {
                                            Some(sel) => match state.map_bindings.get(sel) {
                                                // push function onto stack
                                                Some(dlg) => {
                                                    state.stack.push((*dlg, InstructionID(0)));
                                                    state.bg_manager.push_stack();
                                                },
                                                // otherwise, exit loop
                                                None => {sync_exec = false;}
                                            }
                                            None => {sync_exec = false;}
                                        }
                                    }
                                }
                            }
                        }
                    }

                },
                None => sync_exec = false,
            }
        }


    }

    pub fn draw(&mut self) {

        match self.script_state.as_mut() {
            Some(state) => {

                match state.stack.iter_mut().last() {
                    Some(val) => {
                        let (dialog, instruction) = val;

                        let instructions = &state.code[dialog.0];
                        if instruction.0 < instructions.len() {
                            match instructions[instruction.0] {
                                DialogOperation::JumpCond(_) => {
                                    state.map_manager.draw(&mut self.rendering_globals, self.resource_manager);
                                    state.bg_manager.draw(&mut self.rendering_globals, self.resource_manager);
                                    state.character_renderer.draw(&mut self.rendering_globals, self.resource_manager);
                                    self.option_renderer.renderer(&mut self.text_renderer).draw(&mut self.rendering_globals, self.resource_manager);
                                }
                                DialogOperation::SayText(_,_)| DialogOperation::SayVar(_,_) => {
                                    state.map_manager.draw(&mut self.rendering_globals, self.resource_manager);
                                    state.bg_manager.draw(&mut self.rendering_globals, self.resource_manager);
                                    state.character_renderer.draw(&mut self.rendering_globals, self.resource_manager);
                                    self.dialog_renderer.renderer(&mut self.text_renderer).draw(&mut self.rendering_globals, self.resource_manager);
                                }
                                _ => ()
                            }
                        }
                    },
                    None => {
                        state.map_manager.draw(&mut self.rendering_globals, self.resource_manager);
                    }
                }


                if state.in_inv {
                    println!("drawing inventory!");
                    self.inventory_renderer.renderer(&mut state.map_manager)
                        .draw(&mut self.rendering_globals, self.resource_manager);
                }
            },
            None => ()
        }

    }
}



