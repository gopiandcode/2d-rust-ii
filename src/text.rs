use crate::shader::Shader;
use crate::resource_loader;
use crate::statics::{APPLICATION_NAME, APPLICATION_INITIAL_SIZE};
use crate::graphics::{construct_transform_matrix};
use crate::globals::RenderingGlobals;
use crate::config::RenderingConfig;
use crate::resource_loader::ResourceManager;


use sdl2::event::Event;
use sdl2::video::SwapInterval;
use sdl2::VideoSubsystem;

use gl::types::{GLfloat, GLint, GLsizei, GLuint, GLsizeiptr};

use nalgebra::base::{Matrix4, Vector2, Vector3, Vector4};
use nalgebra::{Translation3, Rotation3, Transform3};

use log::{error, warn, info, debug, trace};

use std::time::{Duration, Instant, SystemTime};
use std::os::raw::c_void;

use glyph_brush::{BrushAction, BrushError, GlyphBrushBuilder, GlyphBrush, Section, Layout, HorizontalAlign, VerticalAlign};
use glyph_brush::rusttype::{Scale, Rect, point};


pub struct TextRenderer<'a> {
    font_texture_id: GLuint,
    vao: GLuint,
    vbo: GLuint,
    glyph_brush: GlyphBrush<'a, [f32; 13]>,
    vertex_count: usize,
    vertex_max: usize,
    hdpi_factor: f32
}

impl<'a> TextRenderer<'a> {
    pub fn new(
        hdpi_factor: f32,
        globals: &mut RenderingGlobals,
        config: &'a RenderingConfig,
        resource_manager: &mut ResourceManager
    ) -> Self {
        let mut glyph_brush = GlyphBrushBuilder::using_font_bytes(&config.font).build();
        let (tex_width, tex_height) = glyph_brush.texture_dimensions();

        let mut name = 0;
        unsafe {
            gl::PixelStorei(gl::UNPACK_ALIGNMENT, 1);
            gl::GenTextures(1, &mut name);
            gl::BindTexture(gl::TEXTURE_2D, name);

            gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_WRAP_S, gl::CLAMP_TO_EDGE as _);
            gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_WRAP_T, gl::CLAMP_TO_EDGE as _);
            gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MIN_FILTER, gl::LINEAR as _);
            gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MAG_FILTER, gl::LINEAR as _);

            gl::TexImage2D(
                gl::TEXTURE_2D, 0, gl::RED as _, tex_width as _, tex_height as _, 0, gl::RED,
                gl::UNSIGNED_BYTE, std::ptr::null()
            );
        }


        let mut vao = 0;
        let mut vbo = 0;

        unsafe {
            gl::GenVertexArrays(1, &mut vao);
            gl::GenBuffers(1, &mut vbo);
        }

        let transform = construct_transform_matrix(
            2.0 / APPLICATION_INITIAL_SIZE.0 as f32, 2.0 / APPLICATION_INITIAL_SIZE.1 as f32, -1.0, -1.0, 0.0
        );
        let FONT_SHADER = &mut resource_manager[globals.FONT_SHADER];

        FONT_SHADER.enable();
            FONT_SHADER.set_matrix4f(
                &globals.TRANSFORM_TAG, &transform
            );
            FONT_SHADER.set_fragment_data_location(
                &globals.OUT_COLOR_TAG, 0
            );

            unsafe {
                gl::BindVertexArray(vao);
                gl::BindBuffer(gl::ARRAY_BUFFER, vbo);
            }

            let mut offset = 0;
            for (attr_pos, float_count) in &[
                // left_top
                (&globals.LEFT_TOP_TAG, 3),
                // right_bottom
                (&globals.RIGHT_BOTTOM_TAG, 2),
                // tex_left_top
                (&globals.TEX_LEFT_TOP_TAG, 2),
                // tex_right_bottom
                (&globals.TEX_RIGHT_BOTTOM_TAG, 2),
                // color
                (&globals.COLOR_TAG, 4),
            ] {

                let attr_pos = FONT_SHADER.get_variable_position(*attr_pos).unwrap();

                unsafe {
                    gl::EnableVertexAttribArray(attr_pos as _);
                    gl::VertexAttribPointer(
                        attr_pos,
                        *float_count,
                        gl::FLOAT,
                        gl::FALSE,
                        std::mem::size_of::<[GLfloat; 13]>() as i32,
                        offset as _
                    );
                    gl::VertexAttribDivisor(attr_pos as _, 1);
                }

                offset += *float_count * 4;
            }

        unsafe {
            gl::BindBuffer(gl::ARRAY_BUFFER, 0);
        }

        Self {
            font_texture_id: name,
            vao: vao,
            vbo: vbo,
            glyph_brush: glyph_brush,
            vertex_count: 0,
            vertex_max: 0,
            hdpi_factor: hdpi_factor,
        }
    }

    fn update_texture_size(&mut self, tex_width: f32, tex_height: f32) {
        unsafe {
            gl::DeleteTextures(1, &self.font_texture_id);
        }

        let mut name = 0;
        unsafe {
            gl::PixelStorei(gl::UNPACK_ALIGNMENT, 1);
            gl::GenTextures(1, &mut name);
            gl::BindTexture(gl::TEXTURE_2D, name);

            gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_WRAP_S, gl::CLAMP_TO_EDGE as _);
            gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_WRAP_T, gl::CLAMP_TO_EDGE as _);
            gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MIN_FILTER, gl::LINEAR as _);
            gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MAG_FILTER, gl::LINEAR as _);

            gl::TexImage2D(
                gl::TEXTURE_2D, 0, gl::RED as _, tex_width as _, tex_height as _, 0, gl::RED,
                gl::UNSIGNED_BYTE, std::ptr::null()
            );

        }
        self.font_texture_id = name;
    }




    pub fn add_text(&mut self, text: &str, font_size: f32, position: &Vector2<GLfloat>, bounds: &Vector2<GLfloat>, colour: &Vector4<GLfloat>) {
        self.glyph_brush.queue(
            Section {
                text: text,
                scale: Scale::uniform((font_size * self.hdpi_factor).round()),
                screen_position: (position.x, position.y),
                bounds: (bounds.x, bounds.y),
                color: *colour.as_ref(),
                ..Section::default()
            }
        )
    }

    pub fn add_centered_text(&mut self, text: &str, font_size: f32, position: &Vector2<GLfloat>, bounds: &Vector2<GLfloat>, colour: &Vector4<GLfloat>) {
        self.glyph_brush.queue(
            Section {
                text: text,
                scale: Scale::uniform((font_size * self.hdpi_factor).round()),
                screen_position: (position.x, position.y),
                bounds: (bounds.x, bounds.y),
                color: *colour.as_ref(),
                layout: Layout::default().h_align(HorizontalAlign::Center).v_align(VerticalAlign::Center),
                ..Section::default()
            }
        )
    }



    pub fn draw(&mut self, globals: &mut RenderingGlobals, resource_manager: &mut ResourceManager) {
        let texture_id = self.font_texture_id;

        let mut brush_action;

        loop {
            brush_action = self.glyph_brush.process_queued(
                |rect, tex_data| unsafe {

                    gl::BindTexture(gl::TEXTURE_2D, texture_id);
                    gl::TexSubImage2D(
                        gl::TEXTURE_2D,
                        0,
                        rect.min.x as _,
                        rect.min.y as _,
                        rect.width() as _,
                        rect.height() as _,
                        gl::RED,
                        gl::UNSIGNED_BYTE,
                        tex_data.as_ptr() as _
                    );

                },
                to_vertex
            );

            match brush_action {
                Ok(_) => break,
                Err(BrushError::TextureTooSmall { suggested, .. }) => {
                    let max_image_dimension = {
                        let mut value = 0 as gl::types::GLint;
                        unsafe { gl::GetIntegerv(gl::MAX_TEXTURE_SIZE, &mut value) };
                        value as u32
                    };

                    let (new_width, new_height) = if
                        suggested.0 > max_image_dimension || suggested.1 > max_image_dimension
                     {
                        (max_image_dimension, max_image_dimension)
                    } else {
                        suggested
                    };

                    self.update_texture_size(new_width as f32, new_height as f32);
                    self.glyph_brush.resize_texture(new_width, new_height);
                }
            }

        }



            match brush_action.expect("Encountered error while rendering text") {
                BrushAction::Draw(vertices) => {
                    self.vertex_count = vertices.len();
                    unsafe {
                        gl::BindBuffer(gl::ARRAY_BUFFER, self.vbo);

                        if self.vertex_max < self.vertex_count {
                            gl::BufferData(
                                gl::ARRAY_BUFFER,
                                (self.vertex_count * std::mem::size_of::<[f32; 13]>()) as GLsizeiptr,
                                vertices.as_ptr() as _,
                                gl::DYNAMIC_DRAW,
                            );
                        } else {
                            gl::BufferSubData(
                                gl::ARRAY_BUFFER,
                                0,
                                (self.vertex_count * std::mem::size_of::<[f32; 13]>()) as GLsizeiptr,
                                vertices.as_ptr() as _
                            );
                        }
                        self.vertex_max = self.vertex_max.max(self.vertex_count);
                        gl::BindBuffer(gl::ARRAY_BUFFER, 0);
                    }
                },
                BrushAction::ReDraw => {}
            }



            let FONT_SHADER = &mut resource_manager[globals.FONT_SHADER];
            FONT_SHADER.enable();

                unsafe {
                    gl::ActiveTexture(gl::TEXTURE0 + 0);
                    gl::BindTexture(gl::TEXTURE_2D, self.font_texture_id);
                    FONT_SHADER.set_int(
                        &globals.FONT_TEX_TAG, 0
                    );

                    gl::BindVertexArray(self.vao);
                    gl::DrawArraysInstanced(gl::TRIANGLE_STRIP, 0, 4, self.vertex_count as _);
                }
    }
}


impl<'a> Drop for TextRenderer<'a> {
    fn drop(&mut self) {
        unsafe {
            gl::DeleteTextures(1, &self.font_texture_id);
            // TODO: Delete VAOs
        }
    }
}

#[inline]
fn to_vertex(
    glyph_brush::GlyphVertex {
        mut tex_coords,
        pixel_coords,
        bounds,
        color,
        z,
    }: glyph_brush::GlyphVertex,
) -> [f32; 13] {
    let gl_bounds = bounds;

    let mut gl_rect = Rect {
        min: point(pixel_coords.min.x as f32, pixel_coords.min.y as f32),
        max: point(pixel_coords.max.x as f32, pixel_coords.max.y as f32),
    };

    // handle overlapping bounds, modify uv_rect to preserve texture aspect
    if gl_rect.max.x > gl_bounds.max.x {
        let old_width = gl_rect.width();
        gl_rect.max.x = gl_bounds.max.x;
        tex_coords.max.x = tex_coords.min.x + tex_coords.width() * gl_rect.width() / old_width;
    }
    if gl_rect.min.x < gl_bounds.min.x {
        let old_width = gl_rect.width();
        gl_rect.min.x = gl_bounds.min.x;
        tex_coords.min.x = tex_coords.max.x - tex_coords.width() * gl_rect.width() / old_width;
    }
    if gl_rect.max.y > gl_bounds.max.y {
        let old_height = gl_rect.height();
        gl_rect.max.y = gl_bounds.max.y;
        tex_coords.max.y = tex_coords.min.y + tex_coords.height() * gl_rect.height() / old_height;
    }
    if gl_rect.min.y < gl_bounds.min.y {
        let old_height = gl_rect.height();
        gl_rect.min.y = gl_bounds.min.y;
        tex_coords.min.y = tex_coords.max.y - tex_coords.height() * gl_rect.height() / old_height;
    }

    [
        gl_rect.min.x,
        gl_rect.max.y,
        z,
        gl_rect.max.x,
        gl_rect.min.y,
        tex_coords.min.x,
        tex_coords.max.y,
        tex_coords.max.x,
        tex_coords.min.y,
        color[0],
        color[1],
        color[2],
        color[3],
    ]
}
