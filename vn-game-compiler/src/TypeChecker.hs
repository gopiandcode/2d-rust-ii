-- {-# OPTIONS -Wall #-}
module TypeChecker (
  perform_typecheck
                   ) where

import Parser
import Control.Monad.Trans
import Control.Monad.Writer
import Control.Monad.State
import System.IO
import Data.HashMap
import Data.HashSet
import Text.Printf
    
data Type = Int | Bool | Text | Any
  deriving (Show)

instance Eq Type where

  a == b = case a of
    TypeChecker.Any -> True
    Int -> case b of
      TypeChecker.Any -> True
      Int -> True
      _   -> False
    Bool -> case b of
      TypeChecker.Any -> True
      Bool -> True
      _   -> False
    Text -> case b of
      TypeChecker.Any -> True
      Text -> True
      _    -> False


data TypeError = TypeError (Int,Int) String deriving (Show)

type TypeEnv = Map String Type

data Context = Context {
  variable_map :: TypeEnv,
  valid_items :: Set String,
  valid_dialogs :: Set String,
  valid_maps :: Set String,
  valid_backgrounds :: Set String,  
  character_states :: Map String (Set String)
 } deriving (Show)

empty_context :: Context
empty_context = Context
                   Data.HashMap.empty
                   Data.HashSet.empty
                   Data.HashSet.empty
                   Data.HashSet.empty
                   Data.HashSet.empty
                   Data.HashMap.empty
                  
             
type TypeCheck =
  WriterT [TypeError]  (State Context)

print_error :: String -> TypeError -> IO ()
print_error fname (TypeError (l,c) txt) = do
  hPutStrLn stderr (fname ++ ":" ++ (show l) ++ ":" ++ (show c) ++ ": error: " ++ txt)
    

typecheck_program :: Program -> TypeCheck ()
typecheck_program (Program decls dlgs) = do
  mapM_  preprocess_dialog dlgs;
  mapM_ load_decl decls;
  mapM_ typecheck_dialog dlgs;
  check_program_has_main;
  return ()

typecheck_dialog :: Dialog -> TypeCheck ()
typecheck_dialog (Dialog _ name bod)  = do
  mapM_  check_statement bod;
  return ()
          
variable_type_to_type :: VariableType -> Type
variable_type_to_type VarInt = Int
variable_type_to_type VarText = Text
variable_type_to_type VarBool = Bool

load_decl :: Declaration -> TypeCheck ()
load_decl (CharacterDecl i name _ states) = do
  state <- get;
  let char_mem = character_states state;
  let char_decl = Data.HashMap.member name char_mem;
  let new_states = Data.HashSet.fromList states;
  let new_char_mem =  Data.HashMap.insert name new_states char_mem;
  if char_decl then
    tell [(
             TypeError i (printf "Duplicate Character Declaration %s" name)
          )]
   else
    return ()
  put (state { character_states = new_char_mem });
  return ()

         
load_decl (VariableDecl i ty name expr) = do
  state <- get;
  if Data.HashMap.member name (variable_map state) then
    tell [(
             TypeError i (printf "Duplicate Variable Declaration %s" name)
          )]
   else
    return ()
  let exp_type = variable_type_to_type ty;
  real_type <- typeCheck expr;
  if real_type /= exp_type then
    tell [(
             TypeError i (
              printf "Variable (%s) Type doesn't match value - %s /= %s "
                   name (show exp_type) (show real_type))
          )]
   else
    return ()
  let new_member = Data.HashMap.insert name exp_type (variable_map state);
  put (state { variable_map = new_member });
  return ()


load_decl (MapDecl i name path mappings) = do
  state <- get;
  let valid_map = valid_maps state;
  if Data.HashSet.member name valid_map then
    tell [(
             TypeError i (printf "Duplicate Map Declaration %s" name)
          )];
   else
    return ();
  mapM_ load_mapping $! mappings;
  state <- get;
  let valid_map = valid_maps state;
  let new_valid_map = Data.HashSet.insert name valid_map;
  put (state { valid_maps = new_valid_map  } )
  return ()
  
load_decl (BackgroundDecl i bg path) = do
  state <- get;
  if Data.HashSet.member bg (valid_backgrounds state) then
    tell [(
             TypeError i (printf "Duplicate Background Declaration %s" bg)
          )];
    else
     return ();
  let bgs = Data.HashSet.insert bg (valid_backgrounds state)
  put (state { valid_backgrounds = bgs });
  return ()

preprocess_dialog :: Dialog -> TypeCheck ()
preprocess_dialog (Dialog i name _) = do
  state <- get;
  if Data.HashSet.member name (valid_dialogs state) then
    tell [(
             TypeError i (printf "Duplicate Dialog Declaration of %s" name)
          )];
   else
    return ();
  let dialogs = Data.HashSet.insert name (valid_dialogs state);
  put (state { valid_dialogs = dialogs });
  return ()

check_cond :: Info -> Expr -> TypeCheck ()
check_cond i e = do
  typ <- typeCheck e;
  if typ /= Bool then
    tell [(
             TypeError i ( "Non-Boolean Expression provided for condition")
          )];
   else
    return ();

check_text :: Info -> Expr -> TypeCheck ()
check_text i e = do
  typ <- typeCheck e;
  if typ /= Text then
    tell [(
             TypeError i ( "Non-Text Expression provided for Text Argument")
          )];
   else
    return ();
           
check_statement :: Statement -> TypeCheck ()
check_statement (While i expr bod) = do
  check_cond i expr;
  mapM check_statement bod;
  return ()
check_statement (If i conds else_bod) = do
  mapM (check_cond i) (Prelude.map fst conds);  
  mapM (mapM check_statement) (Prelude.map snd conds);
  case else_bod of
    Just elseb -> mapM check_statement elseb;
    Nothing    -> return [()];
  return ()
check_statement (Goto i map) = do
  state <- get;
  if not (Data.HashSet.member map (valid_maps state)) then
    tell [(
             TypeError i (printf "Goto to undefined map %s" map)
          )];
   else
    return ();
  return ()
check_statement (Show i bg) = do
  state <- get;
  if not (Data.HashSet.member bg (valid_backgrounds state)) then
    tell [(
             TypeError i (printf "Show undefined background %s" bg)
          )]
   else
    return ();
  return ()
check_statement (Run i dlg) = do
  state <- get;
  if not (Data.HashSet.member dlg (valid_dialogs state)) then
    tell [(
             TypeError i (printf "Run undefined map %s" dlg)
          )]
   else
    return ()
  return ()
check_statement (Drop i item) = do
  state <- get;
  if not (Data.HashSet.member item (valid_items state)) then
    tell [(
             TypeError i (printf "Dropping undefined item %s" item)
          )]
   else
    return ()
  return ()

check_statement (State i character char_state) = do
  state <- get;
  let o_char_map = Data.HashMap.lookup character (character_states state);
  case o_char_map of
    Nothing -> tell [(
                        TypeError i (printf "Use of undefined character %s" character)
                     )]
    Just sts -> if Data.HashSet.member char_state sts then
                    return ();
                  else
                    tell [(
                        TypeError i (printf "Use of undefined state %s for character %s" char_state character)
                     )]
check_statement (Position i name pos) = do
  state <- get;
  if not (Data.HashMap.member name (character_states state)) then
    tell [(
             TypeError i (printf "Use of undefined character %s" name)
          )]
   else
    return ()
check_statement (Say i chr expr) = do
  state <- get;
  ty <- typeCheck expr;
  if ty /= Text then
    tell [(
             TypeError i ("Non Text argument to say")
          )];
   else
    return ();
  case chr of
    Just character -> if not (Data.HashMap.member character (character_states state)) then
                         tell [(
                            TypeError i (printf "Use of undefined character %s" character)
                         )]
                        else
                         return ()
    Nothing        -> return ()
check_statement (Input i cases) = do
  mapM (check_text i) (Prelude.map fst cases);
  mapM (mapM check_statement) (Prelude.map snd cases);
  return ()
check_statement (Assign i var expr) = do
  state <- get;
  atype <- typeCheck expr;
  let o_vty = Data.HashMap.lookup var (variable_map state);
  case o_vty of
    Nothing -> tell [(
                      TypeError i (printf "Use of undefined variable %s" var)
                 )]
    Just ety -> if ety /= atype then
                    tell [(
                           TypeError i (printf
                               "Attempt to assign value of type %s to variable of type %s"
                               (show atype) (show ety))
                    )]
                 else
                     return ()
check_statement (Remove i) = return ()



load_mapping :: MapMapping -> TypeCheck ()
load_mapping (Item i item dlg) = do
  state <- get;
  let dlgs = valid_dialogs state;
  if not (Data.HashSet.member dlg dlgs) then
    tell [(
             TypeError i (printf "Item %s maps to undefined dialog %s" (show item) (show dlg))
          )];
   else
    return ();
  let items = valid_items state;
  if Data.HashSet.member item items then
    tell [(
             TypeError i (printf "Duplicate definition of item %s" (show item))
          )];
   else
    return ();
  let new_items = Data.HashSet.insert item items;
  put (state { valid_items = new_items });
  return ()
         
load_mapping (Prop i item dlg)  = do
  state <- get;
  let dlgs = valid_dialogs state;
  if not (Data.HashSet.member dlg dlgs) then
    tell [(
             TypeError i (printf "Prop %s maps to undefined dialog %s" (show item) (show dlg))
          )];
   else
    return ();

check_program_has_main :: TypeCheck ()
check_program_has_main = do
  state <- get;
  case Data.HashSet.member "main" . valid_dialogs $ state of
    False -> tell [(
                        TypeError (0,0) "Program has no main"
                     )]
    True  -> return ()               

typeCheck :: Expr -> TypeCheck Type
typeCheck (BoolConst _ _) = return Bool
typeCheck (StringConst _ _) = return Text                            
typeCheck (IntConst _ _) = return Int
typeCheck (Variable i name) = do
  map <- get;
  let ty = (Data.HashMap.lookup name (variable_map map));
  case ty of
    Just v -> return v
    Nothing -> (do
         tell [(
           TypeError i (printf "Undeclared variable %s" name)
          )];
         return TypeChecker.Any
       )
typeCheck (Check i str)  = do
  map <- get;
  let val =  Data.HashSet.member str (valid_items map);
  if not val then
    tell [(
             TypeError i (printf "Undeclared item %s" str)
          )];
   else
    return ();
  return Bool

typeCheck (Format i fmt_string exps) = do
  let spaces = check_format_string fmt_string;
  if (length exps /= spaces) then
     tell [(
          TypeError i (printf "Incorrect number of format parameters for format string \"%s\"" fmt_string)
      )]
    else
     return ();
  mapM typeCheck exps;
  return Text

typeCheck (Not i exp) = do
  ty <- typeCheck exp;
  if ty /= Bool then
     tell [(
          TypeError i ("Incorrect type to boolean operand")
      )];
   else
     return ();
  return Bool

typeCheck (And i a b) = do
  ty <- typeCheck a;
  if ty /= Bool then
     tell [(
          TypeError i ("Incorrect type to boolean operand")
      )];
   else
     return ();
  ty <- typeCheck b;
  if ty /= Bool then
     tell [(
          TypeError i ("Incorrect type to boolean operand")
      )];
   else
     return ();
  return Bool

typeCheck (Or i a b) = do
  ty <- typeCheck a;
  if ty /= Bool then
     tell [(
          TypeError i ("Incorrect type to boolean operand")
      )];
   else
     return ();
  ty <- typeCheck b;
  if ty /= Bool then
     tell [(
          TypeError i ("Incorrect type to boolean operand")
      )];
   else
     return ();
  return Bool

typeCheck (Gt i a b) = do
  ty <- typeCheck a;
  if ty /= Int then
     tell [(
          TypeError i ("Incorrect type to arithmetic operand")
      )];
   else
     return ();
  ty <- typeCheck b;
  if ty /= Int then
     tell [(
          TypeError i ("Incorrect type to arithmetic operand")
      )];
   else
     return ();
  return Bool

typeCheck (Lt i a b) = do
  ty <- typeCheck a;
  if ty /= Int then
     tell [(
          TypeError i ("Incorrect type to arithmetic operand")
      )];
   else
     return ();
  ty <- typeCheck b;
  if ty /= Int then
     tell [(
          TypeError i ("Incorrect type to arithmetic operand")
      )];
   else
     return ();
  return Bool

typeCheck (Gte i a b) = do
  ty <- typeCheck a;
  if ty /= Int then
     tell [(
          TypeError i ("Incorrect type to arithmetic operand")
      )];
   else
     return ();
  ty <- typeCheck b;
  if ty /= Int then
     tell [(
          TypeError i ("Incorrect type to arithmetic operand")
      )];
   else
     return ();
  return Bool

typeCheck (Lte i a b) = do
  ty <- typeCheck a;
  if ty /= Int then
     tell [(
          TypeError i ("Incorrect type to arithmetic operand")
      )];
   else
     return ();
  ty <- typeCheck b;
  if ty /= Int then
     tell [(
          TypeError i ("Incorrect type to arithmetic operand")
      )];
   else
     return ();
  return Bool

typeCheck (Eq i a b) = do
  ty <- typeCheck a;
  if ty /= Int then
     tell [(
          TypeError i ("Incorrect type to arithmetic operand")
      )];
   else
     return ();
  ty <- typeCheck b;
  if ty /= Int then
     tell [(
          TypeError i ("Incorrect type to arithmetic operand")
      )];
   else
     return ();
  return Bool

typeCheck (Neg i a) = do
  ty <- typeCheck a;
  if ty /= Int then
     tell [(
          TypeError i ("Incorrect type to arithmetic operand")
      )];
   else
     return ();
  return Int

typeCheck (Mul i a b) = do
  ty <- typeCheck a;
  if ty /= Int then
     tell [(
          TypeError i ("Incorrect type to arithmetic operand")
      )];
   else
     return ();
  ty <- typeCheck b;
  if ty /= Int then
     tell [(
          TypeError i ("Incorrect type to arithmetic operand")
      )];
   else
     return ();
  return Int

typeCheck (Sub i a b) = do
  ty <- typeCheck a;
  if ty /= Int then
     tell [(
          TypeError i ("Incorrect type to arithmetic operand")
      )];
   else
     return ();
  ty <- typeCheck b;
  if ty /= Int then
     tell [(
          TypeError i ("Incorrect type to arithmetic operand")
      )];
   else
     return ();
  return Int

typeCheck (Add i a b) = do
  ty <- typeCheck a;
  if ty /= Int && ty /= Text then
     tell [(
          TypeError i ("Incorrect type to arithmetic operand")
      )];
   else
     return ();
  ty2 <- typeCheck b;
  if ty /= ty2 then
     tell [(
          TypeError i (printf "Mismatched types to + operand - %s /= %s" (show ty) (show ty2))
      )];
   else
     return ();
  return ty

typeCheck (Div i a b) = do
  ty <- typeCheck a;
  if ty /= Int then
     tell [(
          TypeError i ("Incorrect type to arithmetic operand")
      )];
   else
     return ();
  ty <- typeCheck b;
  if ty /= Int then
     tell [(
          TypeError i ("Incorrect type to arithmetic operand")
      )];
   else
     return ();
  return Int


check_format_string :: String -> Int
check_format_string ('{':'}':xs) = 1 + check_format_string xs
check_format_string (x:xs) = check_format_string xs  
check_format_string [] = 0

perform_typecheck :: String -> Program -> IO Bool
perform_typecheck fname prg = do
    let ty = typecheck_program prg;
    let comp = runState $ runWriterT ty;
    let (((), errs), stt) = comp empty_context;
    mapM (print_error fname) errs;
    return (length errs > 0);
