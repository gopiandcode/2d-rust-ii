#version 330 core
layout (location = 0) in vec2 vp;

out vec3 colour;

void main() {
     colour = vec3(vp, 0.5);
     gl_Position = vec4(vp, 0.0, 1.0);
}