use sdl2::EventPump;
use sdl2::event::Event;
use sdl2::keyboard::Keycode;
use std::collections::HashMap;


// TODO: Change to suitable version
pub struct InputManager {
    input_map: HashMap<Keycode, LogicalKey>
}

#[derive(Clone, Debug)]
pub struct FrameInputs {
    pub exit_requested: bool,
    pub input_keys: Vec<LogicalKey>
}


#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum LogicalKey {
    ENTER,
    UP,
    DOWN,
    LEFT,
    RIGHT,
    INV,
    ESC,
}


impl InputManager {

    pub fn poll_events(&self, event_pump: &mut EventPump) -> FrameInputs {

        let mut inputs = FrameInputs {
            exit_requested: false,
            input_keys: Vec::new()
        };

        for event in event_pump.poll_iter() {
            match event {
                Event::Quit { .. } => inputs.exit_requested = true,
                Event::KeyDown { keycode: Some(ref key), .. } => {
                    match self.input_map.get(key) {
                        Some(logical_key) => inputs.input_keys.push(*logical_key),
                        None => ()
                    }
                },
                _ => ()
            }
        }

        inputs
    }

}


impl Default for InputManager {
    fn default() -> Self {
        let mut input_map : HashMap<Keycode, LogicalKey> = HashMap::new();

        input_map.insert(Keycode::Up, LogicalKey::UP);
        input_map.insert(Keycode::Down, LogicalKey::DOWN);
        input_map.insert(Keycode::Left, LogicalKey::LEFT);
        input_map.insert(Keycode::Right, LogicalKey::RIGHT);

        // VIM BINDINGS baby!
        input_map.insert(Keycode::K, LogicalKey::UP);
        input_map.insert(Keycode::J, LogicalKey::DOWN);
        input_map.insert(Keycode::H, LogicalKey::LEFT);
        input_map.insert(Keycode::L, LogicalKey::RIGHT);

        input_map.insert(Keycode::Space, LogicalKey::ENTER);
        input_map.insert(Keycode::Return, LogicalKey::ENTER);


        input_map.insert(Keycode::I, LogicalKey::INV);
        input_map.insert(Keycode::Escape, LogicalKey::ESC);

        Self {
            input_map: input_map
        }
    }
}


