use std::cell::RefCell;
use std::rc::Rc;
use std::ffi::{CString, CStr};
use std::ptr;
use std::path::Path;

use sdl2::event::Event;

use gl::types::{GLfloat, GLint, GLsizei, GLuint, GLsizeiptr};

use nalgebra::base::{Matrix4, Matrix3, Vector2, Vector3, Vector4};
use nalgebra::{Translation3, Rotation3, Transform3, Translation2, Rotation2, Transform2};

use log::{error, warn, info, debug, trace};

use std::time::{Duration, Instant, SystemTime};
use std::os::raw::c_void;

use crate::resource_loader;
use crate::resource_loader::{TextureID, ShaderID};
use crate::statics::{APPLICATION_INITIAL_SIZE};
use crate::texture::Texture;
use crate::shader::Shader;
use crate::gpu_buffer::buffer_data;






pub struct RenderingGlobals {
    /// main VAO,VBO for quads
    pub GPU_QUAD: (GLuint, GLuint),

    pub FILL_SHADER: ShaderID,
    pub TEX_SHADER: ShaderID,
    pub COLOR_TAG: CString,
    pub TRANSFORM_TAG: CString,
    pub TEX_TRANSFORM_TAG: CString,
    pub SPRITE_COLOR_TAG: CString,
    pub IMAGE_TAG: CString,
    pub SIZE_TAG: CString,
    pub TEX_SIZE_TAG: CString,

    pub FONT_SHADER: ShaderID,
    pub FONT_TEX_TAG: CString,
    pub OUT_COLOR_TAG: CString,
    pub LEFT_TOP_TAG: CString,
    pub RIGHT_BOTTOM_TAG: CString,
    pub TEX_LEFT_TOP_TAG: CString,
    pub TEX_RIGHT_BOTTOM_TAG: CString,
}

impl RenderingGlobals {
    pub fn new(resource_manager: &mut resource_loader::ResourceManager) -> Self {
        let mut vbo = 0;
        let mut vao = 0;

        unsafe {
            gl::GenVertexArrays(1, &mut vao);
            gl::GenBuffers(1, &mut vbo);

            buffer_data::<GLfloat>(vbo, &vec![
                -1.0, -1.0,
                1.0, -1.0,
                -1.0, 1.0,
                1.0, -1.0,
                1.0, 1.0,
                -1.0, 1.0,
            ]);

            gl::BindVertexArray(vao);
            gl::EnableVertexAttribArray(0);
            gl::BindBuffer(gl::ARRAY_BUFFER, vbo);
            gl::VertexAttribPointer(
                0, 2, gl::FLOAT, gl::FALSE,
                0, std::ptr::null()
            );
            gl::BindBuffer(gl::ARRAY_BUFFER, 0);
            gl::BindVertexArray(0);
        }
        let GPU_QUAD = (vao, vbo);
        let mut fill_shader = resource_manager.load_shader(
            "./shaders/fill.vs",
            "./shaders/fill.fs"
        ).unwrap();

        let FILL_SHADER = fill_shader;

        let mut tex_shader = resource_manager.load_shader(
            "./shaders/tex.vs",
            "./shaders/tex.fs"
        ).unwrap();

        let mut font_shader = resource_manager.load_shader(
            "./shaders/text.vs",
            "./shaders/text.fs"
        ).expect("Could not build shader!");



        let FONT_SHADER = font_shader;

        let TEX_SHADER = tex_shader;
        let TRANSFORM_TAG = CString::new("transform").unwrap();
        let COLOR_TAG = CString::new("color").unwrap();
        let TEX_TRANSFORM_TAG = CString::new("tex_transform").unwrap();
        let SPRITE_COLOR_TAG = CString::new("sprite_color").unwrap();
        let IMAGE_TAG = CString::new("image").unwrap();
        let SIZE_TAG = CString::new("size").unwrap();
        let TEX_SIZE_TAG = CString::new("tex_size").unwrap();


        let FONT_TEX_TAG = CString::new("font_tex").unwrap();
        let OUT_COLOR_TAG = CString::new("out_color").unwrap();
        let LEFT_TOP_TAG = CString::new("left_top").unwrap();
        let RIGHT_BOTTOM_TAG = CString::new("right_bottom").unwrap();
        let TEX_LEFT_TOP_TAG = CString::new("tex_left_top").unwrap();
        let TEX_RIGHT_BOTTOM_TAG = CString::new("tex_right_bottom").unwrap();




        Self {
            GPU_QUAD,

            FILL_SHADER,
            TEX_SHADER,
            COLOR_TAG,
            TRANSFORM_TAG,
            TEX_TRANSFORM_TAG,
            SPRITE_COLOR_TAG,
            IMAGE_TAG,
            SIZE_TAG,
            TEX_SIZE_TAG,

            FONT_SHADER,
            FONT_TEX_TAG,
            OUT_COLOR_TAG,
            LEFT_TOP_TAG,
            RIGHT_BOTTOM_TAG,
            TEX_LEFT_TOP_TAG,
            TEX_RIGHT_BOTTOM_TAG,
        }

    }
}






