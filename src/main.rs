#![allow(dead_code)]
#![allow(unused_imports)]

use sdl2::event::Event;
use sdl2::video::SwapInterval;
use sdl2::VideoSubsystem;

use gl::types::{GLfloat, GLint, GLsizei, GLuint, GLsizeiptr};

use nalgebra::base::{Matrix4, Vector2, Vector3, Vector4};
use nalgebra::{Translation3, Rotation3, Transform3};

use log::{error, warn, info, debug, trace};

use std::time::{Duration, Instant, SystemTime};
use std::os::raw::c_void;

use glyph_brush::{BrushAction, BrushError, GlyphBrushBuilder, GlyphBrush, Section};
use glyph_brush::rusttype::{Scale, Rect, point};

pub mod shader;
pub mod resource_loader;
pub mod graphics;
pub mod config;
pub mod texture;
pub mod text;
pub mod globals;
pub mod gpu_buffer;
pub mod statics;
pub mod dialog;
pub mod input;
pub mod character;
pub mod option;
pub mod map;
pub mod bg;
pub mod script;
pub mod state;
pub mod inventory;
pub mod game_manager;

use crate::resource_loader::ResourceManager;
use crate::gpu_buffer::buffer_data;
use crate::graphics::{TexturedQuad, Quad, construct_transform_matrix};
use crate::statics::{APPLICATION_NAME, APPLICATION_INITIAL_SIZE};
use crate::shader::Shader;
use crate::text::TextRenderer;
use crate::globals::RenderingGlobals;
use crate::config::RenderingConfig;
use crate::di1alog::{DialogRenderer};
use crate::input::{InputManager, LogicalKey};
use crate::character::{CharacterRenderer, CharacterPosition};
use crate::option::OptionRenderer;
use crate::map::{MapManager};
use crate::script::ScriptEngine;



fn main() {

    // initialise the logging subsystem
    simple_logger::init().unwrap();

    // Initialise sdl
    let sdl = sdl2::init().expect("Could not initialise SDL2");

    // Load video subsystem
    let video_subsystem = sdl.video().expect("Could not initialise SDL2 video subsystem");


    video_subsystem.gl_set_swap_interval(SwapInterval::VSync);

    let (ddpi, hdpi, vdpi) = video_subsystem.display_dpi(0).expect("Could not get the display dpi");

    // configure GL versions
    let gl_attr = video_subsystem.gl_attr();
    gl_attr.set_context_profile(sdl2::video::GLProfile::Core);
    gl_attr.set_context_version(3,3);

    // open window
    let window = video_subsystem
        .window(
            APPLICATION_NAME,
            APPLICATION_INITIAL_SIZE.0 as u32,
            APPLICATION_INITIAL_SIZE.1 as u32
        )
        .opengl()
        .build()
        .expect("Could not create SDL2 window");

    // create gl context
    let _gl_context = window.gl_create_context()
        .expect("Could not retrieve opengl context");
    let _gl = gl::load_with(|s| video_subsystem.gl_get_proc_address(s) as *const std::os::raw::c_void);


    // load event pump to retrieve events
    let mut event_pump = sdl.event_pump().unwrap();


    // setup resources and load configurations (maybe move this to external file)
    let mut resource_manager = ResourceManager::new();
    let config = RenderingConfig::new(&mut resource_manager);
    let input_manager = InputManager::default();

    let mut game_manager = resource_manager.load_game_manager("./res/dialog/basic.intro").expect("Could not load game_manager");

    // start the script engine and load up a script
    let mut script_engine = ScriptEngine::new(&mut resource_manager, &config, hdpi).expect("Could not start scripting engine");

    script_engine.load_script("./res/scripts/example.scrpt").expect("Could not load script.");

    let mut game_renderer = game_manager.renderer(&mut script_engine);

    unsafe {
        gl::Viewport(
            0,0,
            APPLICATION_INITIAL_SIZE.0,
            APPLICATION_INITIAL_SIZE.1
        );
        gl::ClearColor(0.3, 0.3, 0.5, 1.0);
        gl::Enable(gl::CULL_FACE);
        gl::Enable(gl::BLEND);
        gl::Enable(gl::FRAMEBUFFER_SRGB);
        gl::BlendFunc(gl::SRC_ALPHA, gl::ONE_MINUS_SRC_ALPHA);
    }

    let mut last_time = SystemTime::now();


    'main: loop {

        let delta_time = {
            let current_time = SystemTime::now();
            let elapsed_time = current_time.duration_since(last_time).expect("Could not get elapsed time");
            last_time = current_time;

            (elapsed_time.as_secs() * 1000) as f32
                + (elapsed_time.subsec_nanos() as f32 / 1_000_000 as f32)
        };


        let inputs = input_manager.poll_events(&mut event_pump);

        if inputs.exit_requested {break 'main}


        unsafe {
            gl::Clear(gl::COLOR_BUFFER_BIT);
        }

        // GAME LOOP

        // parse inputs
        game_renderer.update(inputs, delta_time);

        if game_renderer.exit_requested() {
            break 'main;
        }

        // draw state
        game_renderer.draw();

        // swap buffers
        window.gl_swap_window();
    }


}


