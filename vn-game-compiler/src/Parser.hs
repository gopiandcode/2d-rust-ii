{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}


module Parser (
  parseFile, Info, Program (..), Declaration (..), VariableType (..),
  MapMapping (..), Body, Dialog (..), Statement (..), CharacterPosition (..),
  Expr (..), 
)  where
import Control.Monad.Trans
import Control.Monad.Trans.State
import Control.Monad.Trans.Writer

import Text.Parsec
import System.IO
import Control.Monad
import Text.ParserCombinators.Parsec
import Text.ParserCombinators.Parsec.Expr
import Text.ParserCombinators.Parsec.Language
import qualified Text.ParserCombinators.Parsec.Token as Token
import Data.Data hiding (Infix, Prefix)
import Data.List

import Data.Generics.Uniplate.Data



type Info = (Int,Int)

data Program = Program {
  program_declarations :: [Declaration],
  program_dialogs :: [Dialog]
} deriving (Show)


data Declaration = CharacterDecl Info String String [String]
   | VariableDecl Info VariableType String Expr
   | MapDecl Info String String [MapMapping]
   | BackgroundDecl Info String String
   deriving (Show)

data VariableType = VarInt | VarText | VarBool deriving (Show)

data MapMapping = Item Info String String | Prop Info String String deriving (Show)
type Body = [Statement]
data Dialog = Dialog Info String Body deriving (Show)


data Statement = While Info Expr Body
  | If Info [(Expr, Body)] (Maybe Body)
  | Goto Info String
  | Show Info String
  | Run Info String
  | Drop Info String
  | State Info String String
  | Position Info String CharacterPosition
  | Say Info (Maybe String) Expr
  | Input Info [(Expr, Body)]
  | Assign Info String Expr
  | Remove Info
  deriving (Show)

data CharacterPosition = CharLeft | CharRight | CharCenter | CharOff deriving (Show)
data Expr = BoolConst Info Bool
  | StringConst Info String
  | IntConst Info Integer
  | Variable Info String
  | Check Info String
  | Format Info String [Expr]
  | Not Info Expr
  | And Info Expr Expr
  | Or Info Expr Expr
  | Gt Info Expr Expr
  | Lt Info Expr Expr
  | Gte Info Expr Expr
  | Lte Info Expr Expr
  | Eq Info Expr Expr
  | Neg Info Expr
  | Add Info Expr Expr
  | Sub Info Expr Expr
  | Mul Info Expr Expr
  | Div Info Expr Expr
  deriving (Show)




languageDef =
  emptyDef {
   Token.commentStart = "/*",
   Token.commentEnd = "*/",
   Token.commentLine = "//",
   Token.identStart = letter,
   Token.identLetter = (alphaNum <|> (oneOf "_$")),
   Token.reservedNames = [
     "character",
     "dialog",
     "variable",
     "check",
     "int",
     "text",
     "state",
     "position",
     "speak",
     "info",
     "while",
     "if",
     "else",
     "format",
     "LEFT",
     "RIGHT",
     "CENTER",
     "OFFSCREEN",
     "item",
     "prop",
     "map",
     "bg",
     "run",
     "goto",
     "remove",
     "input",
     "show"
   ],
   Token.reservedOpNames = [
     "+", "-", "*", "/",         -- arithmetic
     "<", ">", ">=", "<=", "==", -- relational
     ":=", "=>", ":", "->",      -- miscelaneous
     "&&", "||", "!"             -- boolean
   ]
 }

lexer = Token.makeTokenParser languageDef
comma = Token.comma lexer
identifier = Token.identifier lexer
reserved = Token.reserved lexer
reservedOp = Token.reservedOp lexer
parens = Token.parens lexer
stringlit = Token.stringLiteral lexer
integer = Token.integer lexer
semi = Token.semi lexer
braces = Token.braces lexer
whiteSpace = Token.whiteSpace lexer


program :: Parser Program
program = do
  decls <- (many declaration);
  dlgs <- (many dialog);
  return $ Program decls dlgs

declaration :: Parser Declaration
declaration = character_declaration <|>
              variable_declaration <|>
              map_declaration <|>
              background_declaration 

get_info :: Parser Info
get_info = do
  state <- getPosition;
  return $ (sourceLine state, sourceColumn state)

character_declaration :: Parser Declaration
character_declaration = do
  info <- get_info;
  reserved "character";
  cname <- identifier;
  reservedOp ":";
  fname <- stringlit;
  states <- braces (sepBy1 identifier comma);
  return $ CharacterDecl info cname fname states


variable_type :: Parser VariableType
variable_type =
  (do reserved "int"; return VarInt) <|>
  (do reserved "text"; return VarText)

variable_declaration :: Parser Declaration
variable_declaration = do
  info <- get_info;
  reserved "variable";
  typ <- variable_type;
  name <- identifier;
  reservedOp ":=";
  expr <- expression;
  semi;
  return $ VariableDecl info typ name expr

background_declaration :: Parser Declaration
background_declaration = do
  info <- get_info;
  reserved "bg";
  name <- identifier;
  reservedOp ":";
  path <- stringlit;
  semi;
  return $ BackgroundDecl info name path

map_mapping :: Parser MapMapping
map_mapping = item_map_mapping <|> prop_map_mapping

item_map_mapping :: Parser MapMapping
item_map_mapping = do
  info <- get_info;
  reserved "item";
  name <- identifier;
  reservedOp "->";
  target <- identifier;
  return $ Item info name target

prop_map_mapping :: Parser MapMapping
prop_map_mapping = do
  info <- get_info;  
  reserved "prop";
  name <- identifier;
  reservedOp "->";
  target <- identifier;
  return $ Prop info name target


map_declaration :: Parser Declaration
map_declaration = do
  info <- get_info;
  reserved "map";
  name <- identifier;
  reservedOp ":";
  path <- stringlit;
  mappings <- braces (sepBy1 map_mapping semi);
  return $ MapDecl info name path mappings

statement :: Parser Statement
statement =
  while_statement <|>
  if_statement <|>
  goto_statement <|>
  show_statement <|>
  run_statement <|>
  drop_statement <|>
  state_statement <|>
  position_statement <|>
  say_statement <|>
  input_statement <|>
  assign_statement <|>
  remove_statement

assign_statement :: Parser Statement
assign_statement = do
  info <- get_info;
  reserved "variable";
  name <- identifier;
  reservedOp ":=";
  expr <- expression;
  semi;
  return $ Assign info name expr

while_statement :: Parser Statement
while_statement = do
  info <- get_info;
  reserved "while";
  cond <- expression;
  bod <- braces body;
  return $ While info cond bod

if_statement :: Parser Statement
if_statement = do
  info <- get_info;
  reserved "if";
  cond <- expression;
  body <- braces body;
  (others, last) <- elseif_statement;
  return $ If info ((cond,body):others) last

else_elseif_statement :: Parser ([(Expr, Body)], Maybe Body)
else_elseif_statement = do
        reserved "if";
        cond <- expression;
        body <- braces body;
        (others, last) <- elseif_statement;    
        return $ ((cond,body):others, last)

else_else_statement :: Parser ([(Expr, Body)], Maybe Body)
else_else_statement = do
      body <- braces body;
      return $ ([], Just body)
  
elseif_statement :: Parser ([(Expr,Body)], Maybe Body)
elseif_statement = (do
    reserved "else";
    (else_elseif_statement <|> else_else_statement)) <|>
    (return $ ([],Nothing))

goto_statement :: Parser Statement
goto_statement = do
  info <- get_info;
  reserved "goto";
  name <- identifier;
  semi;
  return $ Goto info name

show_statement :: Parser Statement
show_statement = do
  info <- get_info;
  reserved "show";
  name <- identifier;
  semi;
  return $ Show info name

run_statement :: Parser Statement
run_statement = do
  info <- get_info;
  reserved "run";
  name <- identifier;
  semi;
  return $ Run info name

drop_statement :: Parser Statement
drop_statement = do
  info <- get_info;
  reserved "drop";
  name <- identifier;
  semi;
  return $ Drop info name

state_statement :: Parser Statement
state_statement = do
  info <- get_info;
  reserved "state";
  name <- identifier;
  state <- identifier;
  semi;
  return $ Parser.State info name state

character_position :: Parser CharacterPosition
character_position =
  (do reserved "LEFT"; return CharLeft) <|>
  (do reserved "RIGHT"; return CharRight) <|>
  (do reserved "CENTER"; return CharCenter) <|>
  (do reserved "OFFSCREEN"; return CharOff)
  

position_statement :: Parser Statement
position_statement = do
  info <- get_info;
  reserved "position";
  name <- identifier;
  position <- character_position;
  semi;
  return $ Position info name position

info_say_statement :: Parser Statement
info_say_statement = do
  info <- get_info;
  reserved "info";
  expr <- expression;
  semi;
  return $ Say info Nothing expr

speak_say_statement :: Parser Statement
speak_say_statement = do
  info <- get_info;
  (reserved "say" <|> reserved "speak");
  name <- identifier;
  expr <- expression;
  semi;
  return $ Say info (Just name) expr

say_statement :: Parser Statement
say_statement = info_say_statement <|> speak_say_statement

input_mapping :: Parser (Expr,Body)
input_mapping = do
  match <- expression;
  reservedOp "->";
  bod <- braces body;
  return $ (match, bod)
  
input_statement :: Parser Statement
input_statement = do
  info <- get_info;
  reserved "input";
  cases <- braces (many input_mapping);
  return $ Input info cases

remove_statement :: Parser Statement
remove_statement = do
  info <- get_info;
  reserved "remove";
  semi;
  return $ Remove info

body :: Parser Body
body = many statement

dialog :: Parser Dialog
dialog = do
  info <- get_info;
  reserved "dialog";
  name <- identifier;
  body <- braces body;
  return $ Dialog info name body

expression :: Parser Expr
expression = buildExpressionParser expression_operators expression_terms

bool_expression :: Parser Expr
bool_expression = do
  info <- get_info;
  (reserved "true" >> (return $ BoolConst info True)) <|>
   (reserved "false" >> (return $ BoolConst info False))

check_expression :: Parser Expr
check_expression = do
  info <- get_info;
  reserved "check";
  str <- parens identifier;
  return $ Check info str

format_contents :: Parser (String,[Expr])
format_contents = do
  txt <- stringlit;
  comma;
  exs <- sepBy1 expression comma;
  return $ (txt, exs)
  
format_expression :: Parser Expr
format_expression = do
  info <- get_info;
  reserved "format";
  (txt,expr) <- parens format_contents;
  return $ Format  info txt expr

string_expression :: Parser Expr
string_expression = do
              info <- get_info;
              txt <- stringlit;
              return $ StringConst info txt

int_expression :: Parser Expr
int_expression = do
  info <- get_info;
  int <- integer;
  return $ IntConst info int

variable_expression :: Parser Expr
variable_expression = do
  info <- get_info;
  var <- identifier;
  return $ Variable info var
  
expression_terms = parens expression <|>
                   bool_expression <|>
                   format_expression <|>
                   string_expression <|>
                   int_expression <|>
                   variable_expression <|>
                   check_expression
                   
expression_operators = [
  [
    Prefix (reservedOp "-" >> get_info >>= \i -> return (Neg i))
  ],
  [
    Infix (reservedOp "*" >> get_info >>= \i -> return (Mul i)) AssocLeft,
    Infix (reservedOp "/" >> get_info >>= \i -> return (Div i)) AssocLeft
  ],
  [
    Infix (reservedOp "+" >> get_info >>= \i -> return (Add i)) AssocLeft,
    Infix (reservedOp "-" >> get_info >>= \i -> return (Sub i)) AssocLeft
  ],
  [
    Infix (reservedOp "==" >> get_info >>= \i -> return (Eq i)) AssocNone,    
    Infix (reservedOp ">" >> get_info >>= \i -> return (Gt i)) AssocNone,
    Infix (reservedOp ">=" >> get_info >>= \i -> return (Gte i)) AssocNone,    
    Infix (reservedOp "<" >> get_info >>= \i -> return (Lt i)) AssocNone,
    Infix (reservedOp "<=" >> get_info >>= \i -> return (Lte i)) AssocNone
  ],
  [
        Prefix (reservedOp "!" >> get_info >>= \i -> return (Not i))
  ],
  [
    Infix (reservedOp "&&" >> get_info >>= \i -> return (And i)) AssocLeft,
    Infix (reservedOp "||" >> get_info >>= \i -> return (Or i)) AssocLeft
  ]
 ]


 
parseFile :: String -> IO Program
parseFile file = do
  file_txt <- readFile file
  case parse program "" file_txt of
    Left e -> print e >> fail "parse error"
    Right r -> return r
