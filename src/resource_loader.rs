use std::io::{Read, Write};
use std::fs::File;
use std::path::Path;
use std::collections::HashMap;
use std::collections::hash_map::Entry;
use std::ffi::CString;
use std::os::raw::c_void;
use std::string::String;
use std::rc::Rc;
use std::cell::RefCell;
use std::ops::{Index, IndexMut};
use std::time::SystemTime;

use crate::shader::{Shader, GLSLError};
use crate::texture::{Texture};
use crate::graphics::{TexturedQuad, Quad};
use crate::dialog::DialogBox;
use crate::character::{Character};
use crate::statics::{CHARACTER_SIZE};
use crate::option::OptionBox;
use crate::map::{Map, Item};
use crate::map::masker::{ImageMask, Rect, find_non_transparent_regions};
use crate::inventory::InventoryRendererState;
use crate::game_manager::GameManagerState;

use gl::types::{GLfloat, GLint, GLsizei, GLuint, GLsizeiptr};
use regex::Regex;
use nalgebra::base::{Vector4, Vector2};
use lazy_static::lazy_static;

#[derive(Debug)]
pub enum ResourceError {
    FileError(String),
    IncompatibleFormat(String),
    CompilationError(GLSLError),
    InternalError(String),
}

#[derive(Debug, Copy, Clone)]
pub struct TextureID(usize);

#[derive(Debug, Copy, Clone)]
pub struct ShaderID(usize);


pub struct ResourceManager {
    texture_map: HashMap<String, TextureID>,
    shader_map: HashMap<String, ShaderID>,
    font_map: HashMap<String, Vec<u8>>,

    loaded_textures: Vec<Texture>,
    loaded_shaders: Vec<Shader>,
}


impl TextureID {
    pub fn get_dimensions(&self, resource_manager: &ResourceManager) -> (u32, u32) {
        let texture = &resource_manager[*self];
        (texture.width, texture.height)
    }


    pub fn bind(&self, resource_manager: &ResourceManager, position: GLuint) {
        let texture = &resource_manager[*self];
        texture.bind(position);
    }
}


impl Index<TextureID> for ResourceManager {
    type Output = Texture;

    fn index(&self, id: TextureID) -> &Self::Output {
        &self.loaded_textures[id.0]
    }
}

impl Index<ShaderID> for ResourceManager {
    type Output = Shader;

    fn index(&self, id: ShaderID) -> &Self::Output {
        &self.loaded_shaders[id.0]
    }
}

impl IndexMut<ShaderID> for ResourceManager {

    fn index_mut(&mut self, id: ShaderID) -> &mut Self::Output {
        &mut self.loaded_shaders[id.0]
    }
}

impl ResourceManager {


    pub fn new() -> Self {
        Self {
            texture_map: HashMap::new(),
            shader_map: HashMap::new(),
            font_map: HashMap::new(),
            loaded_textures: Vec::new(),
            loaded_shaders: Vec::new()
        }
    }


    pub fn load_shader<T: AsRef<Path>, U: AsRef<Path>>(
        &mut self,
        vs_file: T,
        fs_file: U,
    ) -> Result<ShaderID, ResourceError> {

        let key = vs_file.as_ref().to_str().unwrap().to_owned() + fs_file.as_ref().to_str().unwrap();

        match self.shader_map.entry(key) {
            Entry::Occupied(entry) => Ok(*entry.get()),
            Entry::Vacant(entry) => {
                let mut vs_string = String::new();
                let mut fs_string = String::new();

                {
                    let mut vs = File::open(vs_file).map_err(|e| ResourceError::FileError(e.to_string()))?;
                    let mut fs = File::open(fs_file).map_err(|e| ResourceError::FileError(e.to_string()))?;

                    vs.read_to_string(&mut vs_string).map_err(|e| ResourceError::FileError(e.to_string()))?;
                    fs.read_to_string(&mut fs_string).map_err(|e| ResourceError::FileError(e.to_string()))?;
                }


                let shader = Shader::new(vs_string.into_bytes(), fs_string.into_bytes()).map_err(|e| ResourceError::CompilationError(e))?;

                let id = ShaderID(self.loaded_shaders.len());
                self.loaded_shaders.push(shader);

                Ok(id)
            }
        }

    }

    pub fn load_font<T: AsRef<Path> + Into<String>>(&mut self, font_path: T) -> Result<Vec<u8>, ResourceError> {
        let font_path: String = font_path.into();

        match self.font_map.entry(font_path.clone()) {
            Entry::Occupied(entry) => Ok(entry.get().to_vec()),
            Entry::Vacant(entry) => {

                let mut file = File::open(font_path).map_err(|err| ResourceError::FileError(err.to_string()))?;

                let mut buf : Vec<u8> = Vec::new();

                file.read_to_end(&mut buf);

                Ok(entry.insert(buf).to_vec())
            }
        }
    }




    pub fn load_texture<T: Into<String>>(
        &mut self,
        image_file: T
    ) -> Result<TextureID, ResourceError> {

        let image_file = image_file.into();

        match self.texture_map.entry(image_file.clone()) {
            Entry::Occupied(entry) => Ok(*entry.get()),
            Entry::Vacant(entry) => {
                let img = image::open(&image_file).map_err(|e| ResourceError::FileError(e.to_string()))?;

                let img = match img {
                    image::DynamicImage::ImageRgba8(img) => img,
                    x => x.to_rgba()
                };

                let (width, height) = img.dimensions();


                let texture = Texture::new(
                    width, height,
                    img.as_ptr() as *const c_void,
                ).build().map_err(|e| ResourceError::IncompatibleFormat(e))?;

                let id = TextureID(self.loaded_textures.len());
                self.loaded_textures.push(texture);

                Ok(*entry.insert(id))
            }
        }
    }


    pub fn load_inventory_renderer<T: AsRef<Path>>(&mut self, base: T) -> Result<InventoryRendererState, ResourceError> {

        let dialog_string = {
            let mut buf = String::new();
            let mut file = File::open(base).map_err(|err| ResourceError::FileError(err.to_string()))?;
            file.read_to_string(&mut buf).map_err(
                |err| ResourceError::FileError(err.to_string())
            )?;
            buf
        };

        lazy_static! {
            static ref dialog_regex: Regex = Regex::new(
                r"(?m)^ *(item_box|arrow|base_color|arrow_color|inactive_arrow_color|item_color|active_item_color) *: *(.*)$"
            ).unwrap();
        }


        let mut item_box: Option<_> = None;
        let mut arrow_tex: Option<_> = None;

        let mut base_color: Option<_> = None;

        let mut arrow_color: Option<_> = None;
        let mut inactive_arrow_color: Option<_> = None;

        let mut item_box_color: Option<_> = None;
        let mut active_item_box_color: Option<_> = None;

        // LOAD string format from file
        for line in dialog_string.lines().filter(|line| !line.is_empty()) {
            let dialog_capture = dialog_regex.captures(line).ok_or(
                ResourceError::IncompatibleFormat(
                    format!("incorrect line format: {:?}", line)
                )
            )?;
            let label = dialog_capture.get(1).ok_or(
                ResourceError::IncompatibleFormat(
                    format!("incorrect line format: {:?}", line)
                )
            )?.as_str();
            let value = dialog_capture.get(2).ok_or(
                ResourceError::IncompatibleFormat(
                    format!("incorrect line format: {:?}", line)
                )
            )?.as_str();

            if label == "base_color" || label == "arrow_color" || label == "inactive_arrow_color"
                || label == "item_color" || label == "active_item_color" {
                let result: Vec<_> = value.split(',').take(4).collect();

                let c0 = result[0].trim().parse::<f32>().map_err(
                    |err| ResourceError::IncompatibleFormat(err.to_string())
                )?;
                let c1 = result[1].trim().parse::<f32>().map_err(
                    |err| ResourceError::IncompatibleFormat(err.to_string())
                )?;
                let c2 = result[2].trim().parse::<f32>().map_err(
                    |err| ResourceError::IncompatibleFormat(err.to_string())
                )?;
                let c3 = result[3].trim().parse::<f32>().map_err(
                    |err| ResourceError::IncompatibleFormat(err.to_string())
                )?;


                if label == "base_color" {
                    base_color = Some(Vector4::new(c0, c1, c2, c3));
                } else if label == "arrow_color"  {
                    arrow_color = Some(Vector4::new(c0, c1, c2, c3));
                } else if label == "inactive_arrow_color" {
                    inactive_arrow_color = Some(Vector4::new(c0, c1, c2, c3));
                } else if label == "item_color" {
                    item_box_color = Some(Vector4::new(c0, c1, c2, c3));
                } else if label == "active_item_color" {
                    active_item_box_color = Some(Vector4::new(c0, c1, c2, c3));
                }
                }

            else if label == "item_box" {
                item_box = Some(value.trim());
            } else if label == "arrow" {
                arrow_tex = Some(value.trim());
            } else {
                unreachable!();
            }
        }

        let item_box = item_box.unwrap_or(
            "./res/images/empty.png"
        );
        let arrow_tex = arrow_tex.unwrap_or(
            "./res/images/empty.png"
        );
        let base_color = base_color.unwrap_or(Vector4::new(0.2, 0.4, 0.6, 0.5));
        let arrow_color = arrow_color.unwrap_or(Vector4::new(1.0, 1.0, 1.0, 1.0));
        let inactive_arrow_color = inactive_arrow_color.unwrap_or(Vector4::new(0.8, 0.8, 0.8, 1.0));
        let item_box_color = item_box_color.unwrap_or(Vector4::new(0.8, 0.8, 0.8, 1.0));
        let active_item_box_color = active_item_box_color.unwrap_or(Vector4::new(1.0, 1.0, 1.0, 1.0));

        let item_box = self.load_texture(item_box)?;
        let arrow_tex = self.load_texture(arrow_tex)?;



        Ok(
            InventoryRendererState::new(
                item_box, arrow_tex, base_color, arrow_color,
                inactive_arrow_color, item_box_color, active_item_box_color
            )
        )
    }



    pub fn load_dialog_box<T: AsRef<Path>>(&mut self, base: T) -> Result<DialogBox, ResourceError> {

        let dialog_string = {
            let mut buf = String::new();
            let mut file = File::open(base).map_err(|err| ResourceError::FileError(err.to_string()))?;
            file.read_to_string(&mut buf).map_err(
                |err| ResourceError::FileError(err.to_string())
            )?;
            buf
        };

        lazy_static! {
            static ref dialog_regex: Regex = Regex::new(
                r"(?m)^ *(base|corner|edge|texture_color|text_color) *: *(.*)$"
            ).unwrap();
        }


        let mut base_color: Option<_> = None;
        let mut texture_color: Option<_> = None;
        let mut text_color: Option<_> = None;
        let mut edge_texture: Option<_> = None;
        let mut corner_texture: Option<_> = None;

        // LOAD string format from file
        for line in dialog_string.lines().filter(|line| !line.is_empty()) {
            let dialog_capture = dialog_regex.captures(line).ok_or(
                ResourceError::IncompatibleFormat(
                    format!("incorrect line format: {:?}", line)
                )
            )?;
            let label = dialog_capture.get(1).ok_or(
                ResourceError::IncompatibleFormat(
                    format!("incorrect line format: {:?}", line)
                )
            )?.as_str();
            let value = dialog_capture.get(2).ok_or(
                ResourceError::IncompatibleFormat(
                    format!("incorrect line format: {:?}", line)
                )
            )?.as_str();

            if label == "base" || label == "texture_color" || label == "text_color"{
                let result: Vec<_> = value.split(',').take(4).collect();

                let c0 = result[0].trim().parse::<f32>().map_err(
                    |err| ResourceError::IncompatibleFormat(err.to_string())
                )?;
                let c1 = result[1].trim().parse::<f32>().map_err(
                    |err| ResourceError::IncompatibleFormat(err.to_string())
                )?;
                let c2 = result[2].trim().parse::<f32>().map_err(
                    |err| ResourceError::IncompatibleFormat(err.to_string())
                )?;
                let c3 = result[3].trim().parse::<f32>().map_err(
                    |err| ResourceError::IncompatibleFormat(err.to_string())
                )?;

                if label == "base" {
                    base_color = Some(Vector4::new(c0, c1, c2, c3));
                } else if label == "texture_color" {
                    texture_color = Some(Vector4::new(c0, c1, c2, c3));
                } else {
                    text_color = Some(Vector4::new(c0, c1, c2, c3));
                }
            } else if label == "corner" {
                corner_texture = Some(value);
            } else if label == "edge" {
                edge_texture = Some(value);
            } else {
                unreachable!();
            }
        }

        let base_color = base_color.unwrap_or(Vector4::new(0.3, 0.3, 0.5, 1.0));
        let texture_color = texture_color.unwrap_or(Vector4::new(0.0, 0.0, 0.0, 0.0));
        let text_color = text_color.unwrap_or(Vector4::new(1.0, 1.0, 1.0, 1.0));

        let edge_texture = edge_texture.unwrap_or(
            "./res/images/empty.png"
        );
        let corner_texture = corner_texture.unwrap_or(
            "./res/images/empty.png"
        );

        let edge_texture = self.load_texture(edge_texture)?;
        let corner_texture = self.load_texture(corner_texture)?;

        let edge = TexturedQuad::new(
            // position
            Vector2::new(0.0, 0.0),
            // size
            Vector2::new(0.0, 0.0),
            // rotation
            0.0,
            // tex_position
            Vector2::new(0.0, 0.0),
            // tex_size
            Vector2::new(0.0, 0.0),
            // tex_rotation
            0.0,
            // tex_color
            texture_color,
            edge_texture
        );

        let corner = TexturedQuad::new(
            // position
            Vector2::new(0.0, 0.0),
            // size
            Vector2::new(0.0, 0.0),
            // rotation
            0.0,
            // tex_position
            Vector2::new(0.0, 0.0),
            // tex_size
            Vector2::new(0.0, 0.0),
            // tex_rotation
            0.0,
            // tex_color
            texture_color,
            corner_texture
        );

        let base = Quad::new(
            // position
            Vector2::new(0.0, 0.0),
            // size
            Vector2::new(0.0, 0.0),
            // rotation
            0.0,
            // color
            base_color
        );



        Ok(
            DialogBox::new(
                corner, edge, base, text_color, &self
            )
        )
    }


    pub fn load_option_box<T: AsRef<Path>>(&mut self, base: T) -> Result<OptionBox, ResourceError> {

        let option_string = {
            let mut buf = String::new();
            let mut file = File::open(base).map_err(|err| ResourceError::FileError(err.to_string()))?;
            file.read_to_string(&mut buf).map_err(
                |err| ResourceError::FileError(err.to_string())
            )?;
            buf
        };

        lazy_static!{
            static ref option_regex: Regex = Regex::new(
                r"(?m)^ *(inactive_base|base|inactive_corner|inactive_edge|corner|edge|texture_color|text_color|inactive_text_color) *: *(.*)$"
            ).unwrap();
        }


        let mut base_color: Option<_> = None;
        let mut inactive_base_color: Option<_> = None;

        let mut texture_color: Option<_> = None;
        let mut text_color: Option<_> = None;
        let mut inactive_text_color: Option<_> = None;

        let mut edge_texture: Option<_> = None;
        let mut corner_texture: Option<_> = None;

        let mut inactive_edge_texture: Option<_> = None;
        let mut inactive_corner_texture: Option<_> = None;

        // LOAD string format from file
        for line in option_string.lines().filter(|line| !line.is_empty()) {
            let option_capture = option_regex.captures(line).ok_or(
                ResourceError::IncompatibleFormat(
                    format!("incorrect line format: {:?}", line)
                )
            )?;
            let label = option_capture.get(1).ok_or(
                ResourceError::IncompatibleFormat(
                    format!("incorrect line format: {:?}", line)
                )
            )?.as_str();
            let value = option_capture.get(2).ok_or(
                ResourceError::IncompatibleFormat(
                    format!("incorrect line format: {:?}", line)
                )
            )?.as_str();

            if label == "base" || label == "texture_color" || label == "text_color" || label == "inactive_base" || label == "inactive_text_color" {
                let result: Vec<_> = value.split(',').take(4).collect();

                let c0 = result[0].trim().parse::<f32>().map_err(
                    |err| ResourceError::IncompatibleFormat(err.to_string())
                )?;
                let c1 = result[1].trim().parse::<f32>().map_err(
                    |err| ResourceError::IncompatibleFormat(err.to_string())
                )?;
                let c2 = result[2].trim().parse::<f32>().map_err(
                    |err| ResourceError::IncompatibleFormat(err.to_string())
                )?;
                let c3 = result[3].trim().parse::<f32>().map_err(
                    |err| ResourceError::IncompatibleFormat(err.to_string())
                )?;

                if label == "base" {
                    base_color = Some(Vector4::new(c0, c1, c2, c3));
                } else if label == "texture_color" {
                    texture_color = Some(Vector4::new(c0, c1, c2, c3));
                } else if label == "text_color" {
                    text_color = Some(Vector4::new(c0, c1, c2, c3));
                } else if label == "inactive_base" {
                    inactive_base_color = Some(Vector4::new(c0, c1, c2, c3));
                } else if label == "inactive_text_color" {
                    inactive_text_color = Some(Vector4::new(c0, c1, c2, c3));
                }
            } else if label == "corner" {
                corner_texture = Some(value);
            } else if label == "edge" {
                edge_texture = Some(value);
            } else if label == "inactive_corner" {
                inactive_corner_texture = Some(value);
            } else if label == "inactive_edge" {
                inactive_edge_texture = Some(value);
            } else {
                unreachable!();
            }
        }

        let base_color = base_color.unwrap_or(Vector4::new(0.3, 0.3, 0.5, 1.0));
        let inactive_base_color = inactive_base_color.unwrap_or(base_color.clone());

        let texture_color = texture_color.unwrap_or(Vector4::new(0.0, 0.0, 0.0, 0.0));
        let text_color = text_color.unwrap_or(Vector4::new(1.0, 1.0, 1.0, 1.0));
        let inactive_text_color = inactive_text_color.unwrap_or(text_color.clone());

        let edge_texture = edge_texture.unwrap_or(
            "./res/images/empty.png"
        );
        let corner_texture = corner_texture.unwrap_or(
            "./res/images/empty.png"
        );

        let inactive_edge_texture = inactive_edge_texture.unwrap_or(
            edge_texture
        );
        let inactive_corner_texture = inactive_corner_texture.unwrap_or(
            corner_texture
        );


        let edge_texture = self.load_texture(edge_texture)?;
        let corner_texture = self.load_texture(corner_texture)?;

        let inactive_edge_texture = self.load_texture(inactive_edge_texture)?;
        let inactive_corner_texture = self.load_texture(inactive_corner_texture)?;

        let edge = TexturedQuad::new(
            // position
            Vector2::new(0.0, 0.0),
            // size
            Vector2::new(0.0, 0.0),
            // rotation
            0.0,
            // tex_position
            Vector2::new(0.0, 0.0),
            // tex_size
            Vector2::new(0.0, 0.0),
            // tex_rotation
            0.0,
            // tex_color
            texture_color,
            edge_texture
        );

        let corner = TexturedQuad::new(
            // position
            Vector2::new(0.0, 0.0),
            // size
            Vector2::new(0.0, 0.0),
            // rotation
            0.0,
            // tex_position
            Vector2::new(0.0, 0.0),
            // tex_size
            Vector2::new(0.0, 0.0),
            // tex_rotation
            0.0,
            // tex_color
            texture_color,
            corner_texture
        );

        let base = Quad::new(
            // position
            Vector2::new(0.0, 0.0),
            // size
            Vector2::new(0.0, 0.0),
            // rotation
            0.0,
            // color
            base_color
        );



        Ok(
            OptionBox::new(
                (corner_texture, inactive_corner_texture),
                (edge_texture, inactive_edge_texture),
                (base_color, inactive_base_color),
                (text_color, inactive_text_color),
                corner, edge, base, &self
            )
        )
    }

    pub fn load_image<P : AsRef<Path>>(&mut self, path: &P) -> Result<ImageMask, ResourceError> {
        let image = image::open(path);
        match image {
            Err(e) => {
                Err(
                    ResourceError::FileError(
                        format!("Err: Could not parse image format - {:?}", e)
                    )
                )
            }
            Ok(image) => {
                let image = image.to_luma_alpha();
                Ok(ImageMask::new(&image))
            }
        }
    }



    pub fn load_map<T: AsRef<Path>>(&mut self, base: T) ->
        Result<(
            TextureID, TextureID, Vec<(GLfloat,GLfloat,GLfloat,GLfloat)>,
            Vec<(String, TextureID, TextureID, Vector2<GLfloat>, Vector2<GLfloat>)>,
            Vec<String>
        ), ResourceError>
    {

        let mut start_time = SystemTime::now();

        let map_file_string = {
            let mut buf = String::new();
            let mut file = File::open(base).map_err(|err| ResourceError::FileError(err.to_string()))?;
            file.read_to_string(&mut buf).map_err(
                |err| ResourceError::FileError(err.to_string())
            )?;
            buf
        };

        println!("load_map: loaded map file - {:?}", {
            let new_time = SystemTime::now();
            let duration = new_time.duration_since(start_time);
            start_time = new_time;
            duration
        });


        lazy_static!{
            static ref map_file_regex: Regex = Regex::new(
                r#"(?m)^ *(base|mask|item|prop) *: *(?:"(.*)"|(\w+) *"(.+)" *"(.+)" *(-?\d+\.\d+) *(-?\d+\.\d+) *(-?\d+\.\d+) *(-?\d+.\d+)|((?: *\w+)+)) *$"#
            ).unwrap();
        }

        println!("load_map: constructed map regex - {:?}", {
            let new_time = SystemTime::now();
            let duration = new_time.duration_since(start_time);
            start_time = new_time;
            duration
        });



        let mut base_tex: Option<_> = None;
        let mut mask_tex: Option<_> = None;
        let mut prop_vec: Option<_> = None;
        let mut items: Vec<_> = Vec::new();

        // LOAD string format from file
        for line in map_file_string.lines().filter(|line| !line.is_empty()) {
            let map_capture = map_file_regex.captures(line).ok_or(
                ResourceError::IncompatibleFormat(
                    format!("incorrect line format: {:?}", line)
                )
            )?;

            let label = map_capture.get(1).ok_or(
                ResourceError::IncompatibleFormat(
                    format!("incorrect line format: {:?}", line)
                )
            )?.as_str();


            if label == "base" || label == "mask" {
                let value = map_capture.get(2).ok_or(
                    ResourceError::IncompatibleFormat(
                        format!("incorrect line format: {:?}", line)
                    )
                )?.as_str();

                if label == "base" {
                    base_tex = Some(value);
                } else {
                    mask_tex = Some(value);
                }
            } else if label == "prop" {
                let props = map_capture.get(10).ok_or(
                    ResourceError::IncompatibleFormat(
                        format!("incorrect line format: {:?}", line)
                    )
                )?.as_str();

                prop_vec = Some(props.split(" "));
            } else if label == "item" {
                let name = map_capture.get(3).ok_or(
                    ResourceError::IncompatibleFormat(
                        format!("incorrect line format: {:?}", line)
                    )
                )?.as_str();

                let map_tex = map_capture.get(4).ok_or(
                    ResourceError::IncompatibleFormat(
                        format!("incorrect line format: {:?}", line)
                    )
                )?.as_str();

                let inv_tex = map_capture.get(5).ok_or(
                    ResourceError::IncompatibleFormat(
                        format!("incorrect line format: {:?}", line)
                    )
                )?.as_str();

                let pos_x = map_capture.get(6).ok_or(
                    ResourceError::IncompatibleFormat(
                        format!("incorrect line format: {:?}", line)
                    )
                )?.as_str().parse::<f32>().map_err(
                    |err| ResourceError::IncompatibleFormat(err.to_string())
                )?;

                let pos_y = map_capture.get(7).ok_or(
                    ResourceError::IncompatibleFormat(
                        format!("incorrect line format: {:?}", line)
                    )
                )?.as_str().parse::<f32>().map_err(
                    |err| ResourceError::IncompatibleFormat(err.to_string())
                )?;

                let size_x = map_capture.get(8).ok_or(
                    ResourceError::IncompatibleFormat(
                        format!("incorrect line format: {:?}", line)
                    )
                )?.as_str().parse::<f32>().map_err(
                    |err| ResourceError::IncompatibleFormat(err.to_string())
                )?;

                let size_y = map_capture.get(8).ok_or(
                    ResourceError::IncompatibleFormat(
                        format!("incorrect line format: {:?}", line)
                    )
                )?.as_str().parse::<f32>().map_err(
                    |err| ResourceError::IncompatibleFormat(err.to_string())
                )?;

                items.push(
                    (name, map_tex, inv_tex, pos_x, pos_y, size_x, size_y)
                );
            }

        }

        println!("load_map: parsed all lines - {:?}", {
            let new_time = SystemTime::now();
            let duration = new_time.duration_since(start_time);
            start_time = new_time;
            duration
        });


        let base_tex = base_tex.ok_or(ResourceError::IncompatibleFormat(String::from(
            "No base texture provided for map"
        )))?;

        println!("load_map: loaded base tex - {:?}", {
            let new_time = SystemTime::now();
            let duration = new_time.duration_since(start_time);
            start_time = new_time;
            duration
        });



        let mask_tex = mask_tex.ok_or(ResourceError::IncompatibleFormat(String::from(
            "No mask texture provided for map"
        )))?;

        println!("load_map: loaded mask_tex - {:?}", {
            let new_time = SystemTime::now();
            let duration = new_time.duration_since(start_time);
            start_time = new_time;
            duration
        });


        let props = {
            let cache_tex = String::from(mask_tex) + ".cache";
            let mask_attr = std::fs::metadata(mask_tex).map_err(
                |err| ResourceError::FileError(err.to_string())
            )?;

            let cached = match File::open(&cache_tex) {
                Ok(mut f) => {
                    match f.metadata() {
                        Ok(fm) => match fm.modified() {
                            Ok(fmt) => match mask_attr.modified() {
                                Ok(mfmt) =>
                                    if fmt < mfmt {
                                        None
                                    } else {
                                        let mut buf = Vec::new();
                                        f.read_to_end(&mut buf);
                                        let decoded: Result<Vec<Rect>, _> = bincode::deserialize(&buf[..]);
                                        decoded.ok()
                                    }
                                Err(_) => None
                            }
                            Err(_) => None
                        }
                        Err(_) => None
                    }
                }
                _ => None
            };

            match cached {
                Some(v) => {v},
                None => {
                    let mask_tex_tmp = self.load_image(&mask_tex)?;
                    let props = find_non_transparent_regions(&mask_tex_tmp);

                    let encoded = bincode::serialize(&props);

                    match File::create(&cache_tex) {
                        Ok(mut f) => match encoded {
                            Ok(enc) => {
                                f.write_all(&enc);
                            }
                            _ => ()
                        }
                        _ => (),
                    }

                    props
                }
            }

        };

        println!("load_map: parsed map segments - {:?}", {
            let new_time = SystemTime::now();
            let duration = new_time.duration_since(start_time);
            start_time = new_time;
            duration
        });



        let base_tex = self.load_texture(base_tex)?;
        let mask_tex = self.load_texture(mask_tex)?;

        println!("load_map: loaded textures - {:?}", {
            let new_time = SystemTime::now();
            let duration = new_time.duration_since(start_time);
            start_time = new_time;
            duration
        });


        let prop_vec = prop_vec.map(|x| x.map(|y| y.into()).collect()).unwrap_or(
            (0..props.len()).map(|x| format!("prop{:?}", x)).collect::<Vec<String>>()
        );

        (if prop_vec.len() == props.len() { Ok(())} else {Err(ResourceError::IncompatibleFormat(
            format!("missing names for all map props: len {:?} != {:?}", prop_vec, props.len())
        ))})?;

        let items = {
            let mut new_items = Vec::new();

            for (name, map_tex, inv_tex, pos_x, pos_y, size_x, size_y) in items {
                let map_tex = self.load_texture(map_tex)?;
                let inv_tex = self.load_texture(inv_tex)?;

                let pos : Vector2<GLfloat> = Vector2::new(pos_x, pos_y);
                let size : Vector2<GLfloat> = Vector2::new(size_x, size_y);

                new_items.push((name.into(), map_tex, inv_tex, pos, size));
            }

            new_items
        };

        println!("load_map: loaded items - {:?}", {
            let new_time = SystemTime::now();
            let duration = new_time.duration_since(start_time);
            start_time = new_time;
            duration
        });


        let props = props.into_iter().map(|rect|
                        (rect.0 as f32, rect.1 as f32, rect.2 as f32, rect.3 as f32)
        ).collect::<Vec<(GLfloat,GLfloat,GLfloat,GLfloat)>>();

        Ok((base_tex, mask_tex, props, items, prop_vec))
    }


    pub fn load_character<T: Into<String>>(&mut self, image_file: T) -> Result<Character, ResourceError> {
        let texture_id = self.load_texture(image_file)?;
        let (width, height) = texture_id.get_dimensions(self);
        let (width, height) = (width as usize, height as usize);

        if (width % CHARACTER_SIZE.0 as usize  != 0) || (height % CHARACTER_SIZE.1 as usize != 0) {
            Err(ResourceError::IncompatibleFormat(
                format!("Bad image size - {:?}x{:?} is not a multiple of {:?}x{:?}",
                        width, height,
                        CHARACTER_SIZE.0, CHARACTER_SIZE.1
                )
            ))
        } else {
            let texture = TexturedQuad::new(
                Vector2::new(0.0, 0.0),
                Vector2::new(0.0, 0.0),
                0.0,
                Vector2::new(0.0, 0.0),
                Vector2::new(0.0, 0.0),
                0.0,
                Vector4::new(1.0, 1.0, 1.0, 1.0),
                texture_id
            );

            Ok(
                Character::new(texture, width, height)
            )
        }
    }


    pub fn load_game_manager<T: AsRef<Path>>(&mut self, base: T) -> Result<GameManagerState, ResourceError> {

        let game_string = {
            let mut buf = String::new();
            let mut file = File::open(base).map_err(|err| ResourceError::FileError(err.to_string()))?;
            file.read_to_string(&mut buf).map_err(
                |err| ResourceError::FileError(err.to_string())
            )?;
            buf
        };

        lazy_static! {
            static ref game_regex: Regex = Regex::new(
                r"(?m)^ *(start_screen_bg|start_screen_fg|start_screen_button|start_screen_active_color|start_screen_inactive_color|save_screen_bg|save_screen_button|save_screen_saves_button|save_screen_active_color|save_screen_inactive_color) *: *(.*)$"
            ).unwrap();
        }


        let mut start_screen_bg: Option<_> = None;
        let mut start_screen_fg: Option<_> = None;
        let mut start_screen_button: Option<_> = None;

        let mut start_screen_active_color: Option<_> = None;
        let mut start_screen_inactive_color: Option<_> = None;

        let mut save_screen_bg: Option<_> = None;
        let mut save_screen_button: Option<_> = None;
        let mut save_screen_saves_button: Option<_> = None;

        let mut save_screen_active_color: Option<_> = None;
        let mut save_screen_inactive_color: Option<_> = None;

        // LOAD string format from file
        for line in game_string.lines().filter(|line| !line.is_empty()) {
            let text_capture = game_regex.captures(line).ok_or(
                ResourceError::IncompatibleFormat(
                    format!("incorrect line format: {:?}", line)
                )
            )?;
            let label = text_capture.get(1).ok_or(
                ResourceError::IncompatibleFormat(
                    format!("incorrect line format: {:?}", line)
                )
            )?.as_str();
            let value = text_capture.get(2).ok_or(
                ResourceError::IncompatibleFormat(
                    format!("incorrect line format: {:?}", line)
                )
            )?.as_str();

            if label == "start_screen_active_color" || label == "start_screen_inactive_color"
                || label == "save_screen_inactive_color" || label == "save_screen_active_color"  {
                let result: Vec<_> = value.split(',').take(4).collect();

                let c0 = result[0].trim().parse::<f32>().map_err(
                    |err| ResourceError::IncompatibleFormat(err.to_string())
                )?;
                let c1 = result[1].trim().parse::<f32>().map_err(
                    |err| ResourceError::IncompatibleFormat(err.to_string())
                )?;
                let c2 = result[2].trim().parse::<f32>().map_err(
                    |err| ResourceError::IncompatibleFormat(err.to_string())
                )?;
                let c3 = result[3].trim().parse::<f32>().map_err(
                    |err| ResourceError::IncompatibleFormat(err.to_string())
                )?;


                if label == "start_screen_active_color" {
                    start_screen_active_color = Some(Vector4::new(c0, c1, c2, c3));
                } else if label == "start_screen_inactive_color"  {
                    start_screen_inactive_color = Some(Vector4::new(c0, c1, c2, c3));
                } else if label == "save_screen_active_color" {
                    save_screen_active_color = Some(Vector4::new(c0, c1, c2, c3));
                } else if label == "save_screen_inactive_color" {
                    save_screen_inactive_color = Some(Vector4::new(c0, c1, c2, c3));
                } else {
                    unreachable!();
                }
            } else if label == "start_screen_bg" {
                start_screen_bg = Some(value.trim());
            } else if label == "start_screen_fg" {
                start_screen_fg = Some(value.trim());
            } else if label == "start_screen_button" {
                start_screen_button = Some(value.trim());
            } else if label == "save_screen_bg" {
                save_screen_bg = Some(value.trim());
            } else if label == "save_screen_button" {
                save_screen_button = Some(value.trim());
            } else if label == "save_screen_saves_button" {
                save_screen_saves_button = Some(value.trim());
            } else {
                unreachable!();
            }
        }


        let start_screen_bg = start_screen_bg.ok_or(ResourceError::IncompatibleFormat(
            String::from("Missing start screen background image")
        ))?;
        let start_screen_fg = start_screen_fg.ok_or(ResourceError::IncompatibleFormat(
            String::from("Missing start screen foreground image")
        ))?;

        let start_screen_button = start_screen_button.ok_or(ResourceError::IncompatibleFormat(
            String::from("Missing start screen button image")
        ))?;

        let start_screen_active_color = start_screen_active_color.ok_or(ResourceError::IncompatibleFormat(
            String::from("Missing start screen active color")
        ))?;
        let start_screen_inactive_color = start_screen_inactive_color.ok_or(ResourceError::IncompatibleFormat(
            String::from("Missing start screen inactive color")
        ))?;

        let save_screen_bg = save_screen_bg.ok_or(ResourceError::IncompatibleFormat(
            String::from("Missing save screen background image")
        ))?;
        let save_screen_button = save_screen_button.ok_or(ResourceError::IncompatibleFormat(
            String::from("Missing save screen button image")
        ))?;
        let save_screen_saves_button = save_screen_saves_button.ok_or(ResourceError::IncompatibleFormat(
            String::from("Missing save screen saves button image")
        ))?;

        let save_screen_active_color = save_screen_active_color.ok_or(ResourceError::IncompatibleFormat(
            String::from("Missing start screen active color")
        ))?;
        let save_screen_inactive_color = save_screen_inactive_color.ok_or(ResourceError::IncompatibleFormat(
            String::from("Missing start screen inactive color")
        ))?;


        let start_screen_bg = self.load_texture(start_screen_bg)?;
        let start_screen_fg = self.load_texture(start_screen_fg)?;
        let start_screen_button = self.load_texture(start_screen_button)?;

        let save_screen_bg = self.load_texture(save_screen_bg)?;
        let save_screen_button = self.load_texture(save_screen_button)?;
        let save_screen_saves_button = self.load_texture(save_screen_saves_button)?;



        Ok(
            GameManagerState::new(
                start_screen_bg, start_screen_fg, start_screen_button, start_screen_active_color,
                start_screen_inactive_color, save_screen_bg, save_screen_button, save_screen_saves_button,
                save_screen_active_color, save_screen_inactive_color,
            )
        )
    }

}
