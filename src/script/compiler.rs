use regex::Regex;
use std::borrow::Borrow;
use std::str;
use std::iter::Iterator;
use std::collections::HashSet;
use std::collections::HashMap;
use std::time::SystemTime;

use crate::bg::BackgroundID;
use crate::map::{MapID, ItemID, PropID, SelectID};
use crate::script::{DialogID, InstructionID};
use crate::state::{StateManager, TextVariableID, NumericVariableID, VariableID};
use crate::character::{CharacterID, CharacterPosition};
use crate::character::StateID;



use crate::script::parser::{
    ScriptParser, ParseError, parse_program,
    ASTProgram, ASTDeclaration, ASTMapDeclaration, ASTMapMapping,
    ASTVariableDeclaration, ASTBackgroundDeclaration, ASTCharacterDeclaration, ASTDialog,
    ASTDialogBody, ASTWhileOp, ASTIfOp, ASTDialogOperations, ASTSayOperation, ASTSpeakable,
    ASTInputOperation, ASTExpression, ASTBooleanExpression, ASTAssignmentOperation, ASTFormatExpression,
};

use crate::script::lexer::{ScriptLexer, LexError, ScriptToken};
use crate::resource_loader::{ResourceManager, ResourceError};
use crate::bg::BackgroundManager;
use crate::map::MapManager;
use crate::character::CharacterRenderer;

pub struct TempState<'a, 'rm, 'm> {
    resource_manager: &'rm mut  ResourceManager,
    bg_manager: &'m mut BackgroundManager,
    map_manager: &'m mut MapManager,
    character_manager: &'m mut CharacterRenderer,
    state_manager: &'m mut StateManager,
    map_bindings: &'m mut HashMap<SelectID, DialogID>,
    main: &'m mut Option<DialogID>,


    dlgs: usize,
    dlg_map: HashMap<&'a str, DialogID>,

    bg_map: HashMap<&'a str, BackgroundID>,
    map_map: HashMap<&'a str, MapID>,
    item_map: HashMap<String, ItemID>,
    prop_map: HashMap<String, PropID>,
    character_map: HashMap<&'a str, CharacterID>,
    character_state_map: HashMap<CharacterID, (usize, HashMap<&'a str, StateID>)>,

    text_var: HashMap<&'a str, TextVariableID>,
    numeric_var: HashMap<&'a str, NumericVariableID>,
}

impl<'a, 'rm, 'm> TempState<'a, 'rm, 'm> {
    pub fn new(
        resource_manager: &'rm mut  ResourceManager,
        bg_manager: &'m mut BackgroundManager,
        map_manager: &'m mut MapManager,
        character_manager: &'m mut CharacterRenderer,
        state_manager: &'m mut StateManager,
        map_bindings: &'m mut HashMap<SelectID, DialogID>,
        main: &'m mut Option<DialogID>
    ) -> Self {
        Self {
            resource_manager,
            bg_manager,
            map_manager,
            character_manager,
            state_manager,
            map_bindings,
            main,

            dlgs: 0,
            dlg_map: HashMap::new(),

            bg_map: HashMap::new(),
            map_map: HashMap::new(),
            item_map: HashMap::new(),
            prop_map: HashMap::new(),
            character_map: HashMap::new(),
            character_state_map: HashMap::new(),

            text_var: HashMap::new(),
            numeric_var: HashMap::new(),
        }
    }

    fn get_bg_id(&self, bg: &'a str) -> Result<BackgroundID, CompileError> {
        self.bg_map.get(bg).ok_or(
            CompileError::BackgroundNotFound(format!("{:?} is not a declared background", bg))
        ).map(|v| *v)
    }

    fn get_prop_id(&self, bg: &'a str) -> Result<PropID, CompileError> {
        self.prop_map.get(bg).ok_or(
            CompileError::BackgroundNotFound(format!("{:?} is not a declared background", bg))
        ).map(|x| *x)
    }


    fn get_map_id(&self, bg: &'a str) -> Result<MapID, CompileError> {
        self.map_map.get(bg).ok_or(
            CompileError::MapNotFound(format!("{:?} is not a declared map", bg))
        ).map(|v| *v)
    }

    fn get_dlg_id(&self, bg: &'a str) -> Result<DialogID, CompileError> {
        self.dlg_map.get(bg).ok_or(
            CompileError::DialogNotFound(format!("{:?} is not a declared dialog", bg))
        ).map(|v| *v)
    }

    fn get_item_id(&self, bg: &'a str) -> Result<ItemID, CompileError> {
        self.item_map.get(bg).ok_or(
            CompileError::ItemNotFound(format!("{:?} is not a declared item", bg))
        ).map(|v| *v)
    }

    fn get_character_id(&self, bg: &'a str) -> Result<CharacterID, CompileError> {
        self.character_map.get(bg).ok_or(
            CompileError::CharacterNotFound(format!("{:?} is not a declared character", bg))
        ).map(|v| *v)
    }

    fn get_state_id(&self, chr: CharacterID, bg: &'a str) -> Result<StateID, CompileError> {
        self.character_state_map[&chr].1.get(bg).ok_or(
            CompileError::CharacterNotFound(format!("{:?} is not a declared state", bg))
        ).map(|v| *v)
    }

    fn get_text_id(&self, bg: &'a str) -> Result<TextVariableID, CompileError> {
        self.text_var.get(bg).ok_or(
            CompileError::VariableNotFound(format!("{:?} is not a declared text variable", bg))
        ).map(|v| *v)
    }

    fn get_numeric_id(&self, bg: &'a str) -> Result<NumericVariableID, CompileError> {
        self.numeric_var.get(bg).ok_or(
            CompileError::VariableNotFound(format!("{:?} is not a declared numeric variable", bg))
        ).map(|v| *v)
    }


    fn add_item_binding(&mut self, item: ItemID, dlg: DialogID) {
        self.map_bindings.insert(SelectID::Item(item), dlg);
    }

    fn add_prop_binding(&mut self, prop: PropID, dlg: DialogID) {
        self.map_bindings.insert(SelectID::Prop(prop), dlg);
    }


    fn add_bg_id(&mut self, name: &'a str, file: &'a str) -> Result<BackgroundID, CompileError> {
        match self.get_bg_id(name) {
            Ok(vl) => Err(CompileError::DuplicateDeclaration(format!("{:?} is declared twice", name))),
            Err(nf) => {
                let tex = self.resource_manager.load_texture(file).map_err(|err| CompileError::ResourceError(err))?;
                let id = self.bg_manager.add_background(tex);
                self.bg_map.insert(name, id);
                Ok(id)
            }
        }
    }

    fn add_map_id(&mut self, name: &'a str, file: &'a str) -> Result<MapID, CompileError> {
        match self.get_map_id(name) {
            Ok(vl) => Err(CompileError::DuplicateDeclaration(format!("{:?} is declared twice", name))),
            Err(nf) => {

                let mut start_time = SystemTime::now();

                let (b_t, m_t, prps, its, prnms) = self.resource_manager.load_map(file)
                    .map_err(|err| CompileError::ResourceError(err))?;

                println!("loaded map - {:?}", {
                    let new_time = SystemTime::now();
                    let duration = new_time.duration_since(start_time);
                    start_time = new_time;
                    duration
                });



                for (ind, props) in prnms.into_iter().enumerate() {
                    self.prop_map.insert(props, PropID(ind));
                }

                println!("stored prop names map  - {:?}",  {
                    let new_time = SystemTime::now();
                    let duration = new_time.duration_since(start_time);
                    start_time = new_time;
                    duration
                });


                let (mpid, its) = self.map_manager.add_map(
                    b_t, m_t, prps, its
                );

                println!("stored map - {:?}",  {
                    let new_time = SystemTime::now();
                    let duration = new_time.duration_since(start_time);
                    start_time = new_time;
                    duration
                });


                for (txt, id) in its {
                    self.item_map.insert(txt, id);
                }
                println!("added items - {:?}",  {
                    let new_time = SystemTime::now();
                    let duration = new_time.duration_since(start_time);
                    start_time = new_time;
                    duration
                });




                self.map_map.insert(name, mpid);
                Ok(mpid)
            }
        }
    }

    fn add_dlg_id(&mut self, bg: &'a str) -> Result<DialogID, CompileError> {
        match self.get_dlg_id(bg) {
            Ok(vl) => Err(CompileError::DuplicateDeclaration(format!("{:?} is declared twice", bg))),
            Err(nf) => {
                let id = DialogID(self.dlgs);

                self.dlgs += 1;
                self.dlg_map.insert(bg, id);

                if bg == "main" {
                    *self.main = Some(id);
                }

                Ok(id)
            }
        }
    }


    fn add_character_id(&mut self, name: &'a str, file: &'a str) -> Result<CharacterID, CompileError> {
        match self.get_character_id(name) {
            Ok(vl) => Err(CompileError::DuplicateDeclaration(format!("{:?} is declared twice", name))),
            Err(nf) => {
                println!("loading {:?}", file);
                let character = self.resource_manager.load_character(file)
                    .map_err(|err| CompileError::ResourceError(err))?;

                let id = self.character_manager.add_character(character);
                self.character_map.insert(name, id);

                self.character_state_map.insert(id, (0, HashMap::new()));

                Ok(id)
            }
        }
    }

    fn add_state_id(&mut self, character: CharacterID, bg: &'a str) -> Result<StateID, CompileError> {
        match self.get_state_id(character, bg) {
            Ok(vl) => Err(CompileError::DuplicateDeclaration(format!("{:?} is declared twice", bg))),
            Err(nf) => {

                let id = StateID(self.character_state_map[&character].0);
                self.character_state_map.get_mut(&character).unwrap().0 += 1;

                (self.character_state_map.get_mut(&character).unwrap().1).insert(bg, id);

                Ok(id)
            }
        }
    }

    fn add_text_id(&mut self, bg: &'a str, text: String) -> Result<TextVariableID, CompileError> {
        match self.get_text_id(bg) {
            Ok(vl) => Err(CompileError::DuplicateDeclaration(format!("{:?} is declared twice", bg))),
            Err(nf) => {
                let id = self.state_manager.add_text_variable(text);

                self.text_var.insert(bg, id);

                Ok(id)
            }
        }
    }

    fn add_numeric_id(&mut self, bg: &'a str, value: i32) -> Result<NumericVariableID, CompileError> {
        match self.get_numeric_id(bg) {
            Ok(vl) => Err(CompileError::DuplicateDeclaration(format!("{:?} is declared twice", bg))),
            Err(nf) => {
                let id = self.state_manager.add_numeric_variable(value);

                self.numeric_var.insert(bg, id);

                Ok(id)
            }
        }
    }

}

#[derive(Debug)]
pub enum CompileError {
    ResourceError(ResourceError),
    VariableNotFound(String),
    BackgroundNotFound(String),
    MapNotFound(String),
    CharacterNotFound(String),
    DialogNotFound(String),
    StateNotFound(String),
    ItemNotFound(String),
    DuplicateDeclaration(String),
}


pub fn compile_program<'a, 'rm, 'm>(program: ASTProgram<'a>, temp_state: &mut TempState<'a,'rm,'m>) -> Result<Vec<Vec<DialogOperation>>, CompileError> {
    let mut start_time  = SystemTime::now();

    let mut dialogs = Vec::new();

    for dialog in program.dialogs.iter() {
        temp_state.add_dlg_id(dialog.name)?;
    }
    compile_declarations(program.declarations, temp_state)?;

    println!("compiled declarations - {:?}", {
        let new_time = SystemTime::now();
        let duration = new_time.duration_since(start_time);
        start_time = new_time;
        duration
    });



    for dialog in program.dialogs {
        let result = compile_body(dialog.operations.0, &*temp_state)?;
        dialogs.push(result);
    }

    println!("compiled code - {:?}", {
        let new_time = SystemTime::now();
        let duration = new_time.duration_since(start_time);
        start_time = new_time;
        duration
    });



    Ok(dialogs)
}

pub fn compile_declarations<'a,'rm,'m>(decls: Vec<ASTDeclaration<'a>>, state: &mut TempState<'a,'rm,'m>) -> Result<(), CompileError> {
    let mut start_time  = SystemTime::now();
    for (ind,decl) in decls.into_iter().enumerate() {
        let decl_txt = format!("{:?}", decl);

        match decl {
            ASTDeclaration::BackgroundDeclaration(bg_decl) => {
                state.add_bg_id(bg_decl.name, bg_decl.file)?;
            },
            ASTDeclaration::CharacterDeclaration(char_decl) => {
                let id = state.add_character_id(char_decl.name, char_decl.file)?;
                for chr_state in char_decl.character_states {
                    state.add_state_id(id, chr_state)?;
                }
            },
            ASTDeclaration::MapDeclaration(map_decl) => {
                let mut start_time  = SystemTime::now();

                state.add_map_id(map_decl.0, map_decl.1)?;

                println!("added map - {:?}",  {
                    let new_time = SystemTime::now();
                    let duration = new_time.duration_since(start_time);
                    start_time = new_time;
                    duration
                });



                for mapping in map_decl.2 {
                    let mapping_txt = format!("{:?}", mapping);

                    match mapping {
                        ASTMapMapping::Item(name, dlg) => {
                            let item_id = state.get_item_id(name)?;
                            let dlg_id = state.get_dlg_id(dlg)?;

                            state.add_item_binding(item_id, dlg_id);
                        }
                        ASTMapMapping::Prop(name, dlg) => {
                            let prop_id = state.get_prop_id(name)?;
                            let dlg_id = state.get_dlg_id(dlg)?;

                            state.add_prop_binding(prop_id, dlg_id);
                        }
                    }

                    println!("compiled mapping {:?} - {:?}", mapping_txt, {
                        let new_time = SystemTime::now();
                        let duration = new_time.duration_since(start_time);
                        start_time = new_time;
                        duration
                    });



                }
            },
            ASTDeclaration::VariableDeclaration(var_decl) => {
                match var_decl {
                    ASTVariableDeclaration::Int(name, expr) => {
                        let expr = compile_expr(&expr, state)?;
                        let value = state.state_manager.eval_expr(&expr);

                        state.add_numeric_id(name, value)?;
                    },
                    ASTVariableDeclaration::Text(name, txt) => {
                        state.add_text_id(name, txt.into())?;
                    }
                }
            },
        }

        println!("compiled declaration {:?} - {:?}", decl_txt, {
            let new_time = SystemTime::now();
            let duration = new_time.duration_since(start_time);
            start_time = new_time;
            duration
        });




    }
    Ok(())
}

pub fn update_pos(ins: &mut Vec<DialogOperation>, pos: usize) -> &mut Vec<DialogOperation>{
    for op in ins.iter_mut() {
        match op {
            DialogOperation::Jump(ins) => {ins.0 += pos;},
            DialogOperation::JumpBool(cond, ins) => {ins.0 += pos;},
            DialogOperation::JumpCond(vals) => {
                for val in vals.iter_mut() {
                    (val.1).0 += pos;
                }
            },
            _ => ()
        }
    }

    ins
}


pub fn compile_body<'a,'rm,'m>(operations: Vec<ASTDialogOperations<'a>>, state: &TempState<'a,'rm,'m>) -> Result<Vec<DialogOperation>, CompileError> {

    let mut instructions = Vec::new();
    let mut pos = 0;
    for operation in operations {
        match operation {
            ASTDialogOperations::Goto(map_str) => {
                instructions.push(
                    DialogOperation::Goto(
                        state.get_map_id(map_str)?
                    )
                );
                pos += 1;
            }
            ASTDialogOperations::Show(bg_str) => {
                instructions.push(
                    DialogOperation::Show(
                        state.get_bg_id(bg_str)?
                    )
                );
                pos += 1;
            }
            ASTDialogOperations::Run(dlg_str) => {
                instructions.push(
                    DialogOperation::Run(
                        state.get_dlg_id(dlg_str)?
                    )
                );
                pos += 1;
            }
            ASTDialogOperations::Drop(item_str) => {
                instructions.push(
                    DialogOperation::Drop(
                        state.get_item_id(item_str)?
                    )
                );
                pos += 1;
            }
            ASTDialogOperations::State(chr_str, st_str) => {
                let id = state.get_character_id(chr_str)?;
                let std = state.get_state_id(id, st_str)?;

                instructions.push(
                    DialogOperation::State(
                        id, std
                    )
                );
                pos += 1;
            }
            ASTDialogOperations::Position(chr_str, o_pos) => {
                let id = state.get_character_id(chr_str)?;

                instructions.push(
                    DialogOperation::Position(
                        id, o_pos
                    )
                );
                pos += 1;
            }
            ASTDialogOperations::Say(say_op) => {
                let chr = match say_op.name {
                    Some(vlu) => Some(state.get_character_id(vlu)?),
                    None => None
                };
                match say_op.text {
                    ASTSpeakable::Text(text_str) => {
                        instructions.push(
                            DialogOperation::SayText(chr, text_str.into())
                        );
                    }
                    ASTSpeakable::Var(var_str) => {
                        instructions.push(
                            DialogOperation::SayVar(chr, state.get_text_id(var_str)?)
                        );
                    }
                }
                pos += 1;
            }
            ASTDialogOperations::Assign(assgn_op) => {

                match assgn_op {
                    ASTAssignmentOperation::Expr(num_str, expr) => {
                        instructions.push(
                            DialogOperation::SetNumVar(
                                state.get_numeric_id(num_str)?,
                                compile_expr(&expr, state)?
                            )
                        );
                    }
                    ASTAssignmentOperation::Str(text_str, speech_str) => {
                        instructions.push(
                            DialogOperation::SetStrVar(state.get_text_id(text_str)?, speech_str.into())
                        );
                    }
                    ASTAssignmentOperation::Format(text_str, fmt_expr) => {
                        let mut vars = Vec::new();
                        for num_str in fmt_expr.vars {
                            vars.push(
                                state.get_numeric_id(num_str)?
                            );
                        }
                        instructions.push(
                            DialogOperation::SetFmt(
                                state.get_text_id(text_str)?,
                                fmt_expr.text.into(),
                                vars
                            )
                        );
                    }
                }

                pos += 1;
            }
            ASTDialogOperations::Remove => {
                instructions.push(
                    DialogOperation::Remove
                );
                pos += 1;
            }
            ASTDialogOperations::If(ASTIfOp(bool_expr, dialog_body, else_body)) => {
                let bool_expr = compile_bool_expr(&bool_expr, state)?;
                let mut if_body = compile_body(dialog_body.0, state)?;

                match else_body {
                    Some(body) => {
                        let mut else_body = compile_body(body.0, state)?;
                        instructions.push(
                            DialogOperation::JumpBool(bool_expr, InstructionID(pos + else_body.len() + 2))
                        );
                        pos += 1;
                        update_pos(&mut else_body, pos);
                        let else_len = else_body.len();
                        instructions.extend(else_body);
                        pos += else_len;
                        instructions.push(
                            DialogOperation::Jump(
                                InstructionID(
                                    pos + 1 + if_body.len()
                                )
                            )
                        );
                        pos += 1;
                        update_pos(&mut if_body, pos);
                        let if_len = if_body.len();
                        instructions.extend(if_body);
                        pos += if_len;
                    },
                    None => {
                        instructions.push(
                            DialogOperation::JumpBool(
                                bool_expr,
                                InstructionID(
                                    pos + 2
                                )
                            )
                        );
                        pos += 1;
                        instructions.push(
                            DialogOperation::Jump(
                                InstructionID(
                                    pos + 1 + if_body.len()
                                )
                            )
                        );
                        pos += 1;
                        update_pos(&mut if_body, pos);
                        let if_len = if_body.len();
                        instructions.extend(if_body);
                        pos += if_len;
                    }
                }
            }
            ASTDialogOperations::While(ASTWhileOp(bool_expr, dialog_body)) => {
                let bool_expr = compile_bool_expr(&bool_expr, state)?;
                let mut while_body = compile_body(dialog_body.0, state)?;

                let start_pos = pos;
                instructions.push(
                    DialogOperation::JumpBool(
                        bool_expr,
                        InstructionID(
                            pos + 1 /* skip this ins */ + 1
                        )
                    )
                );
                pos += 1;
                let end_pos = pos + 1 + while_body.len() + 1;
                instructions.push(
                    DialogOperation::Jump(
                        InstructionID(
                            pos + 1 + while_body.len() + 1
                        )
                    )
                );
                pos += 1;
                update_pos(&mut while_body, pos);
                let while_len = while_body.len();
                instructions.extend(while_body);
                pos += while_len;
                instructions.push(
                    DialogOperation::Jump(
                        InstructionID(
                            start_pos
                        )
                    )
                );
                pos += 1;
                println!("{:?} == {:?}", pos, end_pos);
                assert!(pos == end_pos);
            }
            ASTDialogOperations::Input(ASTInputOperation(ops)) => {

                let mut compiled = Vec::new();
                let mut result = Vec::new();
                let mut total_len = 0;
                let mut temp_pos = pos + 1;

                for (speakable, dialog_body) in ops {
                    let bod = compile_body(dialog_body.0, state)?;
                    let mtch = match speakable {
                        ASTSpeakable::Text(text) => MatchText::Text(text.into()),
                        ASTSpeakable::Var(var_str) => MatchText::Variable(state.get_text_id(var_str)?)
                    };
                    total_len += bod.len() + 1;

                    result.push((mtch.clone(), InstructionID(temp_pos)));
                    temp_pos += bod.len() + 1;

                    compiled.push((bod, mtch));
                }

                instructions.push(
                    DialogOperation::JumpCond(result)
                );
                pos += 1;

                for (mut bod, mtch) in compiled {
                    update_pos(&mut bod, pos);
                    let bod_len = bod.len();
                    instructions.extend(bod);
                    pos += bod_len;

                    instructions.push(
                        DialogOperation::Jump(InstructionID(temp_pos))
                    );
                    pos += 1;
                }
            }

        }
    }

    Ok(instructions)
}



pub fn compile_expr<'a,'rm,'m>(expr: &ASTExpression<'a>, state: &TempState<'a,'rm,'m>) -> Result<NumericExpression, CompileError> {
    match expr {
        ASTExpression::BaseNumeric(val) => Ok(NumericExpression::Constant(*val)),
        ASTExpression::BaseVariable(var) => Ok(NumericExpression::Variable(state.get_numeric_id(var)?)),
        ASTExpression::Add(a,b) => {
            let a = Box::new(compile_expr(a.borrow(), state)?);
            let b = Box::new(compile_expr(b.borrow(), state)?);
            Ok(NumericExpression::Add(a,b))
        }
        ASTExpression::Sub(a,b) => {
            let a = Box::new(compile_expr(a.borrow(), state)?);
            let b = Box::new(compile_expr(b.borrow(), state)?);
            Ok(NumericExpression::Sub(a,b))
        }
        ASTExpression::Mul(a,b) => {
            let a = Box::new(compile_expr(a.borrow(), state)?);
            let b = Box::new(compile_expr(b.borrow(), state)?);
            Ok(NumericExpression::Mul(a,b))
        }
        ASTExpression::Div(a,b) => {
            let a = Box::new(compile_expr(a.borrow(), state)?);
            let b = Box::new(compile_expr(b.borrow(), state)?);
            Ok(NumericExpression::Div(a,b))
        }
    }
}

pub fn compile_bool_expr<'a,'rm,'m>(expr: &ASTBooleanExpression<'a>, state: &TempState<'a,'rm,'m>) -> Result<BooleanExpression, CompileError> {
    match expr {
        ASTBooleanExpression::Check(item_str) => Ok(BooleanExpression::Check(state.get_item_id(item_str)?)),
        ASTBooleanExpression::Gt(a,b) => Ok(
            BooleanExpression::Gt(
                compile_expr(a,state)?,
                compile_expr(b,state)?,
            )
        ),
        ASTBooleanExpression::Ge(a,b) => Ok(
            BooleanExpression::Ge(
                compile_expr(a,state)?,
                compile_expr(b,state)?,
            )
        ),
        ASTBooleanExpression::Lt(a,b) => Ok(
            BooleanExpression::Lt(
                compile_expr(a,state)?,
                compile_expr(b,state)?,
            )
        ),
        ASTBooleanExpression::Le(a,b) => Ok(
            BooleanExpression::Le(
                compile_expr(a,state)?,
                compile_expr(b,state)?,
            )
        ),
        ASTBooleanExpression::Eq(a,b) => Ok(
            BooleanExpression::Eq(
                compile_expr(a,state)?,
                compile_expr(b,state)?,
            )
        ),
        ASTBooleanExpression::And(a,b) => {
            let a = Box::new(compile_bool_expr(a.borrow(), state)?);
            let b = Box::new(compile_bool_expr(b.borrow(), state)?);
            Ok(BooleanExpression::And(a,b))
        }
        ASTBooleanExpression::Or(a,b) => {
            let a = Box::new(compile_bool_expr(a.borrow(), state)?);
            let b = Box::new(compile_bool_expr(b.borrow(), state)?);
            Ok(BooleanExpression::Or(a,b))
        }
    }
}


#[derive(Debug, Clone)]
pub enum MatchText {
    Text(String), Variable(TextVariableID)
}

#[derive(Debug)]
pub enum NumericExpression {
    Variable(NumericVariableID),
    Constant(i32),
    Add(Box<NumericExpression>, Box<NumericExpression>),
    Sub(Box<NumericExpression>, Box<NumericExpression>),
    Div(Box<NumericExpression>, Box<NumericExpression>),
    Mul(Box<NumericExpression>, Box<NumericExpression>)
}

#[derive(Debug)]
pub enum BooleanExpression {
    Gt(NumericExpression, NumericExpression),
    Ge(NumericExpression, NumericExpression),
    Lt(NumericExpression, NumericExpression),
    Le(NumericExpression, NumericExpression),
    Eq(NumericExpression, NumericExpression),
    Check(ItemID),
    And(Box<BooleanExpression>,Box<BooleanExpression>),
    Or(Box<BooleanExpression>,Box<BooleanExpression>)
}


#[derive(Debug)]
pub enum DialogOperation {
    Show(BackgroundID),
    Goto(MapID),
    Run(DialogID),
    Drop(ItemID),
    State(CharacterID, StateID),
    Position(CharacterID, Option<CharacterPosition>),
    SayText(Option<CharacterID>, String),
    SayVar(Option<CharacterID>, TextVariableID),
    SetNumVar(NumericVariableID, NumericExpression),
    SetStrVar(TextVariableID, String),
    SetFmt(TextVariableID, String, Vec<NumericVariableID>),
    JumpCond(Vec<(MatchText, InstructionID)>),
    JumpBool(BooleanExpression, InstructionID),
    Jump(InstructionID),
    Remove
}

pub fn print_instructions(instructions: &Vec<DialogOperation>) {
    for ind in 0..instructions.len() {
        match &instructions[ind] {
            DialogOperation::Show(bg_id) => println!("[{:?}] show {:?}", ind, bg_id),
            DialogOperation::Goto(map_id) => println!("[{:?}] goto {:?}", ind, map_id),
            DialogOperation::Run(dlg_id) => println!("[{:?}] run {:?}", ind, dlg_id),
            DialogOperation::Drop(item_id) => println!("[{:?}] drop {:?}", ind, item_id),
            DialogOperation::State(chr_id, st_id) => println!("[{:?}] state {:?} {:?}", ind, chr_id, st_id),
            DialogOperation::Position(chr_id, pos) => println!("[{:?}] position {:?} {:?}", ind, chr_id, pos),
            DialogOperation::SayText(chr, text) => println!("[{:?}] say {:?} text \"{:?}\"", ind, chr, text),
            DialogOperation::SayVar(chr, txt_id) => println!("[{:?}] say {:?} variable {:?}", ind, chr, txt_id),
            DialogOperation::SetNumVar(num_vr, num_expr) => println!("[{:?}] set {:?} <- {:?}", ind, num_vr, num_expr),
            DialogOperation::SetStrVar(txt_id, str_val) => println!("[{:?}] set {:?} <- \"{:?}\"", ind, txt_id, str_val),
            DialogOperation::SetFmt(txt_id, fmt_str, vals) => println!("[{:?}] set {:?} <- format(\"{:?}\", {:?})", ind, txt_id, fmt_str, vals),
            DialogOperation::JumpBool(expr, trgt) => println!("[{:?}] jump {:?} if {:?}", ind, trgt.0, expr),
            DialogOperation::Jump(pos) => println!("[{:?}] jump {:?}", ind, pos),
            DialogOperation::Remove => println!("[{:?}] remove;", ind),
            DialogOperation::JumpCond(conds) => {
                println!("[{:?}] jump table {{", ind);
                for (mtch, trgt) in conds {
                    println!("\t {:?} => {:?}", mtch, trgt.0);
                }
                println!("}}");
            },
        }
    }
}

