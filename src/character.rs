use crate::text::TextRenderer;
use crate::graphics::{TexturedQuad, Quad};
use crate::globals::RenderingGlobals;
use crate::statics::{APPLICATION_INITIAL_SIZE, CHARACTER_SIZE, CHARACTER_POSITIONS, CHARACTER_SPACING};
use crate::input::{FrameInputs, LogicalKey};
use crate::config::RenderingConfig;
use crate::resource_loader::{ResourceManager};
use std::hash::{Hash};

use std::vec::IntoIter;

use gl::types::{GLfloat, GLint, GLsizei, GLuint, GLsizeiptr};
use nalgebra::base::{Vector4, Vector3, Vector2};
use serde::{Serialize, Deserialize};

#[derive(Serialize,Deserialize,Hash,Copy,Clone,Debug)]
pub enum CharacterPosition {
    LEFT, RIGHT, CENTER
}

#[derive(Serialize, Deserialize,Copy,Clone,Debug,Eq,PartialEq,Hash)]
pub struct CharacterID(usize);

#[derive(Serialize,Deserialize,Copy,Clone,Debug,Eq,PartialEq)]
pub struct StateID(pub usize);

pub struct CharacterRenderer {
    characters: Vec<Character>,
    characters_on_screen: Vec<(CharacterID, CharacterPosition)>,
    draw_order: Vec<CharacterID>,
    active_color: Vector4<GLfloat>,
    inactive_color: Vector4<GLfloat>
}

pub struct Character {
    state: usize,
    width: usize,
    height: usize,
    textured_quad: TexturedQuad,
}




impl Character {


    pub fn new(mut texture: TexturedQuad, width: usize, height: usize) -> Self {
        assert!(width % CHARACTER_SIZE.0 as usize  == 0);
        assert!(height % CHARACTER_SIZE.1 as usize == 0);

        texture.size = Vector2::new(CHARACTER_SIZE.0 as f32, CHARACTER_SIZE.1 as f32);
        texture.tex_size = Vector2::new(CHARACTER_SIZE.0 as f32, CHARACTER_SIZE.1 as f32);
        texture.tex_position = Vector2::new(
            0.0, 0.0
        );

        Character {
            state: 0,
            textured_quad: texture,
            width, height
        }
    }

}


impl CharacterRenderer {

    pub fn save_state(&self) -> (Vec<(CharacterID, CharacterPosition)>, Vec<(CharacterID, StateID)>) {
        (
            self.characters_on_screen.clone(),
            self.draw_order.clone().into_iter().map(|chr| (chr, StateID(self.characters[chr.0].state))).collect()
        )
    }

    pub fn restore_state(&mut self, chars_on_screen: Vec<(CharacterID, CharacterPosition)>, character_states: Vec<(CharacterID, StateID)>) {
        self.characters_on_screen = chars_on_screen;
        for (chr, st) in character_states.iter() {
            self.characters[chr.0].state = st.0;
        }

        self.draw_order = character_states.into_iter().map(|x| x.0).collect();
    }



    pub fn new(config: &RenderingConfig) -> Self {
        Self {
            characters: Vec::new(),
            characters_on_screen: Vec::new(),
            draw_order: Vec::new(),
            active_color: Vector4::new(1.0, 1.0, 1.0, 1.0),
            inactive_color: config.inactive_character_color.clone()
        }
    }


    pub fn add_character(&mut self, character: Character) -> CharacterID {

        let id = CharacterID(self.characters.len());

        self.characters.push(character);

        id
    }

    pub fn get_character_state(&self, id: CharacterID) -> usize {
        let character = &self.characters[id.0];
        character.state
    }


    pub fn set_character_state(&mut self, id: CharacterID, state: usize) {
        let character = &mut self.characters[id.0];
        let character_cols = (character.width / CHARACTER_SIZE.0 as usize);

        let rows = (state / character_cols);
        let cols = (state % character_cols);

        character.state = state;
        character.textured_quad.tex_position = Vector2::new(
            ((cols * CHARACTER_SIZE.0) as f32),
            ((rows * CHARACTER_SIZE.1) as f32)
        );
    }

    pub fn get_character_position(&self, character: CharacterID) -> Option<CharacterPosition> {
        self.characters_on_screen.iter().find_map(|(id, pos)| if *id == character {Some(*pos)} else {None})
    }

    pub fn set_character_position(&mut self, character: CharacterID, position: CharacterPosition)  {

        self.characters_on_screen.retain(|(id, pos)| *id != character);
        self.characters_on_screen.push((character, position));

        self.draw_order.retain(|id| *id != character);
        self.draw_order.push(character);

    }

    pub fn set_character_offscreen(&mut self, character: CharacterID) {
        self.characters_on_screen.retain(|(id, pos)| !(*id == character));
        self.draw_order.retain(|id| *id != character);
    }

    pub fn set_character_active(&mut self, character: CharacterID) {
        assert!(self.characters_on_screen.iter().find(|(id, pos)| *id == character).is_some());

        self.draw_order.retain(|id| *id != character);
        self.draw_order.insert(0, character);
    }





    pub fn draw(&mut self, globals: &mut RenderingGlobals, resource_manager: &mut ResourceManager) {

        let mut left_offset = 0;
        let mut right_offset = 0;
        let mut center_offset = 0;
        // calculate positions of each sprite
        for (c_ind, pos) in self.characters_on_screen.iter() {
            let character = &mut self.characters[c_ind.0];

            match pos {
                CharacterPosition::LEFT => {
                    character.textured_quad.position =
                        Vector2::new(
                            (CHARACTER_POSITIONS.0 + left_offset * CHARACTER_SPACING) as f32,
                            -(APPLICATION_INITIAL_SIZE.1 as f32 / 2.0) + (CHARACTER_SIZE.1 as f32 / 2.0)
                        );
                    left_offset += 1;
                }
                CharacterPosition::RIGHT => {
                    character.textured_quad.position =
                        Vector2::new(
                            (CHARACTER_POSITIONS.2 - right_offset * CHARACTER_SPACING) as f32,
                            -(APPLICATION_INITIAL_SIZE.1 as f32 / 2.0) + (CHARACTER_SIZE.1 as f32 / 2.0)
                        );
                    right_offset += 1;
                }
                CharacterPosition::CENTER => {
                    character.textured_quad.position =
                        Vector2::new(
                            (CHARACTER_POSITIONS.1 + center_offset * CHARACTER_SPACING) as f32,
                            -(APPLICATION_INITIAL_SIZE.1 as f32 / 2.0) + (CHARACTER_SIZE.1 as f32 / 2.0)
                        );
                    center_offset += 1;
                }
            }

        }

        {
            let mut draw_iter = self.draw_order.iter();

            match draw_iter.next() {
                Some(c_id) => {
                    let character = &mut self.characters[c_id.0];
                    character.textured_quad.tex_color = Vector4::new(1.0, 1.0, 1.0, 1.0);
                },
                None => ()
            }

            for c_id in draw_iter {
                let character = &mut self.characters[c_id.0];
                character.textured_quad.tex_color = self.inactive_color.clone();
            }
        }

        for c_id in self.draw_order.iter().rev() {
            let character = &mut self.characters[c_id.0];
            character.textured_quad.draw(globals, resource_manager);
        }



    }


}
