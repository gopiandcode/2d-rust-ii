#version 330 core

out vec4 fragColor;
in vec3 colour;

void main() {
     fragColor = vec4(colour, 0.5);
}
