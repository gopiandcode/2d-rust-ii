#!/bin/bash

# move into the compiler directory
cd ./vn-game-compiler/

echo "BUILDING COMPILER"

# build the static library
stack build --ghc-options='-optl-static -optl-pthread -fPIC -fllvm'

echo "COMPILER BUILT SUCCESFULLY - COPYING FILES"

# find the compiler executable
COMPILER=$(find ./.stack-work/ | grep vn-game-compiler-exe | tail -n 1)

# move it to the program resources
cp $COMPILER ../res/tools/

