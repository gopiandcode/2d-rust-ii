{-# LANGUAGE GeneralizedNewtypeDeriving #-}
module CodeGenerator (
  compile_program, serialize_context, CompiledDialog,
 ItemID (..), PropID (..), MapID (..), DialogID (..), BackgroundID (..), CharacterID (..), StateID (..), TextVarID (..), NumVarID (..), Context (..), Instruction (..), TextExpr (..), NumExpr (..), BoolExpr (..)
                     ) where

import Parser

import Control.Monad.Trans
import Control.Monad.Trans.Maybe
import Control.Monad.Writer
import Control.Monad.State
import Data.Either
import Data.HashMap
import Data.HashSet
import Text.Printf
    
class Incrementable a where
  incr :: a -> a

instance Incrementable Int where
  incr x = x + 1

type CompiledDialog = [(DialogID, [Instruction])]

newtype ItemID = ItemID Int deriving (Show,Incrementable,Eq)
newtype PropID = PropID Int deriving (Show,Incrementable,Eq)
newtype MapID = MapID Int deriving (Show,Incrementable,Eq)
newtype DialogID = DialogID Int deriving (Show,Incrementable,Eq)
newtype BackgroundID = BackgroundID Int deriving (Show,Incrementable,Eq)
newtype CharacterID = CharacterID Int deriving (Show,Incrementable,Eq)
newtype StateID = StateID Int deriving (Show,Incrementable,Eq)
newtype TextVarID = TextVarID Int deriving (Show,Incrementable,Eq)
newtype NumVarID = NumVarID Int deriving (Show,Incrementable,Eq)


data Context = Context {
  variable_map :: Map String (Either (TextVarID, TextExpr) (NumVarID, NumExpr)),
  map_map :: Map String (MapID, String, [(ItemID, DialogID)], [(PropID, DialogID)]),
  item_map :: Map String ItemID,
  prop_map :: Map String PropID, 
  dialog_map :: Map String DialogID,
  background_map :: Map String (BackgroundID, String),
  character_map :: Map String (CharacterID, String, Map String StateID)
} deriving (Show)

serialize_context :: Context -> IO ()
serialize_context ctx = do
  putStrLn "Context:";
  let text_exprs = Prelude.map snd . Prelude.filter (isLeft . snd) . Data.HashMap.toList . variable_map $ ctx;
  let num_exprs = Prelude.map snd . Prelude.filter (isRight . snd) . Data.HashMap.toList . variable_map $ ctx;
  let maps = Prelude.map snd . Data.HashMap.toList . map_map $ ctx;
  let items = Data.HashMap.toList . item_map $ ctx;
  let props = Data.HashMap.toList . prop_map $ ctx;
  let main_dialog =  (dialog_map ctx) !  "main";
  let backgrounds = Prelude.map snd . Data.HashMap.toList . background_map $ ctx;
  let character_list =  (Data.HashMap.toList . character_map $ ctx)  :: [(String, (CharacterID, String, Map String StateID))];
  let characters = Prelude.map (\(w,(x,y,z)) -> (w,x,y, Data.HashMap.toList z))  character_list;
  putStrLn ("TextVar[" ++ (show . length  $ text_exprs) ++ "]:");
  mapM (putStrLn . show) text_exprs
  putStrLn ("NumVar[" ++ (show . length  $ num_exprs) ++ "]:");
  mapM (putStrLn . show) num_exprs
  putStrLn ("Maps[" ++ (show . length $ maps) ++ "]:");
  mapM (putStrLn . show) maps
  putStrLn ("Items[" ++ (show . length $ items) ++ "]:");
  mapM (putStrLn . show) items
  putStrLn ("Props[" ++ (show . length $ props) ++ "]:");
  mapM (putStrLn . show) props
  putStrLn ("Backgrounds[" ++ (show . length $ backgrounds) ++ "]:");
  mapM (putStrLn . show) backgrounds
  putStrLn ("Characters[" ++ (show . length $ characters) ++ "]:");
  mapM (putStrLn . show) characters;
  putStrLn ("MainDialog[" ++ show main_dialog ++ "]")
  return ()


data LoadState = LoadState {
  state_text_var :: TextVarID,
  state_num_var :: NumVarID,
  state_map :: MapID,
  state_item :: ItemID,
  state_prop :: PropID,
  state_dialog :: DialogID,
  state_bg :: BackgroundID,
  state_character :: CharacterID
                             }

empty_load_state :: LoadState
empty_load_state = LoadState
                    (TextVarID 0)
                    (NumVarID 0)                    
                    (MapID 0)
                    (ItemID 0)
                    (PropID 0)
                    (DialogID 0)
                    (BackgroundID 0)
                    (CharacterID 0)

loadstate_text_var :: LoadState -> (TextVarID, LoadState)
loadstate_text_var state = (value, state { state_text_var = (incr value) })
     where value = state_text_var state

loadstate_num_var :: LoadState -> (NumVarID, LoadState)
loadstate_num_var state = (value, state { state_num_var = (incr value) })
     where value = state_num_var state

loadstate_map :: LoadState -> (MapID, LoadState)
loadstate_map state = (value, state { state_map = (incr value) })
     where value = state_map state

loadstate_item :: LoadState -> (ItemID, LoadState)
loadstate_item state = (value, state { state_item = (incr value) })
     where value = state_item state

loadstate_prop :: LoadState -> (PropID, LoadState)
loadstate_prop state = (value, state { state_prop = (incr value) })
     where value = state_prop state

loadstate_dialog :: LoadState -> (DialogID, LoadState)
loadstate_dialog state = (value, state { state_dialog = (incr value) })
     where value = state_dialog state

loadstate_background :: LoadState -> (BackgroundID, LoadState)
loadstate_background state = (value, state { state_bg = (incr value) })
     where value = state_bg state

loadstate_character :: LoadState -> (CharacterID, LoadState)
loadstate_character state = (value, state { state_character = (incr value) })
     where value = state_character  state


empty_context :: Context
empty_context = Context
            Data.HashMap.empty
            Data.HashMap.empty
            Data.HashMap.empty
            Data.HashMap.empty
            Data.HashMap.empty
            Data.HashMap.empty
            Data.HashMap.empty            

type CodeGen = State Context

data Position = PosLeft | PosCenter | PosRight deriving (Show,Eq)

parse_pos_to_code_pos :: CharacterPosition -> Maybe Position
parse_pos_to_code_pos CharLeft = Just CodeGenerator.PosLeft
parse_pos_to_code_pos CharRight = Just CodeGenerator.PosRight
parse_pos_to_code_pos CharCenter = Just CodeGenerator.PosCenter
parse_pos_to_code_pos CharOff = Nothing

data Instruction = Show BackgroundID
   | Goto MapID
   | Run DialogID
   | Drop ItemID
   | State CharacterID StateID
   | Position CharacterID (Maybe Position)
   | Say (Maybe CharacterID) TextExpr
   | SetNum NumVarID NumExpr
   | SetStr TextVarID TextExpr
   | JumpCond [(TextExpr, Int)]
   | JumpBool BoolExpr Int
   | Jump Int
   | Remove
   deriving (Show)


data TextExpr = StringConst String
    | TextVar TextVarID
    | TextFormat String [Either TextExpr NumExpr]
    | TextAdd TextExpr TextExpr
    deriving (Show, Eq)

data NumExpr = IntConst Integer
  | NumVar NumVarID
  | NumNeg NumExpr
  | NumAdd NumExpr NumExpr
  | NumSub NumExpr NumExpr
  | NumMul NumExpr NumExpr
  | NumDiv NumExpr NumExpr
  deriving (Show, Eq)

data BoolExpr = BoolConst  Bool
  | BoolCheck ItemID
  | BoolNot BoolExpr
  | BoolAnd BoolExpr BoolExpr
  | BoolOr  BoolExpr BoolExpr
  | BoolGt  NumExpr NumExpr
  | BoolLt NumExpr NumExpr
  | BoolGte NumExpr NumExpr
  | BoolLte NumExpr NumExpr
  | BoolEq NumExpr NumExpr
  deriving (Show, Eq)


add_character :: String -> CharacterID -> String -> Map String StateID -> CodeGen ()
add_character name id path mp = do
  state <- get;
  let new_map = Data.HashMap.insert name (id, path, mp) $ character_map state;
  put (state { character_map = new_map })

add_text_var :: String -> TextVarID -> TextExpr -> CodeGen ()
add_text_var name variable expr = do
  state <- get;
  let new_map = Data.HashMap.insert name (Left (variable, expr)) $ variable_map state;
  put (state { variable_map = new_map })

add_num_var :: String -> NumVarID -> NumExpr -> CodeGen ()
add_num_var name variable expr = do
  state <- get;
  let new_map = Data.HashMap.insert name (Right (variable, expr)) $ variable_map state;
  put (state { variable_map = new_map })

add_map :: String -> MapID -> String -> [(ItemID, DialogID)] -> [(PropID, DialogID)] -> CodeGen ()
add_map name id path item_map prop_map = do
  state <- get;
  let new_map = Data.HashMap.insert name (id, path, item_map, prop_map) $ map_map state;
  put (state { map_map = new_map })

add_item :: String -> ItemID -> CodeGen ()
add_item name id = do
  state <- get;
  let new_map = Data.HashMap.insert name id $ item_map state;
  put (state { item_map = new_map })

add_prop :: String -> PropID -> CodeGen ()
add_prop name id = do
  state <- get;
  let new_map = Data.HashMap.insert name id $ prop_map state;
  put (state { prop_map = new_map })

add_background :: String -> BackgroundID -> String -> CodeGen ()
add_background name id path = do
  state <- get;
  let new_map = Data.HashMap.insert name (id, path) $ background_map state;
  put (state { background_map = new_map })

add_dialog :: String -> DialogID -> CodeGen ()
add_dialog name id = do
  state <- get;
  let new_map = Data.HashMap.insert name id $ dialog_map state;
  put (state { dialog_map = new_map })

expr_to_num :: Expr -> MaybeT CodeGen  NumExpr
expr_to_num expr = case expr of
  Parser.IntConst _ val -> return $ CodeGenerator.IntConst val
  Parser.Variable _ var -> MaybeT $ do
    state <- get;
    case Data.HashMap.lookup var $ variable_map state of
      Just (Right (num, _)) -> return . Just $ NumVar num
      _                    -> return Nothing
  Parser.Neg _ val -> do
    nval <- expr_to_num val;
    return $ NumNeg nval
  Parser.Add _ a b -> do
    ea <- expr_to_num a;
    eb <- expr_to_num b;
    return $ NumAdd ea eb
  Parser.Sub _ a b -> do
    ea <- expr_to_num a;
    eb <- expr_to_num b;
    return $ NumSub ea eb
  Parser.Mul _ a b -> do
    ea <- expr_to_num a;
    eb <- expr_to_num b;
    return $ NumMul ea eb
  Parser.Div _ a b -> do
    ea <- expr_to_num a;
    eb <- expr_to_num b;
    return $ NumDiv  ea eb
  _ -> MaybeT $ return Nothing

expr_to_text :: Expr -> MaybeT CodeGen TextExpr
expr_to_text expr = case expr of
  Parser.StringConst _ val -> return $ CodeGenerator.StringConst val
  Parser.Variable _ var -> MaybeT $ do
    state <- get;
    case Data.HashMap.lookup var $ variable_map state of
      Just (Left (text, _)) -> return . Just $ TextVar text
      _                    -> return Nothing
  Parser.Add _ a b -> do
    ea <- expr_to_text a;
    eb <- expr_to_text b;
    return $ TextAdd ea eb
  Parser.Format _ str vals -> do
    new_vals <- mapM expr_to_either vals;
    return $ TextFormat str new_vals
  _ -> MaybeT $ return Nothing

expr_to_either :: Expr ->  MaybeT CodeGen (Either TextExpr NumExpr)
expr_to_either expr = MaybeT $ do
  mnum <- runMaybeT (expr_to_num expr);
  case mnum of
    Just num -> return . Just . Right $ num
    _        -> (do
      mtext <- runMaybeT (expr_to_text expr);
      case mtext of
        Just text -> return . Just . Left $ text
        _         -> return Nothing
                )

expr_to_bool :: Expr -> MaybeT CodeGen BoolExpr
expr_to_bool expr = case expr of
  Parser.BoolConst _ v -> return $ CodeGenerator.BoolConst v
  Parser.Check _ i_str -> MaybeT $ do
    state <- get;
    case Data.HashMap.lookup i_str $ item_map state of
      Just (item) -> return . Just $ BoolCheck item
      _                    -> return Nothing
  Parser.Not _ be -> do
    bb <- expr_to_bool be;
    return $ BoolNot bb
  Parser.And _ ea eb -> do
    ba <- expr_to_bool ea;
    bb <- expr_to_bool eb;    
    return $ BoolAnd ba bb
  Parser.Or _ ea eb -> do
    ba <- expr_to_bool ea;
    bb <- expr_to_bool eb;    
    return $ BoolOr ba bb
  Parser.Gt _ ea eb -> do
    ba <- expr_to_num ea;
    bb <- expr_to_num eb;    
    return $ BoolGt ba bb
  Parser.Lt _ ea eb -> do
    ba <- expr_to_num ea;
    bb <- expr_to_num eb;    
    return $ BoolLt ba bb
  Parser.Gte _ ea eb -> do
    ba <- expr_to_num ea;
    bb <- expr_to_num eb;    
    return $ BoolGte ba bb
  Parser.Lte _ ea eb -> do
    ba <- expr_to_num ea;
    bb <- expr_to_num eb;    
    return $ BoolLte ba bb
  Parser.Eq _ ea eb -> do
    ba <- expr_to_num ea;
    bb <- expr_to_num eb;    
    return $ BoolEq ba bb
  _ -> MaybeT $ return Nothing

preprocess_dialogs :: LoadState -> [Dialog] -> CodeGen LoadState
preprocess_dialogs load_state ((Dialog _ name bod):rest) = do
  let (dialog_id, new_load_state) = loadstate_dialog load_state;
  add_dialog name dialog_id;
  preprocess_dialogs new_load_state rest
preprocess_dialogs load_state [] = return load_state

load_declaration :: LoadState -> Declaration -> CodeGen LoadState
load_declaration load_state (CharacterDecl i name path states) = do
  let (character, new_load_state) = loadstate_character load_state;
  let state_map = Data.HashMap.fromList (zip states [StateID i | i <- [0..]]);
  add_character name character path state_map;
  return new_load_state
load_declaration load_state (VariableDecl i ty name expr) =
  case ty of
    VarBool -> error "currently no support for boolean variables"
    VarText ->  do
      let (variable, new_load_state) = loadstate_text_var load_state;
      otvar <- runMaybeT $ expr_to_text expr;
      case otvar of
        Nothing   -> error "non-text initializer for text variable"
        Just tvar -> (do
                      add_text_var name variable tvar;
                      return new_load_state
                     )
    VarInt -> do
      let (variable, new_load_state) = loadstate_num_var load_state;
      onvar <- runMaybeT $ expr_to_num expr;
      case onvar of
        Nothing -> error "non-numeric initializer for numeric variable"
        Just nvar -> (do
                         add_num_var name variable nvar;
                         return new_load_state
                     )
load_declaration load_state (MapDecl i name path mappings) = do
  let (new_map, new_load_state) = loadstate_map load_state;
  (final_load_state, item_maps, prop_maps) <- load_map_mapping new_load_state mappings;
  add_map name new_map path item_maps prop_maps;
  return final_load_state
load_declaration load_state (BackgroundDecl i name path) = do
  let (new_bg, new_load_state) = loadstate_background load_state;
  add_background name new_bg path;
  return new_load_state
load_map_mapping :: LoadState -> [MapMapping] -> CodeGen (LoadState, [(ItemID, DialogID)], [(PropID, DialogID)])
load_map_mapping load_state (mapping:rest) =
  case mapping of
    Parser.Item i name dialog -> do
      let (item_id, new_load_state) = loadstate_item load_state;
      add_item name item_id;
      state <- get;
      let dialog_id = (dialog_map state) ! dialog
      (f1, f2, f3) <- load_map_mapping new_load_state rest;
      return $ (f1, (item_id,dialog_id):f2, f3)
    Parser.Prop i name dialog -> do
      let (prop_id, new_load_state) = loadstate_prop load_state;
      add_prop name prop_id;
      state <- get;
      let dialog_id = (dialog_map state) ! dialog
      (f1, f2, f3) <- load_map_mapping new_load_state rest;
      return $ (f1, f2, (prop_id, dialog_id):f3)
load_map_mapping load_state [] = return (load_state, [], [])

load_declarations :: [Dialog] -> [Declaration] -> CodeGen ()
load_declarations dialogs elems = do
  dialog_load_state <- preprocess_dialogs empty_load_state dialogs
  load_declarations_int dialog_load_state elems
                             where load_declarations_int load_state (x:xs) = do
                                      new_load_state <- load_declaration load_state x;
                                      load_declarations_int new_load_state xs
                                   load_declarations_int load_state [] = return ()

generate_cases :: Int -> [(Expr, Body)] -> CodeGen [Instruction]
generate_cases pos ((exp, bod):rest) = do
  obexp <- runMaybeT $ expr_to_bool exp;
  case obexp of
    Nothing -> error "invalid boolean expression for if statement"
    Just bexp -> do
      -- generate code for if body at position + 1
      ins <- generate_code (pos + 1) bod
      -- target of if instruction is initial pos + body size + 2 (for jump cond)
      let final_pos = pos + length ins + 2
      -- if condition not met, jump to end of loop
      let j1 = JumpBool (BoolNot bexp) final_pos;
      -- add placeholder for last case
      let j2 = CodeGenerator.Jump (-1);
      -- if code is jump condition + generated instructions + last case
      let if_ins = j1 : ins ++ [j2];
      -- generate the remaining instructions from the end of the if
      remain <- generate_cases final_pos rest;
      -- return the rest
      return $ if_ins ++ remain
generate_cases pos [] = return []


generate_text_cases :: Int -> [(Expr, Body)] -> CodeGen ([(TextExpr,Int)],[Instruction])
generate_text_cases pos ((exp, bod):rest) = do
  otexp <- runMaybeT $ expr_to_text exp;
  case otexp of
    Nothing -> error "invalid text expression for input statement"
    Just texp -> do
      -- generate code for if body at position
      ins <- generate_code pos bod
      -- target of if instruction is  pos + body size + 1 (for jump out)
      let final_pos = pos + length ins + 1
      -- add placeholder for jumping out
      let j2 = CodeGenerator.Jump (-1);
      -- case code is  generated instructions + jump out
      let case_ins = ins ++ [j2];
      -- generate the remaining instructions
      (remain_pos, remain) <- generate_text_cases final_pos rest;
      -- return the rest
      return $ ((texp,pos):remain_pos, case_ins ++ remain)
generate_text_cases pos [] = return ([],[])


generate_code :: Int -> [Statement] -> CodeGen [Instruction]
generate_code pos [] = return []
generate_code pos ((While i expr body):rest) = do
  obexp <- runMaybeT $ expr_to_bool expr;
  case obexp of
    Nothing -> error "invalid boolean expression for while loop"
    Just bexp -> do
        -- generate code for the body starting at the current position+1
        ins <- generate_code (pos + 1) body;
        -- final position is initial position + 1 + length of generated code + 1
        let final_pos = pos + length ins + 2;
        -- add jump condition to jump out of the while loop when cond not met
        let j1 = JumpBool (BoolNot bexp) final_pos;
        -- last jump cond to jump to start
        let j2 = Jump pos;
        let while_ins = (j1 : ins) ++ [j2];
        -- generate code for the rest of the code starting at the final pos
        remain <- generate_code final_pos rest;
        -- return the while block and the rest
        return $ while_ins ++ remain
generate_code pos ((If _ cases elsecase):rest) = do
  -- generate code for cases
  case_code <- generate_cases pos cases;
  -- find position of the end of cases
  let cases_end = pos + length case_code;
  -- generate code for else case
  else_code <- case elsecase of
    Just bod -> generate_code cases_end bod;
    Nothing  -> return [];
  let if_end = cases_end + length else_code;
  let updated_case_code = update_placeholder if_end case_code;
  remain <- generate_code if_end rest
  return $ case_code ++ else_code ++ remain
generate_code pos ((Parser.Goto _ map_str):rest) = do
  state <- get;
  let map = (\(a,_,_,_) -> a) $ (map_map state) ! map_str;
  let j1 = (CodeGenerator.Goto map);
  remain <- generate_code (pos + 1) rest;
  return $ j1:remain
generate_code pos ((Parser.Show _ bg_str):rest) = do
  state <- get;
  let bg = fst $ (background_map state) ! bg_str;
  let j1 = (CodeGenerator.Show bg);
  remain <- generate_code (pos + 1) rest;
  return $ j1:remain
generate_code pos ((Parser.Run _ dlg_str):rest) = do
  state <- get;
  let dlg = (dialog_map state) ! dlg_str;
  let j1 = (CodeGenerator.Run dlg);
  remain <- generate_code (pos + 1) rest;
  return $ j1:remain
generate_code pos ((Parser.Drop _ item_str):rest) = do
  state <- get;
  let item = (item_map state) ! item_str;
  let j1 = (CodeGenerator.Drop item);
  remain <- generate_code (pos + 1) rest;
  return $ j1:remain
generate_code pos ((Parser.State _ chr_str stt_str):rest) = do
  state <- get;
  let (chr,_,st_mp) = (character_map state) ! chr_str;
  let stt = st_mp ! stt_str;
  let j1 = (CodeGenerator.State chr stt);
  remain <- generate_code (pos + 1) rest;
  return $ j1:remain
generate_code pos ((Parser.Position _ chr_str chrpos):rest) = do
  state <- get;
  let (chr,_,_) = (character_map state) ! chr_str;
  let opos = parse_pos_to_code_pos chrpos;
  let j1 = (CodeGenerator.Position chr opos);
  remain <- generate_code (pos + 1) rest;
  return $ j1:remain
generate_code pos ((Parser.Say _ ochr oexpr):rest) = do
  ochr_id <- case ochr of
    Just chr_str -> do
      state <- get;
      let (chr, _,_) = (character_map state) ! chr_str;
      return . Just $ chr
    Nothing -> return $ Nothing;
  otxt <- runMaybeT $ expr_to_text oexpr;
  case otxt of
    Nothing -> error "non-text expression provided to say operation"
    Just txt -> do
       let j1 = CodeGenerator.Say ochr_id txt;
       remain <- generate_code (pos + 1) rest;
       return $ j1:remain
generate_code pos ((Input _ cases):rest) = do
  (jcases, code) <- generate_text_cases (pos+1) cases;
  -- final position is the current pos + length of all cases + 1 (for jump)
  let final_pos = pos + length code + 1
  let pos_code = update_placeholder final_pos code;
  remain <- generate_code final_pos rest;
  return $ ((JumpCond jcases):pos_code) ++ remain
generate_code pos ((Assign _ v exp):rest) = do
  state <- get;
  j1 <- case variable_map state ! v of
    Left (tvar,_) -> do
      otexp <- runMaybeT $ expr_to_text exp
      case otexp of
        Nothing -> error "non-textual expression in assignment"
        Just texp -> return $ SetStr tvar texp
    Right (nvar,_) -> do
      onexp <- runMaybeT $ expr_to_num exp;
      case onexp of
        Nothing -> error "non-numeric expression in assignment"
        Just nexp -> return $ SetNum nvar nexp
  remain <- generate_code (pos+1) rest;
  return $ j1:remain
generate_code pos ((Parser.Remove _):rest) = do
  let j1 = CodeGenerator.Remove;
  remain <- generate_code (pos+1) rest;
  return $ j1:remain
  
update_placeholder :: Int -> [Instruction] -> [Instruction]
update_placeholder pos ((Jump (-1)):rest) = (Jump pos):(update_placeholder pos rest)
update_placeholder pos (x:xs) = x : (update_placeholder pos xs)
update_placeholder pos [] = []

load_dialog :: [Dialog] -> CodeGen CompiledDialog
load_dialog ((Dialog _ name bod):t) = do
  state <- get;
  let id = dialog_map state ! name;
  ins <- generate_code 0 bod;
  rem <- load_dialog t;
  return $ (id,ins):rem
load_dialog [] = return $ []

compile_program_int :: Program -> CodeGen CompiledDialog
compile_program_int program = do
  load_declarations (program_dialogs program) (program_declarations program);
  load_dialog $ program_dialogs program

compile_program :: Program -> (CompiledDialog,Context)
compile_program  = \x -> (runState . compile_program_int) x empty_context
