use std::fs::File;
use std::io::Read;
use gl::types::GLfloat;
use nalgebra::base::Vector4;
use crate::resource_loader::ResourceManager;


pub struct RenderingConfig {
    pub font: Vec<u8>,
    pub inactive_character_color: Vector4<GLfloat>
}


impl RenderingConfig {
    pub fn new(resource_manager: &mut ResourceManager) -> Self {

        let buf = resource_manager.load_font("./res/fonts/mplus-2m-regular.ttf").expect(
                "Could not load text resource: ./res/fonts/mplus-2m-regular.ttf"
        );

        Self {
            font: buf,
            inactive_character_color: Vector4::new(0.6, 0.6, 0.6, 1.0)
        }
    }
}
