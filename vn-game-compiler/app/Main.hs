{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE TypeOperators     #-}

module Main where

import Lib
import System.IO
import System.Exit
import Control.Monad.Trans.Maybe
import Options.Generic
-- load_and_compile_program
-- serialize_compiled_program
-- run_compiled_program

data ProgramOptions = Compile {
                           simulate :: (Maybe Bool) <?> "perform a headless simulation of the compiled program",
                           fname :: String <?> "path to script file"
                      }
                    | TypeCheck { fname :: String <?> "path to script file" }
                    deriving (Generic, Show)
instance ParseRecord (ProgramOptions)

main :: IO ()
main = do
  options <- getRecord "vn-game-compiler"
  case options of
    Compile shouldSimulate filename -> do
             ocompiled <- runMaybeT $ load_and_compile_program . unHelpful $ filename;
             case ocompiled of
               Nothing ->   exitWith (ExitFailure (1))
               Just (code,ctx)  -> do
                 serialize_compiled_program code ctx;
                 case unHelpful shouldSimulate of
                   Just True -> run_compiled_program code ctx
                   _ -> return ()
                 exitWith (ExitSuccess)             
    TypeCheck filename -> do
             ocompiled <- runMaybeT $ load_and_compile_program . unHelpful $ filename;
             case ocompiled of
               Nothing ->   exitWith (ExitFailure (1))
               Just _  ->   exitWith (ExitSuccess)
