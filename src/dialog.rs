use crate::text::TextRenderer;
use crate::graphics::{TexturedQuad, Quad};
use crate::globals::RenderingGlobals;
use crate::statics::{APPLICATION_INITIAL_SIZE};
use crate::input::{FrameInputs, LogicalKey};
use crate::resource_loader::ResourceManager;

use std::vec::IntoIter;

use gl::types::{GLfloat, GLint, GLsizei, GLuint, GLsizeiptr};
use nalgebra::base::{Vector4, Vector3, Vector2};

pub struct DialogRendererState {
    dialog_box: DialogBox,

    text_position: usize,
    last_update: f32,

    tasks: (Dialog, usize),

    // temporary
    completed: bool,
}

pub struct DialogRenderer<'a, 'b> {
    text_renderer: &'a mut TextRenderer<'b>,
    state: &'a mut DialogRendererState
}

pub struct DialogSnippet {pub text: String, pub freq: f32}
pub struct Dialog(Vec<DialogSnippet>);

impl<T: Into<String>> From<Vec<T>> for Dialog {
    fn from(item: Vec<T>) -> Self {
        Self (
            item.into_iter().map(|item| DialogSnippet {text: item.into(), freq: 30.0}).collect()
        )
    }
}


impl DialogRendererState {
    pub fn new(dialog_box: DialogBox) -> Self {
        Self {
            dialog_box,
            text_position: 0,
            last_update: 0.0,
            completed: true,
            tasks: (Dialog(Vec::new()),0)
        }
    }


    pub fn renderer<'a,'b>(&'a mut self, text_renderer: &'a mut TextRenderer<'b>) -> DialogRenderer<'a,'b> {
        DialogRenderer {
            text_renderer,
            state: self
        }
    }
}


// text_renderer,, text_renderer: TextRenderer<'a>
impl<'a,'b> DialogRenderer<'a,'b> {


    pub fn set_text<T: Into<String>>(&mut self, text: T) {
        self.state.completed = false;
        self.state.tasks = (Dialog::from(vec![text]), 0);

        self.state.text_position = 0;

        if self.state.tasks.1 >= (self.state.tasks.0).0.len() {
            self.state.completed = true;
        }
    }

    pub fn update(&mut self, input: FrameInputs, delta_time: f32) {


        let input_pressed = {
            input.input_keys.iter().any(|key| match key { LogicalKey::ENTER => true, _ => false})
        };

        if self.state.tasks.1 >= (self.state.tasks.0).0.len() {
            self.state.completed = true;
        }

        if !self.state.completed  {
            self.state.last_update += delta_time;
            let DialogSnippet {
                text,
                freq
            } = &(self.state.tasks.0).0[self.state.tasks.1];

            if self.state.text_position < text.len() {
                if self.state.last_update > *freq {
                    self.state.last_update = 0.0;
                    self.state.text_position += 1;

                }
            } else if input_pressed {
                self.state.text_position = 0;
                self.state.tasks.1 += 1;

                if self.state.tasks.1 >= (self.state.tasks.0).0.len() {
                    self.state.completed = true;
                }
            }
        }
    }


    pub fn is_complete(&mut self) -> bool {
        self.state.completed
    }

    pub fn draw(&mut self, globals: &mut RenderingGlobals, resource_manager: &mut ResourceManager) {

        if !self.state.completed {

            let DialogSnippet {
                text,
                freq
            } = &(self.state.tasks.0).0[self.state.tasks.1];

            self.state.dialog_box.draw(globals, resource_manager);

            self.text_renderer.add_text(
                &text[..self.state.text_position], 0.3,
                &Vector2::new(
                    30.0,
                    APPLICATION_INITIAL_SIZE.1 as f32 *  3.0/4.0 + 30.0,
                ),
                &Vector2::new(
                    APPLICATION_INITIAL_SIZE.0 as f32 - 30.0 * 2.0,
                    APPLICATION_INITIAL_SIZE.1 as f32/2.0 - 30.0 * 2.0
                ),
                &self.state.dialog_box.text,
            );

            self.text_renderer.draw(globals, resource_manager);
        }
    }
}




pub struct DialogBox {
    corner: TexturedQuad,
    edge: TexturedQuad,
    base: Quad,
    text: Vector4<GLfloat>
}

impl DialogBox {

    pub fn new(
        mut corner: TexturedQuad,
        mut edge: TexturedQuad,
        mut base: Quad,
        mut text: Vector4<GLfloat>,
        resource_manager: &ResourceManager
    ) -> Self {

        base.position = Vector2::new(
            0.0,
            0.0 - APPLICATION_INITIAL_SIZE.1 as f32/2.0
        );
        base.size = Vector2::new(
            APPLICATION_INITIAL_SIZE.0 as f32,
            APPLICATION_INITIAL_SIZE.1 as f32/2.0
        );

        let (corner_texture_width, corner_texture_height) = {
            let texture = &resource_manager[corner.texture];
            (texture.width, texture.height)
        };

        corner.size = Vector2::new(30.0, 30.0);
        corner.tex_size = Vector2::new(
            corner_texture_width as f32,
            corner_texture_height as f32
        );


        let (edge_texture_width, edge_texture_height) = {
            let texture = &resource_manager[edge.texture];
            (texture.width, texture.height)
        };


        edge.tex_size = Vector2::new(
            edge_texture_width as f32,
            edge_texture_height as f32
        );


        Self {
            corner,
            edge,
            base,
            text
        }
    }

    pub fn draw(&mut self, globals: &mut RenderingGlobals, resource_manager: &mut ResourceManager) {
        self.base.draw(globals, resource_manager);

        self.corner.position = Vector2::new(
            0.0 - APPLICATION_INITIAL_SIZE.0 as f32/2.0 + 15.0,
            0.0 - APPLICATION_INITIAL_SIZE.1 as f32 /4.0 - 15.0
        );
        self.corner.rotation = 0.0;
        self.corner.draw(globals, resource_manager);


        self.corner.position = Vector2::new(
            0.0 - APPLICATION_INITIAL_SIZE.0 as f32/2.0 + 15.0,
            0.0 - APPLICATION_INITIAL_SIZE.1 as f32/2.0 + 15.0
        );
        self.corner.rotation = 270.0_f32.to_radians();
        self.corner.draw(globals, resource_manager);


        self.corner.position = Vector2::new(
            0.0 + APPLICATION_INITIAL_SIZE.0 as f32/2.0 - 15.0,
            0.0 - APPLICATION_INITIAL_SIZE.1 as f32/2.0 + 15.0
        );
        self.corner.rotation = 180.0_f32.to_radians();
        self.corner.draw(globals, resource_manager);


        self.corner.position = Vector2::new(
            0.0 + APPLICATION_INITIAL_SIZE.0 as f32/2.0 - 15.0,
            0.0 - APPLICATION_INITIAL_SIZE.1 as f32/4.0 - 15.0
        );
        self.corner.rotation = 90.0_f32.to_radians();
        self.corner.draw(globals, resource_manager);


        self.edge.size = Vector2::new(
            APPLICATION_INITIAL_SIZE.0 as f32 - 30.0 * 2.0,
            30.0
        );
        self.edge.position = Vector2::new(
            0.0,
            0.0 - APPLICATION_INITIAL_SIZE.1 as f32 /4.0 - 15.0
        );
        self.edge.rotation = 0.0;
        self.edge.draw(globals, resource_manager);



        self.edge.position = Vector2::new(
            0.0,
            0.0 - APPLICATION_INITIAL_SIZE.1 as f32/2.0 + 15.0
        );
        self.edge.rotation = 180.0_f32.to_radians();
        self.edge.draw(globals, resource_manager);


        self.edge.size = Vector2::new(
            APPLICATION_INITIAL_SIZE.1 as f32 * 4.0/16.0 - 30.0 * 2.0,
            30.0
        );
        self.edge.position = Vector2::new(
            0.0 + APPLICATION_INITIAL_SIZE.0 as f32/2.0 - 15.0,
            0.0 - APPLICATION_INITIAL_SIZE.1 as f32 * 3.0/8.0
        );
        self.edge.rotation = 90.0_f32.to_radians();
        self.edge.draw(globals, resource_manager);

        self.edge.position = Vector2::new(
            0.0 - APPLICATION_INITIAL_SIZE.0 as f32/2.0 + 15.0,
            0.0 - APPLICATION_INITIAL_SIZE.1 as f32 * 3.0/8.0
        );
        self.edge.rotation = 270.0_f32.to_radians();
        self.edge.draw(globals, resource_manager);

    }

}
