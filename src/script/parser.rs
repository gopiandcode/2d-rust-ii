use regex::Regex;
use std::str;
use std::iter::Iterator;
use std::collections::HashSet;

use crate::script::lexer::{ScriptLexer, LexError, ScriptToken};
use crate::character::CharacterPosition;

use std::collections::hash_map::DefaultHasher;
use std::hash::{Hash, Hasher};

#[derive(Debug,Hash)]
pub enum ParseError {
    BadToken(LexError),
    BadExpression(String),
}

pub struct ScriptParser<'a,'b> {
   pub (super) lexer: ScriptLexer<'a,'b>,
   pub (super) next: Option<ScriptToken<'a>>,
}

impl<'a,'b> ScriptParser<'a,'b> {
    pub fn from(lexer: ScriptLexer<'a,'b>) -> Self {
        Self {
            lexer,
            next: None,
        }
    }


    fn peek(&mut self) -> Option<&ScriptToken<'a>> {
        if self.next.is_none() {
            self.next = self.lexer.next();
        }

        self.next.as_ref()
    }


    fn consume(&mut self) -> Option<ScriptToken<'a>> {
        if self.next.is_none() {
            self.next = self.lexer.next();
        }

        self.next.take()
    }

}

#[derive(Debug,Hash)]
pub struct ASTProgram<'a> {
    pub declarations: Vec<ASTDeclaration<'a>>,
    pub dialogs: Vec<ASTDialog<'a>>
}

/*
DECL               := CHARACTER_DECL
                    | VARIABLE_DECL
                    | MAP_DECL
                    | BG_DECL
 */

#[derive(Debug,Hash)]
pub enum ASTDeclaration<'a> {
    CharacterDeclaration(ASTCharacterDeclaration<'a>),
    VariableDeclaration(ASTVariableDeclaration<'a>),
    MapDeclaration(ASTMapDeclaration<'a>),
    BackgroundDeclaration(ASTBackgroundDeclaration<'a>),
}

/*
MAP_DECL :+ map <NAME> : "<FILE>" {
MAP_MAPPING*
}

MAP_MAPPING := item <NAME> -> <NAME> ;
             | prop <NAME> -> <NAME> ;
 */

#[derive(Debug,Hash)]
pub struct ASTMapDeclaration<'a>(pub (super) &'a str, pub (super) &'a str, pub (super) Vec<ASTMapMapping<'a>>);


#[derive(Debug,Hash)]
pub enum ASTMapMapping<'a> {
    Item(&'a str, &'a str),
    Prop(&'a str, &'a str),
}


/*
VARIABLE_DECL      := variable int <NAME> := <VALUE>
| variable text <NAME> := "<VALUE>"
;
 */
#[derive(Debug,Hash)]
pub enum ASTVariableDeclaration<'a> {
    Int(&'a str, ASTExpression<'a>),
    Text(&'a str, &'a str),
}

/*
BG_DECL := bg name : "<FILE>" ;
 */
#[derive(Debug,Hash)]
pub struct ASTBackgroundDeclaration<'a> {
    pub name: &'a str,
    pub file: &'a str,
}

/*
CHARACTER_DECL     := character <NAME> : "<FILE>" { <STATE_NAME>* }
 */


#[derive(Debug,Hash)]
pub struct ASTCharacterDeclaration<'a> {
    pub name: &'a str,
    pub file: &'a str,
    pub character_states: Vec<&'a str>
}


/*
DIALOG             := dialog <NAME> {
DIALOG_BODY*
}
*/

#[derive(Debug,Hash)]
pub struct ASTDialog<'a> {
    pub name: &'a str,
    pub operations: ASTDialogBody<'a>
}


#[derive(Debug,Hash)]
pub struct ASTDialogBody<'a>(pub Vec<ASTDialogOperations<'a>>);


/*
WHILE_OP := while BOOL_EXPR {  DIALOG BODY   }
 */

#[derive(Debug,Hash)]
pub struct ASTWhileOp<'a>(pub (super) ASTBooleanExpression<'a>, pub (super) ASTDialogBody<'a>);

/*
IF_OP := if BOOL_EXPR { DIALOG_BODY* } (else { DIALOG_BODY* })?
 */
#[derive(Debug,Hash)]
pub struct ASTIfOp<'a>(pub (super) ASTBooleanExpression<'a>, pub (super) ASTDialogBody<'a>, pub (super) Option<ASTDialogBody<'a>>);

/*
GOTO_OP := goto <NAME> ;

SHOW_OP := show <NAME> ;

RUN_OP := run <NAME> ;

DROP_OP := drop <NAME> ;

MODIFY_STATE_OP    :=  state <NAME> <STATE_NAME>;

MODIFY_POSITION_OP := position <NAME> <LEFT|RIGHT|CENTER|OFFSCREEN>;

SAY_OP             := speak <NAME>  SPEAKABLE_VALUE
                    | info SPEAKABLE_VALUE
;

REMOVE_OP := remove ;
 */

#[derive(Debug,Hash)]
pub enum ASTDialogOperations<'a> {
    Goto(&'a str),
    Show(&'a str),
    Run(&'a str),
    Drop(&'a str),
    State(&'a str, &'a str),
    Position(&'a str, Option<CharacterPosition>),
    Say(ASTSayOperation<'a>),
    Assign(ASTAssignmentOperation<'a>),
    Input(ASTInputOperation<'a>),
    While(ASTWhileOp<'a>),
    If(ASTIfOp<'a>),
    Remove
}


/*
SAY_OP             := speak <NAME>  SPEAKABLE_VALUE
                    | info SPEAKABLE_VALUE
;
 */


#[derive(Debug,Hash)]
pub struct ASTSayOperation<'a> {
    pub name: Option<&'a str>,
    pub text: ASTSpeakable<'a>
}

#[derive(Debug,Hash)]
pub enum ASTSpeakable<'a> {
    Text(&'a str),
    Var(&'a str)
}


/*
INPUT_OP := input {
INPUT_MATCH => {
DIALOG_BODY
    } *
}

INPUT_MATCH := "<TEXT>" | variable
*/

#[derive(Debug,Hash)]
pub struct ASTInputOperation<'a>(pub (super) Vec<(ASTSpeakable<'a>, ASTDialogBody<'a>)>);

#[derive(Debug,Hash)]
pub enum ASTExpression<'a> {
    Add(Box<ASTExpression<'a>>, Box<ASTExpression<'a>>),
    Sub(Box<ASTExpression<'a>>, Box<ASTExpression<'a>>),
    Mul(Box<ASTExpression<'a>>, Box<ASTExpression<'a>>),
    Div(Box<ASTExpression<'a>>, Box<ASTExpression<'a>>),
    BaseNumeric(i32),
    BaseVariable(&'a str),
}

#[derive(Debug,Hash)]
pub enum ASTBooleanExpression<'a> {
    Gt(ASTExpression<'a>, ASTExpression<'a>),
    Ge(ASTExpression<'a>, ASTExpression<'a>),
    Lt(ASTExpression<'a>, ASTExpression<'a>),
    Le(ASTExpression<'a>, ASTExpression<'a>),
    Eq(ASTExpression<'a>, ASTExpression<'a>),
    Check(&'a str),
    And(Box<ASTBooleanExpression<'a>>, Box<ASTBooleanExpression<'a>>),
    Or(Box<ASTBooleanExpression<'a>>, Box<ASTBooleanExpression<'a>>),
}


/*
ASSIGNMENT_OP := variable <NAME> := EXPR | "<TEXT>" | format("<TEXT>",VARIABLE, ,);
 */

#[derive(Debug,Hash)]
pub enum ASTAssignmentOperation<'a> {
    Expr(&'a str, ASTExpression<'a>),
    Str(&'a str, &'a str),
    Format(&'a str, ASTFormatExpression<'a>)
}

#[derive(Debug,Hash)]
pub struct ASTFormatExpression<'a> {
    pub text: &'a str,
    pub vars: Vec<&'a str>
}





pub fn parse_program<'a,'b>(parser: &mut ScriptParser<'a,'b>) -> Result<(u64, u64, ASTProgram<'a>), ParseError> {
    let mut decls = Vec::new();
    let mut dialogs = Vec::new();

    while let Some(decl) = parse_decl(parser)? {
        decls.push(decl);
    }

    while let Some(dlg) = parse_dialog(parser)? {
        dialogs.push(dlg);
    }

    let decl_hash = {
        let mut s = DefaultHasher::new();
        dialogs.hash(&mut s);
        s.finish()
    };
    let code_hash = {
        let mut s = DefaultHasher::new();
        dialogs.hash(&mut s);
        s.finish()
    };


    Ok((
        decl_hash,
        code_hash,
        ASTProgram {
        declarations: decls,
        dialogs: dialogs
    }))
}



fn parse_decl<'a,'b>(parser: &mut ScriptParser<'a,'b>) -> Result<Option<ASTDeclaration<'a>>, ParseError> {
    if let Some(value) = parser.peek() {
        match value {
            ScriptToken::KEY_CHARACTER => {
                if let Some(character_decl) = parse_character_decl(parser)? {
                    Ok(Some(ASTDeclaration::CharacterDeclaration(character_decl)))
                } else {unreachable!()}
            },
            ScriptToken::KEY_VARIABLE => {
                if let Some(variable_decl) = parse_variable_decl(parser)? {
                    Ok(Some(ASTDeclaration::VariableDeclaration(variable_decl)))
                } else {unreachable!()}
            },
            ScriptToken::KEY_MAP => {
                if let Some(map_decl) = parse_map_decl(parser)? {
                    Ok(Some(ASTDeclaration::MapDeclaration(map_decl)))
                } else {unreachable!()}
            },
            ScriptToken::KEY_BG => {
                if let Some(bg_decl) = parse_background_decl(parser)? {
                    Ok(Some(ASTDeclaration::BackgroundDeclaration(bg_decl)))
                } else {unreachable!()}
            },
            _ => Ok(None)
        }
    } else {
        Ok(None)
    }
}


fn parse_map_decl<'a,'b>(parser: &mut ScriptParser<'a,'b>) -> Result<Option<ASTMapDeclaration<'a>>, ParseError> {
    if let Some(ScriptToken::KEY_MAP) = parser.peek() {
        parser.consume(); // consume map

        if let Some(ScriptToken::LIT_VAR(label)) = parser.consume() {
            if let Some(ScriptToken::SYM_COLON) = parser.consume() {
                if let Some(ScriptToken::LIT_STR(path)) = parser.consume() {
                    if let Some(ScriptToken::SYM_LBRACE) = parser.consume() {
                        let mut map_mappings = Vec::new();

                        while let Some(mapping) = parse_map_mapping(parser)? {
                            map_mappings.push(mapping);
                        }

                        if let Some(ScriptToken::SYM_RBRACE) = parser.consume() {
                            Ok(Some(ASTMapDeclaration(label, path, map_mappings)))
                        } else {
                            Err(ParseError::BadExpression(
                                format!("[{:?}:{:?}] Bad map declaration - missing terminating Rbrace", parser.lexer.line, parser.lexer.col)
                            ))
                        }
                    } else {
                        Err(ParseError::BadExpression(
                            format!("[{:?}:{:?}] Bad map declaration - missing starting Lbrace", parser.lexer.line, parser.lexer.col)
                        ))
                    }
                } else {
                    Err(ParseError::BadExpression(
                        format!("[{:?}:{:?}] Bad map declaration - missing path", parser.lexer.line, parser.lexer.col)
                    ))
                }
            } else {
                Err(ParseError::BadExpression(
                    format!("[{:?}:{:?}] Bad map declaration - missing colon symbol", parser.lexer.line, parser.lexer.col)
                ))
            }
        } else {
            Err(ParseError::BadExpression(
                format!("[{:?}:{:?}] Bad map declaration - missing variable label", parser.lexer.line, parser.lexer.col)
            ))
        }
    } else {
        Ok(None)
    }
}



fn parse_map_mapping<'a,'b>(parser: &mut ScriptParser<'a,'b>) -> Result<Option<ASTMapMapping<'a>>, ParseError> {
    if let Some(ScriptToken::KEY_ITEM) = parser.peek() {
        parser.consume(); // consume bg

        if let Some(ScriptToken::LIT_VAR(label)) = parser.consume() {
            if let Some(ScriptToken::SYM_BIND) = parser.consume() {
                if let Some(ScriptToken::LIT_VAR(dialog)) = parser.consume() {
                    if let Some(ScriptToken::SYM_SEMI) = parser.consume() {
                        Ok(Some(
                            ASTMapMapping::Item(label, dialog)
                        ))
                    } else {
                        Err(ParseError::BadExpression(
                            format!("[{:?}:{:?}] Bad mapping declaration - missing terminating semicolon", parser.lexer.line, parser.lexer.col)
                        ))
                    }
                } else {
                    Err(ParseError::BadExpression(
                        format!("[{:?}:{:?}] Bad mapping declaration - missing mapped dialog", parser.lexer.line, parser.lexer.col)
                    ))

                }
            } else {
                Err(ParseError::BadExpression(
                    format!("[{:?}:{:?}] Bad mapping declaration - missing bind ->", parser.lexer.line, parser.lexer.col)
                ))

            }
        } else {
            Err(ParseError::BadExpression(
                format!("[{:?}:{:?}] Bad mapping declaration - missing label", parser.lexer.line, parser.lexer.col)
            ))
        }
    } else if let Some(ScriptToken::KEY_PROP) = parser.peek() {
        parser.consume(); // consume bg

        if let Some(ScriptToken::LIT_VAR(label)) = parser.consume() {
            if let Some(ScriptToken::SYM_BIND) = parser.consume() {
                if let Some(ScriptToken::LIT_VAR(dialog)) = parser.consume() {
                    if let Some(ScriptToken::SYM_SEMI) = parser.consume() {
                        Ok(Some(
                            ASTMapMapping::Prop(label, dialog)
                        ))
                    } else {
                        Err(ParseError::BadExpression(
                            format!("[{:?}:{:?}] Bad mapping declaration - missing terminating semicolon", parser.lexer.line, parser.lexer.col)
                        ))
                    }
                } else {
                    Err(ParseError::BadExpression(
                        format!("[{:?}:{:?}] Bad mapping declaration - missing mapped dialog", parser.lexer.line, parser.lexer.col)
                    ))

                }
            } else {
                Err(ParseError::BadExpression(
                    format!("[{:?}:{:?}] Bad mapping declaration - missing bind ->", parser.lexer.line, parser.lexer.col)
                ))

            }
        } else {
            Err(ParseError::BadExpression(
                format!("[{:?}:{:?}] Bad mapping declaration - missing label", parser.lexer.line, parser.lexer.col)
            ))
        }
    } else {
        Ok(None)
    }
}






fn parse_variable_decl<'a,'b>(parser: &mut ScriptParser<'a,'b>) -> Result<Option<ASTVariableDeclaration<'a>>, ParseError> {
    if let Some(ScriptToken::KEY_VARIABLE) = parser.peek() {
        parser.consume(); // consume variable

        if let Some(ty) = parser.consume() {
            match ty {
                ScriptToken::KEY_INT => {
                    if let Some(ScriptToken::LIT_VAR(label)) = parser.consume() {
                        if let Some(ScriptToken::SYM_ASSIGN) = parser.consume() {
                            if let Some(expr) = parse_expr(parser)? {
                                if let Some(ScriptToken::SYM_SEMI) = parser.consume() {
                                    Ok(Some(ASTVariableDeclaration::Int(label, expr)))
                                } else {
                                    Err(ParseError::BadExpression(
                                        format!("[{:?}:{:?}] Bad Variable declaration - missing terminating ;", parser.lexer.line, parser.lexer.col)
                                    ))
                                }
                            } else {
                                Err(ParseError::BadExpression(
                                    format!("[{:?}:{:?}] Bad Variable declaration - invalid expression", parser.lexer.line, parser.lexer.col)
                                ))
                            }
                        } else {
                            Err(ParseError::BadExpression(
                                format!("[{:?}:{:?}] Bad Variable declaration - missing assignment symbol", parser.lexer.line, parser.lexer.col)
                            ))
                        }
                    } else {
                        Err(ParseError::BadExpression(
                            format!("[{:?}:{:?}] Bad Variable declaration - variable names must be labels", parser.lexer.line, parser.lexer.col)
                        ))
                    }
                }
                ScriptToken::KEY_TEXT => {
                     if let Some(ScriptToken::LIT_VAR(label)) = parser.consume() {
                        if let Some(ScriptToken::SYM_ASSIGN) = parser.consume() {
                            if let Some(ScriptToken::LIT_STR(text)) = parser.consume() {
                                if let Some(ScriptToken::SYM_SEMI) = parser.consume() {
                                    Ok(Some(ASTVariableDeclaration::Text(label, text)))
                                } else {
                                    Err(ParseError::BadExpression(
                                        format!("[{:?}:{:?}] Bad Variable declaration - missing terminating ;", parser.lexer.line, parser.lexer.col)
                                    ))
                                }
                            } else {
                                Err(ParseError::BadExpression(
                                    format!("[{:?}:{:?}] Bad Variable declaration - invalid text", parser.lexer.line, parser.lexer.col)
                                ))
                            }
                        } else {
                            Err(ParseError::BadExpression(
                                format!("[{:?}:{:?}] Bad Variable declaration - missing assignment symbol", parser.lexer.line, parser.lexer.col)
                            ))
                        }
                    } else {
                        Err(ParseError::BadExpression(
                            format!("[{:?}:{:?}] Bad Variable declaration - variable names must be labels", parser.lexer.line, parser.lexer.col)
                        ))
                    }
                }
                x => Err(ParseError::BadExpression(
                    format!("[{:?}:{:?}] Bad Variable declaration - invalid type {:?}", parser.lexer.line, parser.lexer.col, x)
                ))
            }
        } else {
            Err(ParseError::BadExpression(
                format!("[{:?}:{:?}] Bad Variable declaration - input terminates early", parser.lexer.line, parser.lexer.col)
            ))
        }
    } else {
        Ok(None)
    }
}



fn parse_background_decl<'a,'b>(parser: &mut ScriptParser<'a,'b>) -> Result<Option<ASTBackgroundDeclaration<'a>>, ParseError> {
    if let Some(ScriptToken::KEY_BG) = parser.peek() {
        parser.consume(); // consume bg

        if let Some(ScriptToken::LIT_VAR(label)) = parser.consume() {
            if let Some(ScriptToken::SYM_COLON) = parser.consume() {
                if let Some(ScriptToken::LIT_STR(path)) = parser.consume() {
                    if let Some(ScriptToken::SYM_SEMI) = parser.consume() {
                        Ok(Some(ASTBackgroundDeclaration {
                            name: label,
                            file: path,
                        }))
                    } else {
                        Err(ParseError::BadExpression(
                            format!("[{:?}:{:?}] Bad character declaration - missing starting lbrace", parser.lexer.line, parser.lexer.col)
                        ))
                    }
                } else {
                    Err(ParseError::BadExpression(
                        format!("[{:?}:{:?}] Bad character declaration - need file path", parser.lexer.line, parser.lexer.col)
                    ))

                }
            } else {
                Err(ParseError::BadExpression(
                    format!("[{:?}:{:?}] Bad character declaration - missing semicolon", parser.lexer.line, parser.lexer.col)
                ))

            }
        } else {
            Err(ParseError::BadExpression(
                format!("[{:?}:{:?}] Bad character declaration - character labels must be labels", parser.lexer.line, parser.lexer.col)
            ))
        }
    } else {
        Ok(None)
    }
}





fn parse_character_decl<'a,'b>(parser: &mut ScriptParser<'a,'b>) -> Result<Option<ASTCharacterDeclaration<'a>>, ParseError> {
    if let Some(ScriptToken::KEY_CHARACTER) = parser.peek() {
        parser.consume(); // consume character

        if let Some(ScriptToken::LIT_VAR(label)) = parser.consume() {
            if let Some(ScriptToken::SYM_COLON) = parser.consume() {
                if let Some(ScriptToken::LIT_STR(path)) = parser.consume() {
                    if let Some(ScriptToken::SYM_LBRACE) = parser.consume() {

                        let mut states = Vec::new();

                        loop {
                            if let Some(ScriptToken::LIT_VAR(state)) = parser.consume() {
                                states.push(state);

                                if let Some(value) = parser.consume() {
                                    match value {
                                        ScriptToken::SYM_COMMA => {},
                                        ScriptToken::SYM_RBRACE => break,
                                        _ => return Err(ParseError::BadExpression(
                                            format!("[{:?}:{:?}] Bad character declaration - incorrect state format", parser.lexer.line, parser.lexer.col)
                                        ))
                                    }
                                } else {
                                    return Err(ParseError::BadExpression(
                                        format!("[{:?}:{:?}] Bad character declaration - input terminates early", parser.lexer.line, parser.lexer.col)
                                    ));

                                }
                            } else {
                                return Err(ParseError::BadExpression(
                                    format!("[{:?}:{:?}] Bad character declaration - state names must be labels", parser.lexer.line, parser.lexer.col)
                                ));

                            }
                        }

                        Ok(Some(ASTCharacterDeclaration {
                            name: label,
                            file: path,
                            character_states: states
                        }))
                    } else {
                        Err(ParseError::BadExpression(
                            format!("[{:?}:{:?}] Bad character declaration - missing starting lbrace", parser.lexer.line, parser.lexer.col)
                        ))
                    }
                } else {
                    Err(ParseError::BadExpression(
                        format!("[{:?}:{:?}] Bad character declaration - need file path", parser.lexer.line, parser.lexer.col)
                    ))

                }
            } else {
                Err(ParseError::BadExpression(
                    format!("[{:?}:{:?}] Bad character declaration - missing semicolon", parser.lexer.line, parser.lexer.col)
                ))

            }
        } else {
            Err(ParseError::BadExpression(
                format!("[{:?}:{:?}] Bad character declaration - character labels must be labels", parser.lexer.line, parser.lexer.col)
            ))
        }
    } else {
        Ok(None)
    }
}


fn parse_dialog<'a,'b>(parser: &mut ScriptParser<'a,'b>) -> Result<Option<ASTDialog<'a>>, ParseError> {

    if let Some(ScriptToken::KEY_DIALOG) = parser.peek() {
        parser.consume(); // consume dialog

        if let Some(ScriptToken::LIT_VAR(label)) = parser.consume() {

            if let Some(ScriptToken::SYM_LBRACE) = parser.consume() {
                let body = parse_body(parser)?;

                if let Some(ScriptToken::SYM_RBRACE) = parser.consume() {
                    Ok(Some(ASTDialog{
                        name: label,
                        operations: body
                    }))
                } else {
                    Err(ParseError::BadExpression(
                        format!("[{:?}:{:?}] Bad dialog declaration - missing terminating Rbrace", parser.lexer.line, parser.lexer.col)
                    ))
                }
            } else {
                Err(ParseError::BadExpression(
                    format!("[{:?}:{:?}] Bad dialog declaration - missing starting Lbrace", parser.lexer.line, parser.lexer.col)
                ))

            }
        } else {
            Err(ParseError::BadExpression(
                format!("[{:?}:{:?}] Bad dialog operation - missing dialog name", parser.lexer.line, parser.lexer.col)
            ))
        }
    } else {
        Ok(None)
    }

}




fn parse_body<'a,'b>(parser: &mut ScriptParser<'a,'b>) -> Result<ASTDialogBody<'a>, ParseError> {
    let mut result = Vec::new();

    while let Some(value) = parse_op(parser)? {
        result.push(value);
    }

    Ok(ASTDialogBody(result))
}


fn parse_while_op<'a,'b>(parser: &mut ScriptParser<'a,'b>) -> Result<Option<ASTWhileOp<'a>>, ParseError> {
    if let Some(ScriptToken::KEY_WHILE) = parser.peek() {
        parser.consume(); // consume body

        if let Some(bool_expr) = parse_bool_expr(parser)? {
            if let Some(ScriptToken::SYM_LBRACE) = parser.consume() {

                let body = parse_body(parser)?;

                if let Some(ScriptToken::SYM_RBRACE) = parser.consume() {
                    Ok(Some(ASTWhileOp(bool_expr, body)))
                } else {
                    Err(ParseError::BadExpression(
                        format!("[{:?}:{:?}] Bad while operation - missing terminating Rbrace", parser.lexer.line, parser.lexer.col)
                    ))
                }
            } else {
                Err(ParseError::BadExpression(
                    format!("[{:?}:{:?}] Bad while operation - missing starting Lbrace", parser.lexer.line, parser.lexer.col)
                ))
            }
        } else {
            Err(ParseError::BadExpression(
                format!("[{:?}:{:?}] Bad while operation - missing condition", parser.lexer.line, parser.lexer.col)
            ))
        }

    } else {
        Ok(None)
    }
}



fn parse_if_op<'a,'b>(parser: &mut ScriptParser<'a,'b>) -> Result<Option<ASTIfOp<'a>>, ParseError> {
    if let Some(ScriptToken::KEY_IF) = parser.peek() {
        parser.consume(); // consume If

        if let Some(bool_expr) = parse_bool_expr(parser)? {
            if let Some(ScriptToken::SYM_LBRACE) = parser.consume() {

                let body = parse_body(parser)?;

                if let Some(ScriptToken::SYM_RBRACE) = parser.consume() {

                    if let Some(ScriptToken::KEY_ELSE) = parser.peek() {
                        parser.consume(); // consume else

                        if let Some(ScriptToken::SYM_LBRACE) = parser.consume() {
                            let else_body = parse_body(parser)?;

                            if let Some(ScriptToken::SYM_RBRACE) = parser.consume() {
                                Ok(Some(ASTIfOp(bool_expr, body, Some(else_body))))
                            } else {
                                Err(ParseError::BadExpression(
                                    format!("[{:?}:{:?}] Bad if operation - missing terminating Rbrace for else", parser.lexer.line, parser.lexer.col)
                                ))
                            }
                        } else {
                            Err(ParseError::BadExpression(
                                format!("[{:?}:{:?}] Bad if operation - missing starting Lbrace", parser.lexer.line, parser.lexer.col)
                            ))
                        }
                    } else {
                        Ok(Some(ASTIfOp(bool_expr, body, None)))
                    }
                } else {
                    Err(ParseError::BadExpression(
                        format!("[{:?}:{:?}] Bad if operation - missing terminating Rbrace", parser.lexer.line, parser.lexer.col)
                    ))
                }
            } else {
                Err(ParseError::BadExpression(
                    format!("[{:?}:{:?}] Bad if operation - missing starting Lbrace", parser.lexer.line, parser.lexer.col)
                ))
            }
        } else {
            Err(ParseError::BadExpression(
                format!("[{:?}:{:?}] Bad if operation - missing condition", parser.lexer.line, parser.lexer.col)
            ))
        }

    } else {
        Ok(None)
    }
}






fn parse_op<'a,'b>(parser: &mut ScriptParser<'a,'b>) -> Result<Option<ASTDialogOperations<'a>>, ParseError> {
    if let Some(value) = parser.peek() {
        match value {
            ScriptToken::KEY_GOTO => {
                parser.consume(); // consume goto
                if let Some(ScriptToken::LIT_VAR(label)) = parser.consume() {
                    if let Some(ScriptToken::SYM_SEMI) = parser.consume() {
                        Ok(Some(ASTDialogOperations::Goto(label)))
                    } else {
                        Err(ParseError::BadExpression(
                            format!("[{:?}:{:?}] Bad Goto operation - missing semicolon", parser.lexer.line, parser.lexer.col)
                        ))
                    }
                } else {
                    Err(ParseError::BadExpression(
                        format!("[{:?}:{:?}] Bad Goto operation - missing label", parser.lexer.line, parser.lexer.col)
                    ))
                }
            }
            ScriptToken::KEY_SHOW => {
                parser.consume(); // consume show
                if let Some(ScriptToken::LIT_VAR(label)) = parser.consume() {
                    if let Some(ScriptToken::SYM_SEMI) = parser.consume() {
                        Ok(Some(ASTDialogOperations::Show(label)))
                    } else {
                        Err(ParseError::BadExpression(
                            format!("[{:?}:{:?}] Bad Show operation - missing semicolon", parser.lexer.line, parser.lexer.col)
                        ))
                    }

                } else {
                    Err(ParseError::BadExpression(
                        format!("[{:?}:{:?}] Bad Show operation - missing bgname", parser.lexer.line, parser.lexer.col)
                    ))
                }
            }
            ScriptToken::KEY_RUN => {
                parser.consume(); // consume run
                if let Some(ScriptToken::LIT_VAR(label)) = parser.consume() {
                    if let Some(ScriptToken::SYM_SEMI) = parser.consume() {
                        Ok(Some(ASTDialogOperations::Run(label)))
                    } else {
                        Err(ParseError::BadExpression(
                            format!("[{:?}:{:?}] Bad run operation - missing semicolon", parser.lexer.line, parser.lexer.col)
                        ))
                    }


                } else {
                    Err(ParseError::BadExpression(
                        format!("[{:?}:{:?}] Bad Run operation - missing target", parser.lexer.line, parser.lexer.col)
                    ))
                }
            }
            ScriptToken::KEY_DROP => {
                parser.consume(); // consume drop
                if let Some(ScriptToken::LIT_VAR(label)) = parser.consume() {
                    if let Some(ScriptToken::SYM_SEMI) = parser.consume() {
                        Ok(Some(ASTDialogOperations::Drop(label)))
                    } else {
                        Err(ParseError::BadExpression(
                            format!("[{:?}:{:?}] Bad drop operation - missing semicolon", parser.lexer.line, parser.lexer.col)
                        ))
                    }

                } else {
                    Err(ParseError::BadExpression(
                        format!("[{:?}:{:?}] Bad drop operation - missing target", parser.lexer.line, parser.lexer.col)
                    ))
                }
            }
            ScriptToken::KEY_STATE => {
                parser.consume(); // consume state
                if let Some(ScriptToken::LIT_VAR(label)) = parser.consume() {
                    if let Some(ScriptToken::LIT_VAR(value)) = parser.consume() {
                        if let Some(ScriptToken::SYM_SEMI) = parser.consume() {
                            Ok(Some(ASTDialogOperations::State(label, value)))
                        } else {
                            Err(ParseError::BadExpression(
                                format!("[{:?}:{:?}] Bad state operation - missing semicolon", parser.lexer.line, parser.lexer.col)
                            ))
                        }


                    } else {
                        Err(ParseError::BadExpression(
                            format!("[{:?}:{:?}] Bad state operation - missing state value", parser.lexer.line, parser.lexer.col)
                        ))
                    }



                } else {
                    Err(ParseError::BadExpression(
                        format!("[{:?}:{:?}] Bad state operation - missing target", parser.lexer.line, parser.lexer.col)
                    ))
                }
            }
            ScriptToken::KEY_INPUT => {
                if let Some(input_op) = parse_input_op(parser)? {
                    Ok(Some(ASTDialogOperations::Input(input_op)))
                } else {unreachable!()}
            }
            ScriptToken::KEY_POSITION => {
                parser.consume(); // consume position
                // position <NAME> <LEFT|RIGHT|CENTER|OFFSCREEN>
                if let Some(ScriptToken::LIT_VAR(name)) = parser.consume() {

                    if let Some(value) = parser.consume() {
                        let position = match value {
                            ScriptToken::KEY_LEFT => Some(CharacterPosition::LEFT),
                            ScriptToken::KEY_RIGHT => Some(CharacterPosition::RIGHT),
                            ScriptToken::KEY_CENTER => Some(CharacterPosition::CENTER),
                            ScriptToken::KEY_OFFSCREEN => None,
                            x => return Err(ParseError::BadExpression(
                                    format!("[{:?}:{:?}] Bad position operation - bad position token {:?}", parser.lexer.line, parser.lexer.col, x)
                                ))
                        };

                        if let Some(ScriptToken::SYM_SEMI) = parser.consume() {
                            Ok(Some(
                                ASTDialogOperations::Position(name, position)
                            ))
                        } else {
                            Err(ParseError::BadExpression(
                                format!("[{:?}:{:?}] Bad position operation - missing semicolon", parser.lexer.line, parser.lexer.col)
                            ))
                        }

                    } else {
                        Err(ParseError::BadExpression(
                            format!("[{:?}:{:?}] Bad position operation - missing position value", parser.lexer.line, parser.lexer.col)
                        ))
                    }
                } else {
                    Err(ParseError::BadExpression(
                        format!("[{:?}:{:?}] Bad position operation - missing target", parser.lexer.line, parser.lexer.col)
                    ))
                }
            }
            ScriptToken::KEY_SPEAK | ScriptToken::KEY_INFO => {
                if let Some(say_op) = parse_say_op(parser)? {
                    Ok(Some(ASTDialogOperations::Say(say_op)))
                } else {unreachable!()}
            },
            ScriptToken::KEY_REMOVE => {
                parser.consume();
                if let Some(ScriptToken::SYM_SEMI) = parser.consume() {
                    Ok(Some(ASTDialogOperations::Remove))
                } else {
                    Err(ParseError::BadExpression(
                        format!("[{:?}:{:?}] Bad remove operation - missing terminating semicolon", parser.lexer.line, parser.lexer.col)
                    ))
                }
            },
            ScriptToken::KEY_VARIABLE  => {
                if let Some(assign_op) = parse_assignment_op(parser)? {
                    Ok(Some(ASTDialogOperations::Assign(assign_op)))
                } else {unreachable!()}
            },
            ScriptToken::KEY_WHILE => {
                if let Some(while_op) = parse_while_op(parser)? {
                    Ok(Some(ASTDialogOperations::While(while_op)))
                } else {unreachable!()}
            },
            ScriptToken::KEY_IF => {
                if let Some(if_op) = parse_if_op(parser)? {
                    Ok(Some(ASTDialogOperations::If(if_op)))
                } else {unreachable!()}
            },
            _ => Ok(None)
        }
    } else {
        Ok(None)
    }
}



fn parse_say_op<'a,'b>(parser: &mut ScriptParser<'a,'b>) -> Result<Option<ASTSayOperation<'a>>, ParseError> {

    if let Some(ScriptToken::KEY_SPEAK) = parser.peek() {
        parser.consume(); // consume speak

        if let Some(ScriptToken::LIT_VAR(name)) = parser.consume() {
            if let Some(rem) = parser.consume() {
                let value = match rem {
                    ScriptToken::LIT_STR(string) => ASTSpeakable::Text(string),
                    ScriptToken::LIT_VAR(string) => ASTSpeakable::Var(string),
                    _ => return Err(ParseError::BadExpression(
                        format!("[{:?}:{:?}] Bad Speak operation - bad text value", parser.lexer.line, parser.lexer.col)
                    ))
                };

                if let Some(ScriptToken::SYM_SEMI) = parser.consume() {
                    Ok(Some(
                        ASTSayOperation {
                            name: Some(name),
                            text: value
                        }
                    ))
                } else {
                    Err(ParseError::BadExpression(
                        format!("[{:?}:{:?}] Bad Speak Operation - missing semicolon", parser.lexer.line, parser.lexer.col)
                    ))
                }
            } else {
                Err(ParseError::BadExpression(
                    format!("[{:?}:{:?}] Bad Speak Operation - missing arguments", parser.lexer.line, parser.lexer.col)
                ))
            }
        } else {
            Err(ParseError::BadExpression(
                format!("[{:?}:{:?}] Bad Speak Operation - missing all arguments", parser.lexer.line, parser.lexer.col)
            ))
        }
    } else if let Some(ScriptToken::KEY_INFO) = parser.peek() {
        parser.consume(); // consume info
        if let Some(rem) = parser.consume() {
            let value = match rem {
                ScriptToken::LIT_STR(string) => ASTSpeakable::Text(string),
                ScriptToken::LIT_VAR(string) => ASTSpeakable::Var(string),
                x => return Err(ParseError::BadExpression(
                    format!("[{:?}:{:?}] Bad Speak Operation - bad text {:?}", parser.lexer.line, parser.lexer.col, x)
                ))
            };

            if let Some(ScriptToken::SYM_SEMI) = parser.consume() {
                Ok(Some(
                    ASTSayOperation {
                        name: None,
                        text: value
                    }
                ))
            } else {
                Err(ParseError::BadExpression(
                    format!("[{:?}:{:?}] Bad Speak Operation", parser.lexer.line, parser.lexer.col)
                ))
            }
        } else {
            Err(ParseError::BadExpression(
                format!("[{:?}:{:?}] Bad Speak Operation", parser.lexer.line, parser.lexer.col)
            ))
        }
    } else {
        Ok(None)
    }
}


fn parse_input_op<'a,'b>(parser: &mut ScriptParser<'a,'b>) -> Result<Option<ASTInputOperation<'a>>, ParseError> {
    if let Some(ScriptToken::KEY_INPUT) = parser.peek() {
        parser.consume();
        if let Some(ScriptToken::SYM_LBRACE) = parser.consume() {

            let mut values = Vec::new();

            loop {

                let option = {
                    if let Some(value) = parser.consume() {
                        match value {
                            ScriptToken::LIT_STR(string) => ASTSpeakable::Text(string),
                            ScriptToken::LIT_VAR(var) => ASTSpeakable::Var(var),
                            ScriptToken::SYM_RBRACE => break,
                            _ => return Err(ParseError::BadExpression(
                                    format!("[{:?}:{:?}] Bad Input Operation - options must either be text or a variable", parser.lexer.line, parser.lexer.col)
                                ))
                        }
                    } else {
                        return Err(ParseError::BadExpression(
                            format!("[{:?}:{:?}] Bad Input Operation - missing terminating Rbrace", parser.lexer.line, parser.lexer.col)
                        ));
                    }
                };

                if let Some(ScriptToken::SYM_BIND) = parser.consume() {
                    if let Some(ScriptToken::SYM_LBRACE) = parser.consume() {
                        let body = parse_body(parser)?;

                        if let Some(ScriptToken::SYM_RBRACE) = parser.consume() {
                            values.push((option, body));
                        } else {
                            return Err(ParseError::BadExpression(
                                format!("[{:?}:{:?}] Bad Input Operation - missing terminating Rbrace for option", parser.lexer.line, parser.lexer.col)
                            ));
                        }
                    } else {
                        return Err(ParseError::BadExpression(
                            format!("[{:?}:{:?}] Bad Input Operation - missing starting Lbrace for option", parser.lexer.line, parser.lexer.col)
                        ));
                    }
                } else {
                    return Err(ParseError::BadExpression(
                        format!("[{:?}:{:?}] Bad Input Operation - missing bind operation following option", parser.lexer.line, parser.lexer.col)
                    ));
                }
            }

            Ok(Some(ASTInputOperation(values)))
        } else {
            Err(ParseError::BadExpression(
                format!("[{:?}:{:?}] Bad Input Operation - missing starting Lbrace", parser.lexer.line, parser.lexer.col)
            ))
        }

    } else {
        Ok(None)
    }
}


fn parse_base_expr<'a,'b>(parser: &mut ScriptParser<'a,'b>) -> Result<Option<ASTExpression<'a>>, ParseError> {

    let mut consume = false;
    match parser.peek() {
        Some(value) => match value {
            ScriptToken::LIT_NUMERIC(value) => {
                consume = true;
            },
            ScriptToken::LIT_VAR(value) => {
                consume = true;
            },
            ScriptToken::SYM_LPAREN => {
                parser.consume(); // consume the Lparen
                let expr = parse_expr(parser)?;

                if parser.peek() != Some(&ScriptToken::SYM_RPAREN) {
                    return Err(ParseError::BadExpression(format!("[{:?}:{:?}] Missing Rparen )", parser.lexer.line, parser.lexer.col)));
                }

                parser.consume(); // consume the Rparen
                return Ok(expr);
            }
            _ => return Ok(None),
        }
        None => return Ok(None)
    }

    if consume {
        match parser.consume() {
            Some(value) => match value {
                ScriptToken::LIT_NUMERIC(value) => {
                    Ok(Some(ASTExpression::BaseNumeric(value)))
                },
                ScriptToken::LIT_VAR(value) => {
                    Ok(Some(ASTExpression::BaseVariable(value)))
                },
                _ => unreachable!()
            }
            None => unreachable!()
        }
    } else {
        Ok(None)
    }
}

fn parse_factor_expr<'a,'b>(parser: &mut ScriptParser<'a,'b>) -> Result<Option<ASTExpression<'a>>, ParseError> {
    // FACTOR_EXPR := BASE_EXPR
    match parse_base_expr(parser)? {
        Some(mut expr) => {
            loop {
                match parser.peek() {
                    Some(value) => match value {
                        ScriptToken::SYM_MULTIPLY => {
                            parser.consume(); // consume *
                            match parse_base_expr(parser)? {
                                Some(other_expr) => {
                                    expr = ASTExpression::Mul(Box::new(expr), Box::new(other_expr));
                                },
                                None => return Err(ParseError::BadExpression(format!("[{:?}:{:?}] Bad Multiply Expression", parser.lexer.line, parser.lexer.col)))
                            }
                        }
                        ScriptToken::SYM_DIVIDE => {
                            parser.consume(); // consume /
                            match parse_base_expr(parser)? {
                                Some(other_expr) => {
                                    expr = ASTExpression::Div(Box::new(expr), Box::new(other_expr));
                                },
                                None => return Err(ParseError::BadExpression(format!("[{:?}:{:?}] Bad Divide Expression", parser.lexer.line, parser.lexer.col)))
                            }
                        }
                        _ => break
                    }
                    None => break
                }

            }
            Ok(Some(expr))
        }
        None => Ok(None)
    }

}

/*
EXPR := FACTOR_EXPR + FACTOR_EXPR | FACTOR_EXPR - FACTOR_EXPR | FACTOR_EXPR
FACTOR_EXPR := BASE_EXPR * BASE_EXPR | BASE_EXPR / BASE_EXPR | BASE_EXPR
BASE_EXPR := VALUE | (EXPR)
 */
fn parse_expr<'a,'b>(parser: &mut ScriptParser<'a,'b>) -> Result<Option<ASTExpression<'a>>, ParseError> {
    // FACTOR_EXPR := BASE_EXPR
    match parse_factor_expr(parser)? {
        Some(mut expr) => {
            loop {
                match parser.peek() {
                    Some(value) => {
                            match value {
                            ScriptToken::SYM_PLUS => {
                                parser.consume(); // consume *
                                match parse_factor_expr(parser)? {
                                    Some(other_expr) => {
                                        expr = ASTExpression::Add(Box::new(expr), Box::new(other_expr));
                                    },
                                    None => return Err(ParseError::BadExpression(format!("[{:?}:{:?}] Bad Add Expression", parser.lexer.line, parser.lexer.col)))
                                }
                            }
                            ScriptToken::SYM_MINUS => {
                                parser.consume(); // consume /
                                match parse_factor_expr(parser)? {
                                    Some(other_expr) => {
                                        expr = ASTExpression::Sub(Box::new(expr), Box::new(other_expr));
                                    },
                                    None => return Err(ParseError::BadExpression(format!("[{:?}:{:?}] Bad Minus Expression", parser.lexer.line, parser.lexer.col)))
                                }
                            }
                            _ => break
                        }
                    }
                    None => break
                }

            }
            Ok(Some(expr))
        }
        None => Ok(None)
    }
}

/*
BOOL_EXPR := EXPR < EXPR
| EXPR > EXPR
| EXPR <= EXPR
| EXPR >= EXPR
| EXPR == EXPR
| check var
| BOOL_EXPR && BOOL_EXPR
| BOOL_EXPR || BOOL_EXPR
 */

fn parse_base_bool_expr<'a,'b>(parser: &mut ScriptParser<'a,'b>) -> Result<Option<ASTBooleanExpression<'a>>, ParseError> {

    match parser.peek() {
        Some(value) => match value {
            ScriptToken::KEY_CHECK => {
                parser.consume(); // consume check
                if let Some(ScriptToken::LIT_VAR(var)) = parser.consume() {
                    return Ok(Some(ASTBooleanExpression::Check(var)));
                } else {
                    return Err(ParseError::BadExpression(format!("[{:?}:{:?}] Bad check operation", parser.lexer.line, parser.lexer.col)))
                }
            }
            _ => (),
        }
        None => return Ok(None)
    }

    match parse_expr(parser)? {
        Some(expr) => {
            match parser.consume() {
                Some(value) => match value {
                    ScriptToken::SYM_LT => {
                            match parse_expr(parser)? {
                                Some(other_expr) => Ok(Some(ASTBooleanExpression::Lt(expr, other_expr))),
                                None => Err(ParseError::BadExpression(format!("[{:?}:{:?}] Bad boolean expression", parser.lexer.line, parser.lexer.col)))
                            }
                    }
                    ScriptToken::SYM_LEQ => {
                        match parse_expr(parser)? {
                            Some(other_expr) => Ok(Some(ASTBooleanExpression::Le(expr, other_expr))),
                            None => Err(ParseError::BadExpression(format!("[{:?}:{:?}] Bad boolean expression", parser.lexer.line, parser.lexer.col)))
                        }
                    }
                    ScriptToken::SYM_GT => {
                        match parse_expr(parser)? {
                            Some(other_expr) => Ok(Some(ASTBooleanExpression::Gt(expr, other_expr))),
                            None => Err(ParseError::BadExpression(format!("[{:?}:{:?}] Bad boolean expression", parser.lexer.line, parser.lexer.col)))
                        }
                    }
                    ScriptToken::SYM_GEQ => {
                        match parse_expr(parser)? {
                            Some(other_expr) => Ok(Some(ASTBooleanExpression::Ge(expr, other_expr))),
                            None => Err(ParseError::BadExpression(format!("[{:?}:{:?}] Bad boolean expression", parser.lexer.line, parser.lexer.col)))
                        }
                    }
                    ScriptToken::SYM_EQ => {
                        match parse_expr(parser)? {
                            Some(other_expr) => Ok(Some(ASTBooleanExpression::Eq(expr, other_expr))),
                            None => Err(ParseError::BadExpression(format!("[{:?}:{:?}] Bad boolean expression", parser.lexer.line, parser.lexer.col)))
                        }
                    }
                    _ => Err(ParseError::BadExpression(format!("[{:?}:{:?}] Bad boolean expression", parser.lexer.line, parser.lexer.col)))
                }
                None => Err(ParseError::BadExpression(format!("[{:?}:{:?}] Bad boolean expression", parser.lexer.line, parser.lexer.col)))
            }
        }
        None => Ok(None)
    }
}

/*
BOOL_EXPR := EXPR < EXPR
| EXPR > EXPR
| EXPR <= EXPR
| EXPR >= EXPR
| EXPR == EXPR
| check var
| BOOL_EXPR && BOOL_EXPR
| BOOL_EXPR || BOOL_EXPR
 */
fn parse_bool_expr<'a,'b>(parser: &mut ScriptParser<'a,'b>) -> Result<Option<ASTBooleanExpression<'a>>, ParseError> {

    match parse_base_bool_expr(parser)? {
        Some(mut expr) => {
            loop {
                match parser.peek() {
                    Some(value) => {
                            match value {
                            ScriptToken::SYM_AND => {
                                parser.consume(); // consume *
                                match parse_base_bool_expr(parser)? {
                                    Some(other_expr) => {
                                        expr = ASTBooleanExpression::And(Box::new(expr), Box::new(other_expr));
                                    },
                                    None => return Err(ParseError::BadExpression(format!("[{:?}:{:?}] Bad And Expression", parser.lexer.line, parser.lexer.col)))
                                }
                            }
                            ScriptToken::SYM_OR => {
                                parser.consume(); // consume /
                                match parse_base_bool_expr(parser)? {
                                    Some(other_expr) => {
                                        expr = ASTBooleanExpression::Or(Box::new(expr), Box::new(other_expr));
                                    },
                                    None => return Err(ParseError::BadExpression(format!("[{:?}:{:?}] Bad Minus Expression", parser.lexer.line, parser.lexer.col)))
                                }
                            }
                            _ => break
                        }
                    }
                    None => break
                }

            }
            Ok(Some(expr))
        }
        None => Ok(None)
    }
}


fn parse_assignment_op<'a,'b>(parser: &mut ScriptParser<'a,'b>) -> Result<Option<ASTAssignmentOperation<'a>>, ParseError> {
    if let Some(ScriptToken::KEY_VARIABLE) = parser.peek() {
        parser.consume(); // consume variable

        if let Some(ScriptToken::LIT_VAR(var)) = parser.consume() {
            if let Some(ScriptToken::SYM_ASSIGN) = parser.consume() {
                let mut string = false;
                match parser.peek() {
                    Some(value) => match value {
                        ScriptToken::KEY_FORMAT => {
                            if let Some(format_expr) = parse_format_expr(parser)? {
                                if let Some(ScriptToken::SYM_SEMI) = parser.consume() {
                                    return Ok(Some(ASTAssignmentOperation::Format(var, format_expr)));
                                } else {
                                    return Err(ParseError::BadExpression(
                                        format!("[{:?}:{:?}] Bad variable assignment Expression - missing terminating semicolon", parser.lexer.line, parser.lexer.col)
                                    ));
                                }
                            } else {
                                return Err(ParseError::BadExpression(
                                    format!("[{:?}:{:?}] Bad variable assignment Expression - invalid format expression", parser.lexer.line, parser.lexer.col)
                                ));
                            }
                        }
                        ScriptToken::LIT_STR(_) => string = true,
                        _ => ()
                    }
                    None => return Err(ParseError::BadExpression(
                            format!("[{:?}:{:?}] Bad variable assignment Expression - incorrect label", parser.lexer.line, parser.lexer.col)
                        ))
                }

                if string {
                    if let Some(ScriptToken::LIT_STR(result)) = parser.consume() {
                        if let Some(ScriptToken::SYM_SEMI) = parser.consume() {
                            return Ok(Some(ASTAssignmentOperation::Str(var, result)));
                        } else {
                            Err(ParseError::BadExpression(
                                format!("[{:?}:{:?}] Bad variable assignment Expression", parser.lexer.line, parser.lexer.col)
                            ))
                        }
                    } else {unreachable!()}
                } else {
                    if let Some(expr) = parse_expr(parser)? {
                        if let Some(ScriptToken::SYM_SEMI) = parser.consume() {
                            return Ok(Some(ASTAssignmentOperation::Expr(var, expr)));
                        } else {
                            Err(ParseError::BadExpression(
                                format!("[{:?}:{:?}] Bad variable assignment Expression", parser.lexer.line, parser.lexer.col)
                            ))
                        }
                    } else {
                        Err(ParseError::BadExpression(
                            format!("[{:?}:{:?}] Bad variable assignment Expression", parser.lexer.line, parser.lexer.col)
                        ))
                    }
                }
            } else {
                Err(ParseError::BadExpression(
                    format!("[{:?}:{:?}] Bad variable assignment Expression", parser.lexer.line, parser.lexer.col)
                ))
            }
        } else {
            Err(ParseError::BadExpression(
                format!("[{:?}:{:?}] Bad variable assignment Expression", parser.lexer.line, parser.lexer.col)
            ))
        }
    } else {
        Ok(None)
    }
}

fn parse_format_expr<'a,'b>(parser: &mut ScriptParser<'a,'b>) -> Result<Option<ASTFormatExpression<'a>>, ParseError> {
    if let Some(ScriptToken::KEY_FORMAT) = parser.peek() {
        parser.consume(); // consume the format operation

        if let Some(ScriptToken::SYM_LPAREN) = parser.consume() {
            if let Some(ScriptToken::LIT_STR(string)) = parser.consume() {
                if let Some(ScriptToken::SYM_COMMA) = parser.consume() {
                    let mut vars = Vec::new();

                    loop {
                        if let Some(ScriptToken::LIT_VAR(var)) = parser.consume() {
                            vars.push(var);
                            match parser.peek() {
                                Some(ScriptToken::SYM_COMMA) => {parser.consume();},
                                Some(ScriptToken::SYM_RPAREN) => {break},
                                _ => return Err(ParseError::BadExpression(
                                        format!("[{:?}:{:?}] Bad Format Expression", parser.lexer.line, parser.lexer.col)
                                    ))
                            }
                        } else {
                            return Err(ParseError::BadExpression(
                                format!("[{:?}:{:?}] Bad Format Expression - arguments must be variables", parser.lexer.line, parser.lexer.col)
                            ));
                        }
                    }
                    parser.consume(); // consume )
                    Ok(Some(ASTFormatExpression{
                        text: string,
                        vars: vars
                    }))
                } else {
                    Err(ParseError::BadExpression(
                        format!("[{:?}:{:?}] Bad Format Expression", parser.lexer.line, parser.lexer.col)
                    ))
                }
            } else {
                Err(ParseError::BadExpression(
                    format!("[{:?}:{:?}] Bad Format Expression", parser.lexer.line, parser.lexer.col)
                ))
            }
        } else {
            Err(ParseError::BadExpression(
                format!("[{:?}:{:?}] Bad Format Expression", parser.lexer.line, parser.lexer.col)
            ))
        }
    } else {
        Ok(None)
    }
}


