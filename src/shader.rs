use std::cell::RefCell;
use std::rc::Rc;
use std::ffi::{CString, CStr};
use std::ptr;

use sdl2::event::Event;

use gl::types::{GLfloat, GLint, GLsizei, GLuint};

use nalgebra::base::{Matrix4, Matrix3, Vector2, Vector3, Vector4};

use log::{error, warn, info, debug, trace};


/// Structure representing shader on the system
#[derive(Debug)]
pub struct Shader {
    id: GLuint,
    use_shader: bool,
}

#[derive(Debug)]
pub enum GLSLError {
    ShaderCompilationError(String),
    ProgramCompilationError(String),
    BadName(String)
}




impl Shader {

    pub fn new<T: Into<Vec<u8>>>(vs_src: T, fs_src: T) -> Result<Self, GLSLError> {
        let mut id = 0;

        let vs_src = CString::new(vs_src).map_err(
            |err| GLSLError::ShaderCompilationError(format!("{:?}", err))
        )?;

        let fs_src = CString::new(fs_src).map_err(
            |err| GLSLError::ShaderCompilationError(format!("{:?}", err))
        )?;


        let s_vert = compile_shader(&vs_src, gl::VERTEX_SHADER)?;
        let s_frag = compile_shader(&fs_src, gl::FRAGMENT_SHADER)?;


        id = compile_program(s_vert, s_frag)?;

        unsafe {
            gl::DeleteShader(s_vert);
            gl::DeleteShader(s_frag);
        }

        Ok(Shader {
            id: id,
            use_shader: false
        })
    }

    pub fn set_use_shader(&mut self, use_shader: bool) {
        self.use_shader = use_shader;
    }

    pub fn enable(&mut self) {
        unsafe  {
            gl::UseProgram(self.id);
        }
    }

    pub fn disable(&mut self) {
        unsafe  {
            gl::UseProgram(0);
        }
    }

    pub fn get_variable_position(&mut self, name: &CString) -> Result<GLuint, GLSLError> {
        let attr = unsafe {
            gl::GetAttribLocation(self.id, name.as_ptr() as _)
        };

        if attr < 0 {
            Err(GLSLError::BadName(
                format!("GetAttribLocation({:?}) = {:?}", name, attr)
            ))
        } else {
            Ok(attr as GLuint)
        }
    }


    pub fn set_fragment_data_location(&mut self, name: &CString, id: GLuint) {
        unsafe {
            gl::BindFragDataLocation(self.id, id, name.as_ptr() as _);
        }
    }

    pub fn set_float(&mut self, name: &CString, value: GLfloat) {
        if self.use_shader {
            self.enable();
        }

        unsafe {
            gl::Uniform1f(
                gl::GetUniformLocation(
                    self.id,
                    name.as_ptr() as _
                ),
                value
            );
        }
    }


    pub fn set_int(&mut self, name: &CString, value: GLint) {
        if self.use_shader {
            self.enable();
        }

        unsafe {
            gl::Uniform1i(
                gl::GetUniformLocation(
                    self.id,
                    name.as_ptr() as _
                ),
                value
            );
        }
    }


    pub fn set_vector2f(&mut self, name: &CString, value: &Vector2<GLfloat>) {
        if self.use_shader {
            self.enable();
        }

        unsafe {
            gl::Uniform2fv(
                gl::GetUniformLocation(
                    self.id,
                    name.as_ptr() as _
                ),
                1,
                value.as_slice().as_ptr()
            );
        }
    }

    pub fn set_vector3f(&mut self, name: &CString, value: &Vector3<GLfloat>) {
        if self.use_shader {
            self.enable();
        }

        unsafe {
            gl::Uniform3fv(
                gl::GetUniformLocation(
                    self.id,
                    name.as_ptr() as _
                ),
                1,
                value.as_slice().as_ptr()
            );
        }
    }

    pub fn set_vector4f(&mut self, name: &CString, value: &Vector4<GLfloat>) {
        if self.use_shader {
            self.enable();
        }

        unsafe {
            gl::Uniform4fv(
                gl::GetUniformLocation(
                    self.id,
                    name.as_ptr() as _
                ),
                1,
                value.as_slice().as_ptr()
            );
        }
    }


    pub fn set_matrix3f(&mut self, name: &CString, value: &Matrix3<GLfloat>) {
        if self.use_shader {
            self.enable();
        }

        unsafe {
            gl::UniformMatrix3fv(
                gl::GetUniformLocation(
                    self.id,
                    name.as_ptr() as _
                ),
                1,
                gl::FALSE,
                value.as_slice().as_ptr()
            );
        }
    }




    pub fn set_matrix4f(&mut self, name: &CString, value: &Matrix4<GLfloat>) {
        if self.use_shader {
            self.enable();
        }

        unsafe {
            gl::UniformMatrix4fv(
                gl::GetUniformLocation(
                    self.id,
                    name.as_ptr() as _
                ),
                1,
                gl::FALSE,
                value.as_slice().as_ptr()
            );
        }
    }

}


impl Drop for Shader {
    fn drop(&mut self) {
        unsafe {
            gl::DeleteProgram(self.id);
        }
    }
}


/// Having compiled a program, retrieves any error messages that were produced
unsafe fn check_program_compile_errors(object: GLuint) -> Option<String> {

    let mut success: GLint = 1;

    gl::GetProgramiv(object, gl::LINK_STATUS, &mut success);

    if success == 0 {
        let mut len =0;
        gl::GetProgramiv(object, gl::INFO_LOG_LENGTH, &mut len);

        let mut error = allocate_cstring_buffer(len as usize);

        gl::GetProgramInfoLog(
            object,
            len,
            ptr::null_mut(),
            error.as_ptr() as *mut gl::types::GLchar
        );

        Some(error.to_string_lossy().into_owned())
    } else {
        None
    }
}

/// Having compiled a shader, retrieves any error messages that were produced
unsafe fn check_shader_compile_errors(object: GLuint) -> Option<String> {
    let mut success: GLint = 1;

    gl::GetShaderiv(object, gl::COMPILE_STATUS, &mut success);

    if success == 0 {
        let mut len =0;
        gl::GetShaderiv(object, gl::INFO_LOG_LENGTH, &mut len);

        let mut error = allocate_cstring_buffer(len as usize);

        gl::GetShaderInfoLog(
            object,
            len,
            ptr::null_mut(),
            error.as_ptr() as *mut gl::types::GLchar
        );

        Some(error.to_string_lossy().into_owned())
    } else {
        None
    }
}


fn compile_shader(source: &CStr, shader_type: GLuint) -> Result<GLuint, GLSLError> {
    unsafe {
        let id = gl::CreateShader(shader_type);
        gl::ShaderSource(id, 1, &source.as_ptr(), ptr::null());
        gl::CompileShader(id);

        if let Some(err_str) = check_shader_compile_errors(id) {
            error!("Shader Compilation Error: {:?}", err_str);
            Err(GLSLError::ShaderCompilationError(err_str))
        } else {
            Ok(id)
        }
    }
}

fn compile_program(vertex_id: GLuint, fragment_id: GLuint) -> Result<GLuint, GLSLError> {
    unsafe {
        let id = gl::CreateProgram();
        gl::AttachShader(id, vertex_id);
        gl::AttachShader(id, fragment_id);

        gl::LinkProgram(id);

        if let Some(err_str) = check_program_compile_errors(id) {
            error!("Program Compilation Error: {:?}", err_str);
            Err(GLSLError::ProgramCompilationError(err_str))
        } else {
            Ok(id)
        }
    }
}


/// allocates an empty CString of the requested length of characters
pub fn allocate_cstring_buffer(len: usize) -> CString {
    let mut buffer: Vec<u8> = Vec::with_capacity(len + 1);
    buffer.extend([b' '].iter().cycle().take(len));
    unsafe {CString::from_vec_unchecked(buffer)}
}

