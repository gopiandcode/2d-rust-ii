use crate::text::TextRenderer;
use crate::graphics::{TempTexturedQuad, Quad};
use crate::globals::RenderingGlobals;
use crate::statics::{APPLICATION_INITIAL_SIZE, OPTION_CORNER_SIZE, OPTION_BOX_SIZE, OPTION_BOX_MARGIN, OPTION_BOX_SPACING};
use crate::input::{FrameInputs, LogicalKey};
use crate::resource_loader::{ResourceManager, TextureID};

use std::vec::IntoIter;

use gl::types::{GLfloat, GLint, GLsizei, GLuint, GLsizeiptr};
use nalgebra::base::{Vector4, Vector3, Vector2};
use serde::{Serialize, Deserialize};


#[derive(Serialize,Deserialize,Debug,Copy,Clone, PartialEq, Eq)]
pub struct BackgroundID(usize);

pub struct BackgroundManager {
    backgrounds: Vec<TextureID>,
    selected: Vec<Option<BackgroundID>>
}



impl BackgroundManager {


    pub fn save_state(&self) -> Vec<Option<BackgroundID>> {
        self.selected.clone()
    }

    pub fn restore_state(&mut self, selected: Vec<Option<BackgroundID>>)  {
        self.selected = selected;
    }



    pub fn new() -> Self {
        Self {
            backgrounds: Vec::new(),
            selected: vec![None]
        }
    }

    pub fn add_background(&mut self, texture: TextureID) -> BackgroundID {
        let id = BackgroundID(self.backgrounds.len());

        self.backgrounds.push(texture);

        id
    }

    pub fn push_stack(&mut self) {
        self.selected.push(None);
    }

    pub fn pop_stack(&mut self) {
        self.selected.pop();
    }


    pub fn set_selected(&mut self, bg: BackgroundID) {
        *self.selected.iter_mut().last().unwrap() = Some(bg);
    }


    pub fn draw(&self, globals: &mut RenderingGlobals, resource_manager: &mut ResourceManager) {

        if let Some(Some(bg)) = self.selected.iter().last() {
            let pos = Vector2::new(0.0, 0.0);
            let mut size = Vector2::new(APPLICATION_INITIAL_SIZE.0 as GLfloat, APPLICATION_INITIAL_SIZE.1 as GLfloat);
            let mut tex_size = Vector2::new(0.0, 0.0);
            let color = Vector4::new(1.0, 1.0, 1.0, 1.0);

            TempTexturedQuad::new(
                &pos,
                &size,
                0.0,
                &pos,
                &size,
                0.0,
                &color,
                self.backgrounds[bg.0]
            ).draw(globals, resource_manager);
        }
    }

}


