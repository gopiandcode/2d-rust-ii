use crate::text::TextRenderer;
use crate::graphics::{TexturedQuad, Quad};
use crate::globals::RenderingGlobals;
use crate::statics::{APPLICATION_INITIAL_SIZE, OPTION_CORNER_SIZE, OPTION_BOX_SIZE, OPTION_BOX_MARGIN, OPTION_BOX_SPACING};
use crate::input::{FrameInputs, LogicalKey};
use crate::resource_loader::{ResourceManager, TextureID};

use std::vec::IntoIter;

use gl::types::{GLfloat, GLint, GLsizei, GLuint, GLsizeiptr};
use nalgebra::base::{Vector4, Vector3, Vector2};

pub struct OptionRendererState {
    option_box: OptionBox,

    options: Vec<String>,

    completed: Option<usize>,
    selected: usize,
}

pub struct OptionRenderer<'a,'b> {
    state: &'a mut OptionRendererState,
    text_renderer: &'a mut TextRenderer<'b>,
}


impl OptionRendererState {
    pub fn new(option_box: OptionBox) -> Self {
        Self {
            option_box,
            completed: Option::None,
            options: Vec::new(),
            selected: 0,
        }
    }

    pub fn renderer<'a,'b>(&'a mut self, text_renderer: &'a mut TextRenderer<'b>) -> OptionRenderer<'a,'b> {
        OptionRenderer {
            state: self,
            text_renderer
        }
    }

}

impl<'a,'b> OptionRenderer<'a,'b> {


    pub fn set_options<T: Into<String>>(&mut self, options: Vec<T>) {
        self.state.selected = 0;
        self.state.completed = Option::None;
        self.state.options = options.into_iter().map(|x| x.into()).collect();
    }

    pub fn update(&mut self, input: FrameInputs, delta_time: f32) {
        if self.state.completed.is_none() && self.state.options.len() > self.state.selected {
            let key_pressed = input.input_keys.iter().last();

            match key_pressed {
                Some(LogicalKey::ENTER) => {
                    self.state.completed = Some(self.state.selected);
                }
                Some(LogicalKey::UP) => if self.state.selected > 0 {self.state.selected -= 1;},
                Some(LogicalKey::DOWN) => {
                    if self.state.selected + 1 < self.state.options.len() {self.state.selected += 1;}
                }
                _ => ()
            }
        }
    }


    pub fn is_complete(&mut self) -> Option<usize> {
        self.state.completed
    }

    pub fn draw(&mut self, globals: &mut RenderingGlobals, resource_manager: &mut ResourceManager) {

        if self.state.completed.is_none() && self.state.options.len() > self.state.selected {

            let no_options = self.state.options.len() as f32;
            let total_size = (no_options * OPTION_BOX_SIZE) + (no_options - 1.0) * OPTION_BOX_SPACING;
            let margins = (APPLICATION_INITIAL_SIZE.1 as f32 - total_size)/2.0;

            let mut position = Vector2::new(
                0.0,
                (APPLICATION_INITIAL_SIZE.1 as f32/2.0) - margins - OPTION_BOX_SIZE/2.0
            );
            let mut text_position = Vector2::new(
                self.state.option_box.base.size[0]/2.0 + OPTION_BOX_MARGIN/2.0,
                margins  + OPTION_BOX_SIZE/2.0
                // (APPLICATION_INITIAL_SIZE.1 as f32/2.0) - margins - OPTION_BOX_SIZE/2.0 + OPTION_CORNER_SIZE.1 /2.0
            );


            let mut size = Vector2::new(
                (self.state.option_box.base.size[0] - OPTION_CORNER_SIZE.0 * 2.0),
                (self.state.option_box.base.size[1] - OPTION_CORNER_SIZE.1 * 2.0),
            );

            for (index, text) in self.state.options.iter().enumerate() {

                self.state.option_box.set_selected(index == self.state.selected);
                self.state.option_box.draw(&position, globals, resource_manager);

                self.text_renderer.add_centered_text(
                    text, 0.3,
                    &text_position,
                    &size,
                    self.state.option_box.get_text_color(),
                );

                self.text_renderer.draw(globals, resource_manager);

                position[1] -= OPTION_BOX_SIZE + OPTION_BOX_SPACING;
                text_position[1] += OPTION_BOX_SIZE + OPTION_BOX_SPACING;
            }

        }
    }
}




pub struct OptionBox {
    corner_texture: (TextureID, TextureID),
    edge_texture: (TextureID, TextureID),
    base_color: (Vector4<GLfloat>, Vector4<GLfloat>),
    corner: TexturedQuad,
    edge: TexturedQuad,
    base: Quad,
    text: (Vector4<GLfloat>, Vector4<GLfloat>),
    active: bool,
}

impl OptionBox {

    pub fn get_text_color(&self) -> &Vector4<GLfloat> {
        if self.active {
            &self.text.0
        } else {
            &self.text.1
        }
    }

    pub fn new(
        mut corner_tex: (TextureID, TextureID),
        mut edge_tex: (TextureID, TextureID),
        base_color: (Vector4<GLfloat>, Vector4<GLfloat>),
        text: (Vector4<GLfloat>, Vector4<GLfloat>),
        mut corner: TexturedQuad,
        mut edge: TexturedQuad,
        mut base: Quad,
        resource_manager: &ResourceManager
    ) -> Self {

        base.position = Vector2::new(0.0, 0.0);
        base.size = Vector2::new(
            APPLICATION_INITIAL_SIZE.0 as f32 - OPTION_BOX_MARGIN, OPTION_BOX_SIZE
        );

        let (corner_texture_width, corner_texture_height) = {
            let texture = &resource_manager[corner.texture];
            (texture.width, texture.height)
        };

        corner.size = Vector2::new(
            OPTION_CORNER_SIZE.0, OPTION_CORNER_SIZE.1
        );

        corner.tex_size = Vector2::new(
            corner_texture_width as f32,
            corner_texture_height as f32
        );


        let (edge_texture_width, edge_texture_height) = {
            let texture = &resource_manager[edge.texture];
            (texture.width, texture.height)
        };


        edge.tex_size = Vector2::new(
            edge_texture_width as f32,
            edge_texture_height as f32
        );


        Self {
            corner_texture: corner_tex,
            edge_texture: edge_tex,
            base_color: base_color,
            corner,
            edge,
            base,
            text,
            active: true
        }
    }

    pub fn set_selected(&mut self, sel: bool) {
        self.active = sel;

        if sel {
            self.corner.texture = self.corner_texture.0;
            self.edge.texture = self.edge_texture.0;
            self.base.color = self.base_color.0.clone();
        } else {
            self.corner.texture = self.corner_texture.1;
            self.edge.texture = self.edge_texture.1;
            self.base.color = self.base_color.1.clone();
        }
    }

    pub fn draw(
        &mut self,
        position: &Vector2<GLfloat>,
        globals: &mut RenderingGlobals,
        resource_manager: &mut ResourceManager
    ) {
        self.base.position = position.clone();
        self.base.draw(globals, resource_manager);

        self.corner.position = Vector2::new(
            position[0] - self.base.size[0]/2.0 + OPTION_CORNER_SIZE.0/2.0,
            position[1] + self.base.size[1]/2.0 - OPTION_CORNER_SIZE.1/2.0
        );
        self.corner.rotation = 0.0;
        self.corner.draw(globals, resource_manager);


        self.corner.position = Vector2::new(
            position[0] - self.base.size[0]/2.0 + OPTION_CORNER_SIZE.0/2.0,
            position[1] - self.base.size[1]/2.0 + OPTION_CORNER_SIZE.1/2.0
        );
        self.corner.rotation = 270.0_f32.to_radians();
        self.corner.draw(globals, resource_manager);


        self.corner.position = Vector2::new(
            position[0] + self.base.size[0]/2.0 - OPTION_CORNER_SIZE.0/2.0,
            position[1] - self.base.size[1]/2.0 + OPTION_CORNER_SIZE.1/2.0
        );
        self.corner.rotation = 180.0_f32.to_radians();
        self.corner.draw(globals, resource_manager);


        self.corner.position = Vector2::new(
            position[0] + self.base.size[0]/2.0 - OPTION_CORNER_SIZE.0/2.0,
            position[1] + self.base.size[1]/2.0 - OPTION_CORNER_SIZE.1/2.0
        );
        self.corner.rotation = 90.0_f32.to_radians();
        self.corner.draw(globals, resource_manager);


        self.edge.size = Vector2::new(
            self.base.size[0] - OPTION_CORNER_SIZE.0 * 2.0,
            OPTION_CORNER_SIZE.1
        );
        self.edge.position = Vector2::new(
            position[0],
            position[1] + self.base.size[1]/2.0 - OPTION_CORNER_SIZE.1/2.0
        );
        self.edge.rotation = 0.0;
        self.edge.draw(globals, resource_manager);



        self.edge.position = Vector2::new(
            position[0],
            position[1] - self.base.size[1]/2.0 + OPTION_CORNER_SIZE.1/2.0
        );
        self.edge.rotation = 180.0_f32.to_radians();
        self.edge.draw(globals, resource_manager);


        self.edge.size = Vector2::new(
            self.base.size[1] - OPTION_CORNER_SIZE.1 * 2.0,
            OPTION_CORNER_SIZE.0
        );
        self.edge.position = Vector2::new(
            position[0] + self.base.size[0]/2.0 - OPTION_CORNER_SIZE.1/2.0,
            position[1]
        );
        self.edge.rotation = 90.0_f32.to_radians();
        self.edge.draw(globals, resource_manager);

        self.edge.position = Vector2::new(
            position[0] - self.base.size[0]/2.0 + OPTION_CORNER_SIZE.1/2.0,
            position[1]
        );
        self.edge.rotation = 270.0_f32.to_radians();
        self.edge.draw(globals, resource_manager);

    }

}
