#![allow(dead_code)]
#![allow(unused_imports)]
use regex::Regex;
use std::str;
use std::iter::Iterator;
use std::collections::HashSet;
use std::hash::{Hash};


#[derive(Debug)]
pub struct ScriptLexer<'a, 'b> {
    pub buf: &'a str,
    pub pos: usize,
    pub line: usize,
    pub col: usize,
    pub error: &'b mut Option<LexError>
}


#[derive(Debug,Hash)]
pub enum LexError {
    InvalidEncoding(String),
    BadSymbol(String),
    BadNumber(String),
    BadVariableName(String)
}

impl<'a, 'b> ScriptLexer<'a,'b> {
    pub fn from(string: &'a str, error: &'b mut Option<LexError>) -> Self {
        Self {
            buf: string,
            pos: 0,
            line: 1,
            col: 0,
            error: error
        }
    }

    // '/'
    fn lex_div(&mut self) -> Option<ScriptToken<'a>> {
        // /
        self.pos += 1;
        self.col += 1;
        Some(ScriptToken::SYM_DIVIDE)
    }
    // '*'
    fn lex_mul(&mut self) -> Option<ScriptToken<'a>> {
        // *
        self.pos += 1;
        self.col += 1;
        Some(ScriptToken::SYM_MULTIPLY)
    }
    // '<'
    fn lex_lt(&mut self) -> Option<ScriptToken<'a>> {
        if let Some(mtch) = (Regex::new(r"^<=").unwrap()).find(&self.buf[self.pos..]) {
            self.pos += mtch.end();
            self.col += mtch.end();
            Some(ScriptToken::SYM_LEQ)
        } else {
            self.pos += 1;
            self.col += 1;
            Some(ScriptToken::SYM_LT)
        }
    }
    // '>'
    fn lex_gt(&mut self) -> Option<ScriptToken<'a>> {
        // >=, >
        if let Some(mtch) = (Regex::new(r"^>=").unwrap()).find(&self.buf[self.pos..]) {
            self.pos += mtch.end();
            self.col += mtch.end();
            Some(ScriptToken::SYM_GEQ)
        } else {
            self.pos += 1;
            self.col += 1;
            Some(ScriptToken::SYM_GT)
        }
    }
    // '='
    fn lex_eq(&mut self) -> Option<ScriptToken<'a>> {
        // ==
        if let Some(mtch) = (Regex::new(r"^==").unwrap()).find(&self.buf[self.pos..]) {
            self.pos += mtch.end();
            self.col += mtch.end();
            Some(ScriptToken::SYM_EQ)
        } else {
            *self.error = Some(LexError::BadSymbol(String::from("found free floating = sign")));
            None
        }
    }

    fn lex_and(&mut self) -> Option<ScriptToken<'a>> {
        // ==
        if let Some(mtch) = (Regex::new(r"&&").unwrap()).find(&self.buf[self.pos..]) {
            self.pos += mtch.end();
            self.col += mtch.end();
            Some(ScriptToken::SYM_AND)
        } else {
            *self.error = Some(LexError::BadSymbol(String::from("found free floating &")));
            None
        }
    }

    fn lex_or(&mut self) -> Option<ScriptToken<'a>> {
        // ==
        if let Some(mtch) = (Regex::new(r"||").unwrap()).find(&self.buf[self.pos..]) {
            self.pos += mtch.end();
            self.col += mtch.end();
            Some(ScriptToken::SYM_OR)
        } else {
            *self.error = Some(LexError::BadSymbol(String::from("found free floating |")));
            None
        }
    }


    // '-'
    fn lex_sub(&mut self) -> Option<ScriptToken<'a>> {
        // -, ->
        if let Some(mtch) = (Regex::new(r"^->").unwrap()).find(&self.buf[self.pos..]) {
            self.pos += mtch.end();
            self.col += mtch.end();
            Some(ScriptToken::SYM_BIND)
        } else {
            self.pos += 1;
            self.col += 1;
            Some(ScriptToken::SYM_MINUS)
        }
    }
    // '+'
    fn lex_add(&mut self) -> Option<ScriptToken<'a>> {
        // +
        self.pos += 1;
        self.col += 1;
        Some(ScriptToken::SYM_PLUS)
    }
    // ':'
    fn lex_col(&mut self) -> Option<ScriptToken<'a>> {
        // :=, :
        if let Some(mtch) = (Regex::new(r"^:=").unwrap()).find(&self.buf[self.pos..]) {
            self.pos += mtch.end();
            self.col += mtch.end();
            Some(ScriptToken::SYM_ASSIGN)
        } else {
            self.pos += 1;
            self.col += 1;
            Some(ScriptToken::SYM_COLON)
        }
    }
    // ','
    fn lex_comma(&mut self) -> Option<ScriptToken<'a>> {
        // ,
        self.pos += 1;
        self.col += 1;
        Some(ScriptToken::SYM_COMMA)
    }
    // '('
    fn lex_lparen(&mut self) -> Option<ScriptToken<'a>> {
        // (
        self.pos += 1;
        self.col += 1;
        Some(ScriptToken::SYM_LPAREN)
    }
    // ')'
    fn lex_rparen(&mut self) -> Option<ScriptToken<'a>> {
        // )
        self.pos += 1;
        self.col += 1;
        Some(ScriptToken::SYM_RPAREN)
    }
    // '{'
    fn lex_rbrace(&mut self) -> Option<ScriptToken<'a>> {
        // {
        self.pos += 1;
        self.col += 1;
        Some(ScriptToken::SYM_LBRACE)
    }
    // '}'
    fn lex_lbrace(&mut self) -> Option<ScriptToken<'a>> {
        // }
        self.pos += 1;
        self.col += 1;
        Some(ScriptToken::SYM_RBRACE)
    }
    // ';'
    fn lex_semi(&mut self) -> Option<ScriptToken<'a>> {
        // ;
        self.pos += 1;
        self.col += 1;
        Some(ScriptToken::SYM_SEMI)
    }
    // '"'
    fn lex_strlit(&mut self) -> Option<ScriptToken<'a>> {
        // "<TEXT>"
        self.pos += 1;
        self.col += 1;

        let mut escaped = false;
        let mut end = self.pos;
        for (ind, o_chr) in self.buf[self.pos..].chars().enumerate() {
            end = self.pos + ind;

            match o_chr {
                '"' => {
                    if !escaped {
                        break;
                    } else {
                        escaped = false;
                    }
                }
                '\\' => {
                    if !escaped {
                        escaped = true;
                    } else {
                        escaped = false;
                    }
                }
                _ => {
                    if escaped {
                        escaped = false;
                    }
                }
            }
        }

        let start = self.pos;
        self.pos = end + 1;
        self.col += end + 1;

        Some(ScriptToken::LIT_STR(&self.buf[start..end]))
    }

    // '0'
    fn lex_num(&mut self) -> Option<ScriptToken<'a>> {
        // <NUMBER>
        if let Some(mtch) = (Regex::new(r"^[0-9]*").unwrap()).find(&self.buf[self.pos..]) {
            match mtch.as_str().parse::<i32>() {
                Ok(res) => {
                    self.pos += mtch.end();
                    self.col += mtch.end();
                    Some(ScriptToken::LIT_NUMERIC(res))
                }
                Err(e) => {
                    *self.error = Some(LexError::BadNumber(e.to_string()));
                    None
                }
            }
        } else {
            unreachable!()
        }
    }

    // c
    fn lex_freelit(&mut self, c: char) -> Option<ScriptToken<'a>> {

        match c {
            'c' => {
                if let Some(grp) = (Regex::new(r"^(character)[^[:alnum:]_]").unwrap()).captures(&self.buf[self.pos..]) {
                    if let Some(mtch) = grp.get(1) {
                        self.pos += mtch.end();
                        self.col += mtch.end();
                        return Some(ScriptToken::KEY_CHARACTER);
                    }
                }
                if let Some(grp) = (Regex::new(r"^(check)[^[:alnum:]_]").unwrap()).captures(&self.buf[self.pos..]) {
                    if let Some(mtch) = grp.get(1) {
                        self.pos += mtch.end();
                        self.col += mtch.end();
                        return Some(ScriptToken::KEY_CHECK);
                    }
                }

            },     // character
            'g' => {
                if let Some(grp) = (Regex::new(r"^(goto)[^[:alnum:]_]").unwrap()).captures(&self.buf[self.pos..]) {
                    if let Some(mtch) = grp.get(1) {
                        self.pos += mtch.end();
                        self.col += mtch.end();
                        return Some(ScriptToken::KEY_GOTO);
                    }
                }
            }, // go

            'd' =>
            {
                if let Some(grp) = (Regex::new(r"^(dialog)[^[:alnum:]_]").unwrap()).captures(&self.buf[self.pos..]) {
                    if let Some(mtch) = grp.get(1) {
                        self.pos += mtch.end();
                        self.col += mtch.end();
                        return Some(ScriptToken::KEY_DIALOG);
                    }
                }
                if let Some(grp) = (Regex::new(r"^(drop)[^[:alnum:]_]").unwrap()).captures(&self.buf[self.pos..]) {
                    if let Some(mtch) = grp.get(1) {
                        self.pos += mtch.end();
                        self.col += mtch.end();
                        return Some(ScriptToken::KEY_DROP);
                    }
                }
            },     // dialog, drop
            'v' => {
                if let Some(grp) = (Regex::new(r"^(variable)[^[:alnum:]_]").unwrap()).captures(&self.buf[self.pos..]) {
                    if let Some(mtch) = grp.get(1) {
                        self.pos += mtch.end();
                        self.col += mtch.end();
                        return Some(ScriptToken::KEY_VARIABLE);
                    }
                }
            },      // variable
            'm' => {
                if let Some(grp) = (Regex::new(r"^(map)[^[:alnum:]_]").unwrap()).captures(&self.buf[self.pos..]) {
                    if let Some(mtch) = grp.get(1) {
                        self.pos += mtch.end();
                        self.col += mtch.end();
                        return Some(ScriptToken::KEY_MAP);
                    }
                }
            },      // else
            'e' => {
                if let Some(grp) = (Regex::new(r"^(else)[^[:alnum:]_]").unwrap()).captures(&self.buf[self.pos..]) {
                    if let Some(mtch) = grp.get(1) {
                        self.pos += mtch.end();
                        self.col += mtch.end();
                        return Some(ScriptToken::KEY_ELSE);
                    }
                }
            },      // else
            'i' =>
            {
                if let Some(grp) = (Regex::new(r"^(int)[^[:alnum:]_]").unwrap()).captures(&self.buf[self.pos..]) {
                    if let Some(mtch) = grp.get(1) {
                        self.pos += mtch.end();
                        self.col += mtch.end();
                        return Some(ScriptToken::KEY_INT);
                    }
                }
                if let Some(grp) = (Regex::new(r"^(input)[^[:alnum:]_]").unwrap()).captures(&self.buf[self.pos..]) {
                    if let Some(mtch) = grp.get(1) {
                        self.pos += mtch.end();
                        self.col += mtch.end();
                        return Some(ScriptToken::KEY_INPUT);
                    }
                }
                if let Some(grp) = (Regex::new(r"^(info)[^[:alnum:]_]").unwrap()).captures(&self.buf[self.pos..]) {
                    if let Some(mtch) = grp.get(1) {
                        self.pos += mtch.end();
                        self.col += mtch.end();
                        return Some(ScriptToken::KEY_INFO);
                    }
                }
                if let Some(grp) = (Regex::new(r"^(item)[^[:alnum:]_]").unwrap()).captures(&self.buf[self.pos..]) {
                    if let Some(mtch) = grp.get(1) {
                        self.pos += mtch.end();
                        self.col += mtch.end();
                        return Some(ScriptToken::KEY_ITEM);
                    }
                }
                if let Some(grp) = (Regex::new(r"^(if)[^[:alnum:]_]").unwrap()).captures(&self.buf[self.pos..]) {
                    if let Some(mtch) = grp.get(1) {
                        self.pos += mtch.end();
                        self.col += mtch.end();
                        return Some(ScriptToken::KEY_IF);
                    }
                }
            }
            ,      // int, info, if
            't' => {
                if let Some(grp) = (Regex::new(r"^(text)[^[:alnum:]_]").unwrap()).captures(&self.buf[self.pos..]) {
                    if let Some(mtch) = grp.get(1) {
                        self.pos += mtch.end();
                        self.col += mtch.end();
                        return Some(ScriptToken::KEY_TEXT);
                    }
                }
            },      // text
            'p' => {
                if let Some(grp) = (Regex::new(r"^(prop)[^[:alnum:]_]").unwrap()).captures(&self.buf[self.pos..]) {
                    if let Some(mtch) = grp.get(1) {
                        self.pos += mtch.end();
                        self.col += mtch.end();
                        return Some(ScriptToken::KEY_PROP);
                    }
                }
                if let Some(grp) = (Regex::new(r"^(position)[^[:alnum:]_]").unwrap()).captures(&self.buf[self.pos..]) {
                    if let Some(mtch) = grp.get(1) {
                        self.pos += mtch.end();
                        self.col += mtch.end();
                        return Some(ScriptToken::KEY_POSITION);
                    }
                }
            },      // position
            's' => {
                if let Some(grp) = (Regex::new(r"^(state)[^[:alnum:]_]").unwrap()).captures(&self.buf[self.pos..]) {
                    if let Some(mtch) = grp.get(1) {
                        self.pos += mtch.end();
                        self.col += mtch.end();
                        return Some(ScriptToken::KEY_STATE);
                    }
                }
                if let Some(grp) = (Regex::new(r"^(speak)[^[:alnum:]_]").unwrap()).captures(&self.buf[self.pos..]) {
                    if let Some(mtch) = grp.get(1) {
                        self.pos += mtch.end();
                        self.col += mtch.end();
                        return Some(ScriptToken::KEY_SPEAK);
                    }
                }
                if let Some(grp) = (Regex::new(r"^(show)[^[:alnum:]_]").unwrap()).captures(&self.buf[self.pos..]) {
                    if let Some(mtch) = grp.get(1) {
                        self.pos += mtch.end();
                        self.col += mtch.end();
                        return Some(ScriptToken::KEY_SHOW);
                    }
                }
            },     // state, speak, show
            'r' => {
                if let Some(grp) = (Regex::new(r"^(run)[^[:alnum:]_]").unwrap()).captures(&self.buf[self.pos..]) {
                    if let Some(mtch) = grp.get(1) {
                        self.pos += mtch.end();
                        self.col += mtch.end();
                        return Some(ScriptToken::KEY_RUN);
                    }
                }
                if let Some(grp) = (Regex::new(r"^(remove)[^[:alnum:]_]").unwrap()).captures(&self.buf[self.pos..]) {
                    if let Some(mtch) = grp.get(1) {
                        self.pos += mtch.end();
                        self.col += mtch.end();
                        return Some(ScriptToken::KEY_REMOVE);
                    }
                }
            }, // remove
            'w' => {
                if let Some(grp) = (Regex::new(r"^(while)[^[:alnum:]_]").unwrap()).captures(&self.buf[self.pos..]) {
                    if let Some(mtch) = grp.get(1) {
                        self.pos += mtch.end();
                        self.col += mtch.end();
                        return Some(ScriptToken::KEY_WHILE);
                    }
                }
            },     // while
            'b' => {
                if let Some(grp) = (Regex::new(r"^(bg)[^[:alnum:]_]").unwrap()).captures(&self.buf[self.pos..]) {
                    if let Some(mtch) = grp.get(1) {
                        self.pos += mtch.end();
                        self.col += mtch.end();
                        return Some(ScriptToken::KEY_BG);
                    }
                }
            },     // bg
            'f' => {
                if let Some(grp) = (Regex::new(r"^(format)[^[:alnum:]_]").unwrap()).captures(&self.buf[self.pos..]) {
                    if let Some(mtch) = grp.get(1) {
                        self.pos += mtch.end();
                        self.col += mtch.end();
                        return Some(ScriptToken::KEY_FORMAT);
                    }
                }
            },   // format
            'L' => {
                if let Some(grp) = (Regex::new(r"^(LEFT)[^[:alnum:]_]").unwrap()).captures(&self.buf[self.pos..]) {
                    if let Some(mtch) = grp.get(1) {
                        self.pos += mtch.end();
                        self.col += mtch.end();
                        return Some(ScriptToken::KEY_LEFT);
                    }
                }
            },  // LEFT
            'R' => {
                if let Some(grp) = (Regex::new(r"^(RIGHT)[^[:alnum:]_]").unwrap()).captures(&self.buf[self.pos..]) {
                    if let Some(mtch) = grp.get(1) {
                        self.pos += mtch.end();
                        self.col += mtch.end();
                        return Some(ScriptToken::KEY_RIGHT);
                    }
                }
            },  // RIGHT
            'O' => {
                if let Some(grp) = (Regex::new(r"^(OFFSCREEN)[^[:alnum:]_]").unwrap()).captures(&self.buf[self.pos..]) {
                    if let Some(mtch) = grp.get(1) {
                        self.pos += mtch.end();
                        self.col += mtch.end();
                        return Some(ScriptToken::KEY_RIGHT);
                    }
                }
            },  // OFFSCREEN
            'C' => {
                if let Some(grp) = (Regex::new(r"^(CENTER)[^[:alnum:]_]").unwrap()).captures(&self.buf[self.pos..]) {
                    if let Some(mtch) = grp.get(1) {
                        self.pos += mtch.end();
                        self.col += mtch.end();
                        return Some(ScriptToken::KEY_RIGHT);
                    }
                }
            },  // CENTER
            _   => ()
        }
        if let Some(mtch) = (Regex::new(r"^[[:alpha:]][[:alnum:]_]*").unwrap()).find(&self.buf[self.pos..]) {
            // println!("{:?}-{:?}, {:?}",mtch.start(), mtch.end(), self.pos);
            // println!("{:?}", &self.buf[self.pos..]);
            // println!("{:?}", &self.buf[self.pos+mtch.end()..]);

            self.pos += mtch.end();
            self.col += mtch.end();

            Some(ScriptToken::LIT_VAR(mtch.as_str()))
        } else {
            *self.error = Some(
                LexError::BadVariableName(String::from("emoji users gtfo!"))
            );
            None
        }
    }
}



/*
tokens

KEYWORDS
- character
- dialog
- variable
- int
- text
- state
- position
- speak
- info
- while
- if
- format
- LEFT
- RIGHT
- CENTER
- OFFSCREEN
SYMBOLS
-  SEMI - ;
-  COLON - :
-  LBRACE {
-  RBRACE }
-  LPAREN )
-  RPAREN (
-  ASSIGN_EQ :=
-  PLUS
-  MINUS
-  MULTIPLY
-  DIVIDE
-  LEQ
-  LT
-  GEQ
-  GT
-  EQ
LITERALS
- STR = "...."
- NUMERIC = 1..1
- VAR = a...
 */

#[derive(Debug, PartialEq, Eq)]
pub enum ScriptToken<'a> {
    KEY_CHARACTER,
    KEY_DIALOG,
    KEY_VARIABLE,
    KEY_INT,
    KEY_INPUT,
    KEY_TEXT,
    KEY_STATE,
    KEY_POSITION,
    KEY_SPEAK,
    KEY_INFO,
    KEY_WHILE,
    KEY_IF,
    KEY_ELSE,
    KEY_FORMAT,
    KEY_LEFT,
    KEY_RIGHT,
    KEY_CENTER,
    KEY_OFFSCREEN,
    KEY_MAP,

    KEY_DROP,
    KEY_BG,
    KEY_CHECK,
    KEY_GOTO,
    KEY_SHOW,
    KEY_RUN,
    KEY_ITEM,
    KEY_PROP,
    KEY_REMOVE,

    SYM_SEMI, // ;                   - DONE
    SYM_COLON, // :                  - DONE
    SYM_LBRACE , // {                - DONE
    SYM_RBRACE, // }                 - DONE
    SYM_LPAREN, // )                 - DONE
    SYM_RPAREN , // (                - DONE
    SYM_ASSIGN, //  :=               - DONE
    SYM_BIND, // ->                  - DONE
    SYM_PLUS, // +                   - DONE
    SYM_MINUS, // -                  - DONE
    SYM_MULTIPLY, // *               - DONE
    SYM_DIVIDE, // /                 - DONE
    SYM_LEQ, // <=                   - DONE
    SYM_LT, // <                     - DONE
    SYM_GEQ, // >=                   - DONE
    SYM_GT, // >                     - DONE
    SYM_EQ, // ==                    - DONE
    SYM_COMMA, // ,                  - DONE
    SYM_AND, // &&                   - DONE
    SYM_OR, // ||                    - DONE

    LIT_STR(&'a str),
    LIT_NUMERIC(i32),
    LIT_VAR(&'a str)
}

impl<'a, 'b> Iterator for ScriptLexer<'a, 'b> {
    type Item = ScriptToken<'a>;


    fn next(&mut self) -> Option<Self::Item> {
        loop {
            if let Some(mtch) = (Regex::new(r"^[\r\t\f\v ]*").unwrap()).find(&self.buf[self.pos..]) {
                self.pos += mtch.end();
                self.col += mtch.end();
            }
            if let Some(mtch) = (Regex::new(r"^\n").unwrap()).find(&self.buf[self.pos..]) {
                self.line += 1;
                self.col = 0;
                self.pos += mtch.end();
            } else {
                break;
            }
        }
        if self.pos >= self.buf.len() {
            return None;
        }
        match self.buf[self.pos..].chars().next() {
            Some(chr) => match chr {
                '/' => self.lex_div(),     // /
                '*' => self.lex_mul(),     // *
                '<' => self.lex_lt(),      // <=, <
                '>' => self.lex_gt(),      // >=, >
                '=' => self.lex_eq(),      // ==
                '-' => self.lex_sub(),     // -
                '+' => self.lex_add(),     // +
                ':' => self.lex_col(),     // :=, :
                ',' => self.lex_comma(),   // ,
                '(' => self.lex_lparen(),  // (
                ')' => self.lex_rparen(),  // )
                '{' => self.lex_rbrace(),  // {
                '}' => self.lex_lbrace(),  // }
                ';' => self.lex_semi(),    // ;
                '"' => self.lex_strlit(),  // "<TEXT>"
                '&' => self.lex_and(),     // &&
                '|' => self.lex_or(),      // ||

                '0' => self.lex_num(),     // <NUMBER>
                '1' => self.lex_num(),     // <NUMBER>
                '2' => self.lex_num(),     // <NUMBER>
                '3' => self.lex_num(),     // <NUMBER>
                '4' => self.lex_num(),     // <NUMBER>
                '5' => self.lex_num(),     // <NUMBER>
                '6' => self.lex_num(),     // <NUMBER>
                '7' => self.lex_num(),     // <NUMBER>
                '8' => self.lex_num(),     // <NUMBER>
                '9' => self.lex_num(),     // <NUMBER>

                c   => self.lex_freelit(c)
            },
            None => {
                *self.error = Some(LexError::InvalidEncoding(
                    format!("Invalid string encoding - emoji users gtfo.")
                ));

                None
            }
        }


    }
}

/*
SCRIPTING SYNTAX - DEFINED AD-HOC
-----------------------------------


PROGRAM            := STATEMENT*

STATEMENT          := DECL;
| DIALOG

*/

/*

DECL               := CHARACTER_DECL
| VARIABLE_DECL
| MAP_DECL
| BG_DECL



DIALOG             := dialog <NAME> {
DIALOG_BODY*
}




DIALOG_BODY        := MODIFY_STATE_OP
| MODIFY_POSITION_OP
| ASSIGNMENT_OP
| SAY_OP
| REMOVE_OP
| GOTO_OP
| SHOW_OP
| RUN_OP
| DROP_OP

| INPUT_OP
| WHILE_OP
| IF_OP
;

IF_OP := if BOOL_EXPR { DIALOG_BODY* } (else { DIALOG_BODY* })?



WHILE_OP := while BOOL_EXPR {  DIALOG BODY   }

INPUT_OP := input {
INPUT_MATCH => {
DIALOG_BODY
            } *
}

INPUT_MATCH := "<TEXT>" | variable



GOTO_OP := goto <NAME> ;

SHOW_OP := show <NAME> ;

RUN_OP := run <NAME> ;

DROP_OP := drop <NAME> ;

DIALOG_DECL := VARIABLE_DECL

MODIFY_STATE_OP    :=  state <NAME> <STATE_NAME>;

MODIFY_POSITION_OP := position <NAME> <LEFT|RIGHT|CENTER|OFFSCREEN>;

SAY_OP             := speak <NAME>  SPEAKABLE_VALUE
| info SPEAKABLE_VALUE
;

REMOVE_OP := remove ;



ASSIGNMENT_OP := variable <NAME> := EXPR | "<TEXT>"

EXPR := FACTOR_EXPR + FACTOR_EXPR | FACTOR_EXPR - FACTOR_EXPR | FACTOR_EXPR
FACTOR_EXPR := BASE_EXPR * BASE_EXPR | BASE_EXPR / BASE_EXPR | BASE_EXPR
BASE_EXPR := VALUE | (EXPR)

BOOL_EXPR := EXPR < EXPR
| EXPR > EXPR
| EXPR <= EXPR
| EXPR >= EXPR
| EXPR == EXPR
| check var
| BOOL_EXPR && BOOL_EXPR
| BOOL_EXPR || BOOL_EXPR

*/

