#version 330 core

in vec2 tex_coords;
out vec4 fragColor;

uniform sampler2D image;
uniform vec4 sprite_color;

void main() {
  fragColor = sprite_color * texture(image, tex_coords);
}
