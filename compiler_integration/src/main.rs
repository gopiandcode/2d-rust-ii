
use nom::{
    do_parse, tag, parse_n_m
};
use std::str::FromStr;
use std::num::ParseIntError;
use std::process::Command;

#[derive(Debug, Copy, Clone)]
pub struct TextVariableID(usize);
#[derive(Debug, Copy, Clone)]
pub struct BackgroundID(usize);
#[derive(Debug, Copy, Clone)]
pub struct MapID(usize);
#[derive(Debug, Copy, Clone)]
pub struct ItemID(usize);
#[derive(Debug, Copy, Clone)]
pub struct PropID(usize);
#[derive(Debug, Copy, Clone)]
pub struct SelectID(usize);
#[derive(Debug, Copy, Clone)]
pub struct DialogID(usize);
#[derive(Debug, Copy, Clone)]
pub struct InstructionID(usize);
#[derive(Debug, Copy, Clone)]
pub struct NumericVariableID(usize);
#[derive(Debug, Copy, Clone)]
pub struct CharacterID(usize);
#[derive(Debug, Copy, Clone)]
pub struct StateID(usize);

#[derive(Debug, Copy, Clone)]
pub enum CharacterPosition {
    LEFT,
    RIGHT,
    CENTER,
}

#[derive(Debug)]
pub enum FormatArg {
    Text(Box<TextExpression>),
    Number(NumericExpression),
}

#[derive(Debug)]
pub enum NumericExpression {
    Variable(NumericVariableID),
    Constant(i32),
    Add(Box<NumericExpression>, Box<NumericExpression>),
    Sub(Box<NumericExpression>, Box<NumericExpression>),
    Div(Box<NumericExpression>, Box<NumericExpression>),
    Mul(Box<NumericExpression>, Box<NumericExpression>),
}

#[derive(Debug)]
pub enum TextExpression {
    Variable(TextVariableID),
    Constant(String),
    Add(Box<TextExpression>, Box<TextExpression>),
    Format(String, Vec<FormatArg>),
}

#[derive(Debug)]
pub enum BooleanExpression {
    Gt(NumericExpression, NumericExpression),
    Ge(NumericExpression, NumericExpression),
    Lt(NumericExpression, NumericExpression),
    Le(NumericExpression, NumericExpression),
    Eq(NumericExpression, NumericExpression),
    Check(ItemID),
    And(Box<BooleanExpression>, Box<BooleanExpression>),
    Or(Box<BooleanExpression>, Box<BooleanExpression>),
}

#[derive(Debug)]
pub enum DialogOperation {
    Show(BackgroundID),
    Goto(MapID),
    Run(DialogID),
    Drop(ItemID),
    State(CharacterID, StateID),
    Position(CharacterID, Option<CharacterPosition>),
    SayText(Option<CharacterID>, TextExpression),
    SetNumVar(NumericVariableID, NumericExpression),
    SetStrVar(TextVariableID, TextExpression),
    JumpCond(Vec<(TextExpression, InstructionID)>),
    JumpBool(BooleanExpression, InstructionID),
    Jump(InstructionID),
    Remove,
}

#[derive(Debug)]
pub enum ParserError {
    Numeric(ParseIntError)
}

fn main() {

    let output = Command::new("./vn-game-compiler-exe")
        .arg("compile")
        .arg("--fname")
        .arg("../vn-game-compiler/example.scrpt")
        .output().unwrap().stdout;

    let output = unsafe { String::from_utf8_unchecked(output) };

    println!("{:?}", output);
    println!("Hello, world!");
}
