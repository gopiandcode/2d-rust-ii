{-# LANGUAGE DeriveDataTypeable #-}
module Lib (load_and_compile_program, serialize_compiled_program, run_compiled_program, CompiledDialog, Context) where

import Parser
import TypeChecker
import CodeGenerator
import Interpreter

import System.IO
import System.Exit
import Control.Monad
import Control.Monad.State
import Control.Monad.Trans.Maybe
import Data.List

import Data.Generics.Uniplate.Data


load_and_compile_program :: String -> MaybeT IO (CompiledDialog, Context)
load_and_compile_program fname = MaybeT $ do
  program <- parseFile fname;
  any_errors <- perform_typecheck fname program;
  if not any_errors
    then do
      let (compiled, context) = compile_program program;
      return . Just $(compiled, context)
    else return Nothing

serialize_compiled_program :: CompiledDialog -> Context -> IO ()
serialize_compiled_program code context = do
      serialize_context context
      putStrLn ""
      putStrLn "Code:"
      mapM_ print_compiled_code code

run_compiled_program :: CompiledDialog -> Context -> IO ()
run_compiled_program compiled context = do
       simulate_game compiled


print_compiled_code :: (DialogID, [Instruction]) -> IO ()
print_compiled_code (id, ins) = do
  putStrLn $ "Dialog " ++ show id ++ "," ++ (show . length $ ins)
  mapM_ print_line $ zip [0..] ins;
  putStrLn  ""

print_line :: (Int,Instruction) -> IO ()
print_line (pos,ins) = do
  putStrLn $ (show pos) ++ ": " ++ (show ins)


