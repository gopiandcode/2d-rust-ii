use std::cell::RefCell;
use std::rc::Rc;
use std::ffi::{CString, CStr};
use std::ptr;
use std::path::Path;

use sdl2::event::Event;

use gl::types::{GLfloat, GLint, GLsizei, GLuint, GLsizeiptr};

use nalgebra::base::{Matrix4, Matrix3, Vector2, Vector3, Vector4};
use nalgebra::{Translation3, Rotation3, Transform3, Translation2, Rotation2, Transform2};

use log::{error, warn, info, debug, trace};

use std::time::{Duration, Instant, SystemTime};
use std::os::raw::c_void;

use crate::resource_loader;
use crate::statics::{APPLICATION_INITIAL_SIZE};
use crate::texture::Texture;





/// Buffers the provided data onto the GPU
pub unsafe fn buffer_data<T>(vbo: GLuint, vertices: &[T]) {
    gl::BindBuffer(gl::ARRAY_BUFFER, vbo);
    gl::BufferData(
        gl::ARRAY_BUFFER,
        (vertices.len() * std::mem::size_of::<T>()) as GLsizeiptr,
        vertices.as_ptr() as *const c_void,
        gl::STATIC_DRAW
    );
    gl::BindBuffer(gl::ARRAY_BUFFER, 0);
}

