# Virtual Novel Game Engine

A small and simple virtual novel engine written in rust - uses OpenGL primitives to render, SDL for managing audio and window operations.

The game engine comes with a simple statically typed scripting language for writing games (I could not for the life of me achieve any level of consistency with Renpy - I dare you to do better) - for example a simple game would be:

```
character joe : "./joe.png";
bg office : "./office.png";
variable int count := 0;
variable int max := 0;
dialog talk_to_joe {
   position joe LEFT;
   speak joe "Hey there buddy!";
}

dialog main {
    info "This is a simple game - pick a number:"
	
	input {
	  "one" -> {
	       variable max := 1;
	  }
	  "multiple" -> {
           variable max := 3;
	  }
	}

   while count < max {
      run talk_to_joe;
	  variable count := count + 1;
   }
}
```

The library also provides a simple emacs `vn-mode` with syntax highlighting and error checking built in.

```
  <TODO - Add picture of it in action>
```

## Usage - Building Project
To build:
1. Run `./setup.sh` from the project root.
2. Run `cargo build` from the project root.
3. Run `cargo run`

## Usage - Packaging Games
Once you have built a game, the game engine also provides a utility script to package the result into a neat and tidy app-image.

To do this, simply run `./build.sh` from the project root.

## Screenshots

(TODO)
