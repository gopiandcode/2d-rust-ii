use crate::text::TextRenderer;
use crate::graphics::{TempTexturedQuad, Quad};
use crate::globals::RenderingGlobals;
use crate::statics::{APPLICATION_INITIAL_SIZE, OPTION_CORNER_SIZE, OPTION_BOX_SIZE, OPTION_BOX_MARGIN, OPTION_BOX_SPACING};
use crate::input::{FrameInputs, LogicalKey};
use crate::resource_loader::{ResourceManager, TextureID};

use std::vec::IntoIter;

use gl::types::{GLfloat, GLint, GLsizei, GLuint, GLsizeiptr};
use nalgebra::base::{Vector4, Vector3, Vector2};
use serde::{Serialize, Deserialize};

pub mod masker;

#[derive(Serialize,Deserialize,Debug,Copy,Clone, PartialEq, Eq)]
pub struct MapID(usize);

#[derive(Serialize,Deserialize,Debug,Copy,Clone, PartialEq, Eq, Hash)]
pub struct PropID(pub usize);

#[derive(Serialize,Deserialize,Debug,Copy,Clone, PartialEq, Eq, Hash)]
pub struct ItemID(usize);

#[derive(Serialize,Deserialize,Debug,Copy,Clone, PartialEq, Eq, Hash)]
pub enum SelectID {
    Prop(PropID), Item(ItemID)
}

pub struct MapManager {
    maps: Vec<Map>,
    items: Vec<Item>,
    inventory: Vec<ItemID>,
    current: Option<MapID>,
    selected: Option<SelectID>,
    pub completed: Option<SelectID>,
}

pub struct Map {
    base_tex: TextureID,
    mask_tex: TextureID,
    props: Vec<(GLfloat,GLfloat,GLfloat,GLfloat)>,
    items: Vec<ItemID>,
}

pub struct Item {
    map_tex: TextureID,
    pub inv_tex: TextureID,
    pos: Vector2<GLfloat>,
    size: Vector2<GLfloat>,
}

impl MapManager {

    pub fn inventory_size(&self) -> usize {
        self.inventory.len()
    }

    pub fn inventory(&self) -> Vec<&Item> {
        self.inventory.iter().map(|i| &self.items[i.0]).collect()
    }

    pub fn save_state(&self) -> (Vec<(MapID, Vec<ItemID>)>, Vec<ItemID>, Option<MapID>) {
        (
            self.maps.iter().enumerate().map(|(ind,mp)| (MapID(ind), mp.items.clone())).collect(),
            self.inventory.clone(),
            self.current.clone()
        )
    }

    pub fn restore_state(&mut self, map_items: Vec<(MapID, Vec<ItemID>)>, inventory: Vec<ItemID>, current: Option<MapID>)  {
        for (map, items) in map_items {
            self.maps[map.0].items = items;
        }
        self.inventory = inventory;
        self.current = current;
    }




    pub fn new() -> Self {
        MapManager {
            maps: Vec::new(),
            items: Vec::new(),
            inventory: Vec::new(),
            current: None,
            selected: None,
            completed: None,
        }
    }


    pub fn update(&mut self, input: FrameInputs, _delta_time: f32) {
        if self.completed.is_none() {
            if let Some(map_id) = self.current {
                let map = &self.maps[map_id.0];

                let action = input.input_keys.iter().last();

                if action.is_some() && self.selected.is_none() {
                    self.selected = Some(SelectID::Prop(PropID(0)));
                } else {
                    let (pos_x, pos_y) = {
                        match self.selected {
                            Some(SelectID::Prop(PropID(p))) => {
                                let prop = &map.props[p];

                                (- (APPLICATION_INITIAL_SIZE.0 as GLfloat)/2.0 + prop.0 + prop.2/2.0,
                                (APPLICATION_INITIAL_SIZE.1 as GLfloat)/2.0 - prop.1 - prop.3/2.0)
                            },
                            Some(SelectID::Item(ItemID(i))) => {
                                let item = &self.items[i].pos;
                                (item[0], item[1])
                            },
                            None => (0.0, 0.0)
                        }
                    };

                    match action {
                        Some(LogicalKey::ENTER) => self.completed = self.selected,
                        Some(LogicalKey::UP) => {

                            let mut min_dist = None;

                            for (ind, prop) in map.props.iter().enumerate() {
                                let (p_x, p_y) =
                                    (- (APPLICATION_INITIAL_SIZE.0 as GLfloat)/2.0 + prop.0 + prop.2/2.0,
                                     (APPLICATION_INITIAL_SIZE.1 as GLfloat)/2.0 - prop.1 - prop.3/2.0);

                                if Some(SelectID::Prop(PropID(ind))) != self.selected && p_y >= pos_y {


                                    let d = (p_y - pos_y).powf(2.0) + (p_x - pos_x).powf(2.0);

                                    if let Some((_oth, dist)) = min_dist {
                                        if d < dist {
                                            min_dist = Some((SelectID::Prop(PropID(ind)), d));
                                        }
                                    } else {
                                        min_dist = Some((SelectID::Prop(PropID(ind)), d));
                                    }
                                }
                            }


                            for itid in map.items.iter() {
                                let item = &self.items[itid.0];
                                if Some(SelectID::Item(*itid)) != self.selected && item.pos[1] >= pos_y {

                                    let d = (item.pos[1] - pos_y).powf(2.0) + (item.pos[0] - pos_x).powf(2.0);

                                    if let Some((_oth, dist)) = min_dist {
                                        if d < dist {
                                            min_dist = Some((SelectID::Item(*itid), d));
                                        }
                                    } else {
                                        min_dist = Some((SelectID::Item(*itid), d));
                                    }
                                }
                            }

                            if let Some((v, _)) = min_dist {
                                self.selected = Some(v);
                            }

                        }
                        Some(LogicalKey::DOWN) => {

                            let mut min_dist = None;

                            for (ind, prop) in map.props.iter().enumerate() {
                                let (p_x, p_y) =
                                    (- (APPLICATION_INITIAL_SIZE.0 as GLfloat)/2.0 + prop.0 + prop.2/2.0,
                                     (APPLICATION_INITIAL_SIZE.1 as GLfloat)/2.0 - prop.1 - prop.3/2.0);

                                if Some(SelectID::Prop(PropID(ind))) != self.selected && p_y <= pos_y {


                                    let d = (p_y - pos_y).powf(2.0) + (p_x - pos_x).powf(2.0);

                                    if let Some((_oth, dist)) = min_dist {
                                        if d < dist {
                                            min_dist = Some((SelectID::Prop(PropID(ind)), d));
                                        }
                                    } else {
                                        min_dist = Some((SelectID::Prop(PropID(ind)), d));
                                    }
                                }
                            }


                            for itid in map.items.iter() {
                                let item = &self.items[itid.0];
                                if Some(SelectID::Item(*itid)) != self.selected && item.pos[1] <= pos_y {

                                    let d = (item.pos[1] - pos_y).powf(2.0) + (item.pos[0] - pos_x).powf(2.0);

                                    if let Some((_oth, dist)) = min_dist {
                                        if d < dist {
                                            min_dist = Some((SelectID::Item(*itid), d));
                                        }
                                    } else {
                                        min_dist = Some((SelectID::Item(*itid), d));
                                    }
                                }
                            }

                            if let Some((v, _)) = min_dist {
                                self.selected = Some(v);
                            }

                        }
                        Some(LogicalKey::LEFT) => {

                            let mut min_dist = None;

                            for (ind, prop) in map.props.iter().enumerate() {
                                let (p_x, p_y) =
                                    (- (APPLICATION_INITIAL_SIZE.0 as GLfloat)/2.0 + prop.0 + prop.2/2.0,
                                     (APPLICATION_INITIAL_SIZE.1 as GLfloat)/2.0 - prop.1 - prop.3/2.0);

                                if Some(SelectID::Prop(PropID(ind))) != self.selected && p_x <= pos_x {


                                    let d = (p_y - pos_y).powf(2.0) + (p_x - pos_x).powf(2.0);

                                    if let Some((_oth, dist)) = min_dist {
                                        if d < dist {
                                            min_dist = Some((SelectID::Prop(PropID(ind)), d));
                                        }
                                    } else {
                                        min_dist = Some((SelectID::Prop(PropID(ind)), d));
                                    }
                                }
                            }


                            for itid in map.items.iter() {
                                let item = &self.items[itid.0];
                                if Some(SelectID::Item(*itid)) != self.selected && item.pos[0] <= pos_x {

                                    let d = (item.pos[1] - pos_y).powf(2.0) + (item.pos[0] - pos_x).powf(2.0);

                                    if let Some((_oth, dist)) = min_dist {
                                        if d < dist {
                                            min_dist = Some((SelectID::Item(*itid), d));
                                        }
                                    } else {
                                        min_dist = Some((SelectID::Item(*itid), d));
                                    }
                                }
                            }

                            if let Some((v, _)) = min_dist {
                                self.selected = Some(v);
                            }

                        }
                        Some(LogicalKey::RIGHT) => {

                            let mut min_dist = None;

                            for (ind, prop) in map.props.iter().enumerate() {
                                let (p_x, p_y) =
                                    (- (APPLICATION_INITIAL_SIZE.0 as GLfloat)/2.0 + prop.0 + prop.2/2.0,
                                     (APPLICATION_INITIAL_SIZE.1 as GLfloat)/2.0 - prop.1 - prop.3/2.0);

                                if Some(SelectID::Prop(PropID(ind))) != self.selected && p_x >= pos_x {


                                    let d = (p_y - pos_y).powf(2.0) + (p_x - pos_x).powf(2.0);

                                    if let Some((_oth, dist)) = min_dist {
                                        if d < dist {
                                            min_dist = Some((SelectID::Prop(PropID(ind)), d));
                                        }
                                    } else {
                                        min_dist = Some((SelectID::Prop(PropID(ind)), d));
                                    }
                                }
                            }


                            for itid in map.items.iter() {
                                let item = &self.items[itid.0];
                                if Some(SelectID::Item(*itid)) != self.selected && item.pos[0] >= pos_x {

                                    let d = (item.pos[1] - pos_y).powf(2.0) + (item.pos[0] - pos_x).powf(2.0);

                                    if let Some((_oth, dist)) = min_dist {
                                        if d < dist {
                                            min_dist = Some((SelectID::Item(*itid), d));
                                        }
                                    } else {
                                        min_dist = Some((SelectID::Item(*itid), d));
                                    }
                                }
                            }

                            if let Some((v, _)) = min_dist {
                                self.selected = Some(v);
                            }

                        }
                        _ => ()
                    }
                }
            }
        }
    }


    pub fn draw(&self, globals: &mut RenderingGlobals, resource_manager: &mut ResourceManager) {
            if let Some(map_id) = self.current {
                let map = &self.maps[map_id.0];
                let pos = Vector2::new(0.0, 0.0);
                let mut size = Vector2::new(APPLICATION_INITIAL_SIZE.0 as GLfloat, APPLICATION_INITIAL_SIZE.1 as GLfloat);
                let mut tex_size = Vector2::new(0.0, 0.0);
                let color = Vector4::new(1.0, 1.0, 1.0, 1.0);

                TempTexturedQuad::new(
                    &pos,
                    &size,
                    0.0,
                    &pos,
                    &size,
                    0.0,
                    &color,
                    map.base_tex
                ).draw(globals, resource_manager);


                let mut item_sel = None;
                match self.selected {
                    Some(value) => match value {
                        SelectID::Prop(pos)  => if pos.0 < map.props.len() {
                            let prop = &map.props[pos.0];

                            let pos_x = prop.0 as GLfloat;
                            let pos_y = prop.1 as GLfloat;

                            let size_x = prop.2 as GLfloat;
                            let size_y = prop.3 as GLfloat;

                            tex_size[0] = size_x;
                            tex_size[1] = size_y;

                            let position = Vector2::new(
                                - (APPLICATION_INITIAL_SIZE.0 as GLfloat)/2.0 + pos_x + size_x/2.0,
                                (APPLICATION_INITIAL_SIZE.1 as GLfloat)/2.0 - pos_y - size_y/2.0
                            );
                            let tex_position = Vector2::new(
                                pos_x,
                                pos_y
                            );

                            TempTexturedQuad::new(
                                &position,
                                &tex_size,
                                0.0,
                                &tex_position,
                                &tex_size,
                                0.0,
                                &color,
                                map.mask_tex
                            ).draw(globals, resource_manager);
                        },
                        SelectID::Item(pos) => item_sel = Some(pos.0)
                    },
                    None => ()
                }

                for (ind, item) in map.items.iter().enumerate() {
                    let item = &self.items[item.0];
                    let selected = if Some(ind) == item_sel { 1.1 } else {1.0};

                    let (w,h) = item.map_tex.get_dimensions(resource_manager);

                    size[0] = selected * item.size[0];
                    size[1] = selected * item.size[1];

                    tex_size[0] = w as GLfloat;
                    tex_size[1] = h as GLfloat;

                    TempTexturedQuad::new(
                        &item.pos,
                        &size,
                        0.0,
                        &pos,
                        &tex_size,
                        0.0,
                        &color,
                        item.map_tex
                    ).draw(globals, resource_manager);
                }
            }
    }

    pub fn set_map(&mut self, map_id: MapID) {
        self.current = Some(map_id);
        self.selected = None;
        self.completed = None;
    }

    pub fn drop_item(&mut self, item_id: ItemID) {
        self.inventory.retain(|item| item != &item_id);
    }

    pub fn check_item(&self, item_id: ItemID) -> bool {
        self.inventory.iter().any(|item| item == &item_id)
    }


    pub fn remove_item(&mut self) {
        match self.completed {
            Some(SelectID::Item(itid)) => {
                if let Some(map) = self.current {
                    // remove the item from the map's items
                    let map = &mut self.maps[map.0];
                    map.items.retain(|item| item != &itid);

                    // add the item to the inventory
                    if !self.inventory.contains(&itid) {
                        self.inventory.push(itid);
                    }
                }
            }
            _ => ()
        }
    }

    pub fn add_map(
        &mut self,
        base_tex: TextureID,
        mask_tex: TextureID,
        props: Vec<(GLfloat,GLfloat,GLfloat,GLfloat)>,
        items: Vec<(String, TextureID, TextureID, Vector2<GLfloat>, Vector2<GLfloat>)>,
    ) -> (MapID, Vec<(String, ItemID)>) {

        let items = {
            let mut new_items = Vec::new();

            for x in items.into_iter() {
                let item = Item {
                    map_tex: x.1,
                    inv_tex: x.2,
                    pos: x.3,
                    size: x.4,
                };
                let id = ItemID(self.items.len());
                self.items.push(item);
                new_items.push((x.0, id));
            }
            new_items
        };

        let map = Map {
            base_tex: base_tex,
            mask_tex: mask_tex,
            props: props,
            items: items.iter().map(|x| (x.1)).collect()
        };
        let id = MapID(self.maps.len());
        self.maps.push(map);

        (id, items)
    }
}


