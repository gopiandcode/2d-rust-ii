use crate::text::TextRenderer;
use crate::graphics::{TexturedQuad, Quad};
use crate::globals::RenderingGlobals;
use crate::statics::{APPLICATION_INITIAL_SIZE, CHARACTER_SIZE, CHARACTER_POSITIONS, CHARACTER_SPACING};
use crate::input::{FrameInputs, LogicalKey};
use crate::config::RenderingConfig;
use crate::resource_loader::{ResourceManager};
use crate::script::compiler::{NumericExpression, BooleanExpression};
use crate::map::MapManager;

use std::vec::IntoIter;


#[derive(Debug,Copy, Clone)]
pub struct TextVariableID(usize);
#[derive(Debug,Copy, Clone)]
pub struct NumericVariableID(usize);
#[derive(Debug,Copy, Clone)]
pub enum VariableID {Text(TextVariableID), Numeric(NumericVariableID)}


pub struct StateManager {
    text_variables: Vec<String>,
    numeric_variables: Vec<i32>
}


impl StateManager {

    pub fn save_state(&self) -> (Vec<String>, Vec<i32>) {
        (self.text_variables.clone(), self.numeric_variables.clone())
    }

    pub fn restore_state(&mut self, text_variables: Vec<String>, numeric_variables: Vec<i32>) {
        self.text_variables = text_variables;
        self.numeric_variables = numeric_variables;
    }

    pub fn new() -> Self {
        Self {
            text_variables: Vec::new(),
            numeric_variables: Vec::new(),
        }
    }

    pub fn add_text_variable<T: Into<String>>(&mut self, string: T) -> TextVariableID {
        let id = TextVariableID(self.text_variables.len());

        self.text_variables.push(string.into());

        id
    }

    pub fn add_numeric_variable(&mut self, value: i32) -> NumericVariableID {
        let id = NumericVariableID(self.numeric_variables.len());

        self.numeric_variables.push(value);

        id
    }


    pub fn get_numeric_variable(&self, id: NumericVariableID) -> i32 {
        self.numeric_variables[id.0]
    }

    pub fn set_numeric_variable(&mut self, id: NumericVariableID, value: i32) {
        self.numeric_variables[id.0] = value;
    }


    pub fn get_text_variable(&self, id: TextVariableID) -> &str {
        &self.text_variables[id.0]
    }

    pub fn set_text_variable(&mut self, id: TextVariableID, value: String)  {
        self.text_variables[id.0] = value;
    }


    pub fn eval_expr(&self, expr: &NumericExpression) -> i32 {
        match expr {
            NumericExpression::Variable(val) => self.get_numeric_variable(*val),
            NumericExpression::Constant(v) => *v,
            NumericExpression::Add(a, b) => self.eval_expr(a) + self.eval_expr(b),
            NumericExpression::Sub(a, b) => self.eval_expr(a) - self.eval_expr(b),
            NumericExpression::Div(a, b) => self.eval_expr(a) / self.eval_expr(b),
            NumericExpression::Mul(a, b) => self.eval_expr(a) * self.eval_expr(b),
        }
    }

    pub fn eval_bexpr(&self, map_manager: &MapManager, expr: &BooleanExpression) -> bool {
        match expr {
            BooleanExpression::Gt(a, b) => self.eval_expr(a) > self.eval_expr(b),
            BooleanExpression::Ge(a, b) => self.eval_expr(a) >= self.eval_expr(b),
            BooleanExpression::Lt(a, b) => self.eval_expr(a) < self.eval_expr(b),
            BooleanExpression::Le(a, b) => self.eval_expr(a) <= self.eval_expr(b),
            BooleanExpression::Eq(a, b) => self.eval_expr(a) == self.eval_expr(b),
            BooleanExpression::Check(item) => map_manager.check_item(*item),
            BooleanExpression::And(a,b) => self.eval_bexpr(map_manager, a) && self.eval_bexpr(map_manager, b),
            BooleanExpression::Or(a,b) => self.eval_bexpr(map_manager, a) || self.eval_bexpr(map_manager, b)
        }
    }


}
